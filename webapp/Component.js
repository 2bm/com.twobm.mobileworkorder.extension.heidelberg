jQuery.sap.declare("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.Component");

// use the load function for getting the optimized preload file if present
sap.ui.component.load({
	name: "com.twobm.mobileworkorder",
	// Use the below URL to run the extended application when SAP-delivered application is deployed on cloud
	// Remove the url parameter once your application is deployed to productive account
	url: jQuery.sap.getModulePath("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension") + "/parent"
		// we use a URL relative to our own component
		// extension application is deployed with customer namespace
});

this.com.twobm.mobileworkorder.Component.extend("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.Component", {
	metadata: {
		manifest: "json",
		includes: [
			"css/calciteWebCoreIcons.css",
			"css/arcgisMap4.9.css"
		]
	},

	init: function () {

		com.twobm.mobileworkorder.Component.prototype.init.apply(this, arguments);

		// Get main i18n model (configured to contain base application texts)
		var oI18nModel = this.getModel("i18n");
		// Get extension i18n model
		var oBundle = this.getModel("i18nExtended").getResourceBundle();
		// Enhance the main i18n with the text from the extention model
		oI18nModel.enhance(oBundle);

	},
});