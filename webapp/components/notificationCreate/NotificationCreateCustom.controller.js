sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"com/twobm/mobileworkorder/components/offline/SyncManager"
], function (Controller, History, MessageBox, Filter, SyncManager) {
	"use strict";
	return sap.ui.controller("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.notificationCreate.NotificationCreateCustom", {

		onRouteMatched: function (oEvent) {
			var direction = sap.ui.core.routing.History.getInstance().getDirection();

			if (direction !== "Backwards") {

				this.getView().setModel(new sap.ui.model.json.JSONModel({
					NotificationShortText: "",
					NotificationLongText: "",
					WorkCenterSelectedText: "",
					WorkCenterSelectedID: "",
					NotificationTypeSelectedType: "M1",
					NotificationTypeSelectedText: "Malfunction Report",
					NotificationPrioritySelectedType: "",
					NotificationPrioritySelectedText: "",
					Photos: []
				}), "viewModel");

				this.newEntry = this.getView().getModel().createEntry("/NotificationsSet", {
					changeSetId: "CreateNotif",
					success: function (oData, oResponse) {
						this.getView().setBusy(false);

						// check if photos exists
						/*	var photos = this.getView().getModel("viewModel").getData().Photos;
							if (photos.length > 0) {
								this.getView().setBusy(true);
								photos.forEach(function (photo) {
									this.onPhotoUpload(photo.Src);
								}.bind(this));
							} else {
								this.navToNavigationDetail();
							}*/
						var photos = this.getView().getModel("viewModel").getData().Photos;
						this.checkForPhotoUpload(photos)
							.then(function () {
								//Make sure to send in the notification immediately
								SyncManager.flushAndThenRefreshSubsets(["NotificationsSet", "PhotosSet"]);
								//this.getOwnerComponent().resetNewNotificationModel();
								this.navToNavigationDetail();
							}.bind(this));
					}.bind(this),
					error: function (error) {
						if (error.statusCode === 0) {
							return;
						}

						this.getView().setBusy(false);
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				});
			}

			var selectObjectForNewNotificationModel = this.getView().getModel("selectObjectForNewNotificationModel");

			if (!jQuery.isEmptyObject(selectObjectForNewNotificationModel.getData())) {
				if (this.newEntry) {
					if (selectObjectForNewNotificationModel.getData().equipmentNo !== "NONE" && selectObjectForNewNotificationModel.getData().equipmentDesc !==
						"NONE") {
						this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equipment", selectObjectForNewNotificationModel.getData().equipmentNo);
						this.getView().getModel().setProperty(this.newEntry.getPath() + "/EquipmentDesc", selectObjectForNewNotificationModel.getData().equipmentDesc);
					}

					this.getView().getModel().setProperty(this.newEntry.getPath() + "/FuncLocDesc", selectObjectForNewNotificationModel.getData().funcLocDesc);
					this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctionalLoc", selectObjectForNewNotificationModel.getData().functionalLoc);

					//this.getView().getModel().setProperty(this.newEntry.getPath() + "/ShortText", selectObjectForNewNotificationModel.getData().shortText);
					//this.getView().getModel().setProperty(this.newEntry.getPath() + "/LongText", selectObjectForNewNotificationModel.getData().longText);
				}
			}

			this.getView().setBindingContext(this.newEntry);
		},

		onSaveNotification: function () {
			if (this.notificationInformationValidates()) {
				this.getView().getModel().setProperty("ShortText", this.getView().getModel("viewModel").getData().NotificationShortText,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("LongText", this.getView().getModel("viewModel").getData().NotificationLongText,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("NotifDate", new Date().toISOString().substr(0, 19), this.getView().getBindingContext());
				this.getView().getModel().setProperty("WorkCenterId", this.getView().getModel("viewModel").getData().WorkCenterSelectedID, this.getView()
					.getBindingContext());
				this.getView().getModel().setProperty("NotifType", this.getView().getModel("viewModel").getData().NotificationTypeSelectedType,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("Priority", this.getView().getModel("viewModel").getData().NotificationPrioritySelectedType,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("WorkCenterName", this.getView().getModel("viewModel").getData().WorkCenterSelectedText,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("WorkCenterText", this.getView().getModel("viewModel").getData().WorkCenterSelectedType,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("NotifTypeText", this.getView().getModel("viewModel").getData().NotificationTypeSelectedText,
					this.getView().getBindingContext());
				this.getView().getModel().setProperty("PriorityText", this.getView().getModel("viewModel").getData().NotificationPrioritySelectedText,
					this.getView().getBindingContext());

				this.getView().setBusy(true);

				// lookup longitude latitude
				var funcLoc = this.getView().getModel().getProperty(this.newEntry.getPath() + "/FunctionalLoc");
				this.readLatLong(funcLoc).then(function (funcObj) {
					// set longitude
					this.getView().getModel().setProperty("Longitude", funcObj.Longitude,
						this.getView().getBindingContext());
					// set latitude
					this.getView().getModel().setProperty("Latitude", funcObj.Latitude,
						this.getView().getBindingContext());

					this.getView().getModel().submitChanges({
						success: jQuery.proxy(function (oData, oResponse) {
							this.getView().setBusy(false);
						}, this),
						error: jQuery.proxy(function (error) {
							this.getView().setBusy(false);
							this.errorCallBackShowInPopUp(error);
						}, this)
					});
				}.bind(this), function (error) {
					// error handling
					this.errorCallBackShowInPopUp(error);
				}.bind(this));

			} else {
				sap.m.MessageToast.show(this.getI18nText("NotificationCreate-SaveValidation-Failed"));
			}
		},

		formatSelectedWorkCenter: function (type, description) {
			if (type) {
				return description + " - " + type;
			}

			return "";
		},

		onSelectBtnPress: function (oEvent) {
				setTimeout(function () {
				this.getRouter().navTo("structureBrowser", {
					notificationContext: this.newEntry.getPath().substr(1),
					parentView: "notificationCreate"
				}, false);
			}.bind(this), 1);
		},

		onNavigateBack: function (oEvent) {
			//Reset create notification model
			var selectObjectForNewNotificationModel = this.getView().getModel("selectObjectForNewNotificationModel");
			selectObjectForNewNotificationModel.setData({});

			//Delete created entry
			this.getView().getModel().deleteCreatedEntry(this.getView().getBindingContext());

			//reset any error state
			this.getView().byId("shortDescription").setValueState("None");
			this.getView().byId("notificationPriorityInput").setValueState("None");

			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			this.getView().setBindingContext(null);

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				//oRouter.navTo("dashboard", {}, true);
				oRouter.navTo("dashboard", true);

			}
		},
		notificationInformationValidates: function () {
			var validationError = false;

			//reset
			this.getView().byId("shortDescription").setValueState("None");
			this.getView().byId("notificationPriorityInput").setValueState("None");

			if (!this.getView().getModel("viewModel").getData().NotificationShortText) {
				this.getView().byId("shortDescription").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().byId("notificationPriorityInput").getValue()) {
				this.getView().byId("notificationPriorityInput").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().getBindingContext().getObject().FunctionalLoc) {
				//this.getView().byId("funcLoc").setValueState("Error");
				validationError = true;
			}

			if (validationError) {
				return false;
			}

			return true;
		},
		onPhotoUpload: function (imageAsBase64) {
			return new Promise(function (resolve, reject) {
				var date = new Date();
				var month = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
				var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
				var hour = (date.getHours() < 10 ? "0" : "") + date.getHours();
				var min = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
				var sec = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
				var dateStr = day + month + date.getFullYear() + "_" + hour + min + sec;
				var userName = this.getView().getModel("appInfoModel").getData().UserName;
				var fileName = "Photo_" + userName + "_" + dateStr + ".jpg";

				var fileData = {
					Filename: fileName,
					Changedate: new Date(),
					Createdby: userName
				};

				var parameters = {
					headers: {
						"slug": fileName,
						"Mimetype": 'image/jpeg'
					},
					success: function (data) {
						var serviceUrl;
						var etag;
						if (data["@com.sap.vocabularies.Offline.v1.isLocal"] || data["@com.sap.vocabularies.Offline.v1.mediaIsOffline"]) {
							serviceUrl = data.__metadata.edit_media;
							etag = data.__metadata.media_etag;
						} else {
							serviceUrl = this.getView().getModel().sServiceUrl + "/PhotosSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
								"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')/$value";
						}

						var value = {
							"imageAsBase64": imageAsBase64,
							"fileName": fileName,
							"serviceUrl": serviceUrl,
							"etag": etag
						};
						resolve(value);

						//Upload the file stream to local db
						//this.sendUploadRequest(imageAsBase64, fileName, serviceUrl, etag);
					}.bind(this),
					error: function (error) {
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				};

				//Create document entry
				var createPath = this.getView().getBindingContext().getPath() + "/PhotosSet";

				//this.getView().getModel().create(createPath, fileData, parameters);
				this.getView().getModel().create(createPath, fileData, parameters);
			}.bind(this))
		},
		sendUploadRequest: function (imageData, fileName, serviceUrl, etag) {
			return new Promise(function (resolve, reject) {
				if (sap.hybrid && cordova.require("cordova/platform").id == "ios" && sap.Xhook && window.webkit && window.webkit.messageHandlers) {
					var headers = {};
					headers['Accept'] = "application/json";
					headers['X-CSRF-Token'] = this.getView().getModel().getSecurityToken();
					headers['Content-Type'] = "image/png";
					headers['if-match'] = etag;
					headers['content-encoding'] = "base64";
					headers['slug'] = fileName;
					sap.OData.request({
							requestUri: serviceUrl,
							method: "PUT",
							headers: headers,
							body: imageData
						},
						function (data, response) {
							resolve();
						}.bind(this),
						function (error) {
							reject();
						}.bind(this)
					);
				} else {
					var xhr = new XMLHttpRequest();
					xhr.open("PUT", serviceUrl, false);
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("content-type", "image/png"); // Not used by backend, but necessary for transport to backend
					xhr.setRequestHeader("content-encoding", "base64");
					xhr.setRequestHeader("x-csrf-token", this.getView().getModel().getSecurityToken());
					xhr.setRequestHeader("slug", fileName);
					xhr.setRequestHeader("if-match", etag);
					xhr.onreadystatechange = function () {
						if (xhr.readyState === 4) {
							if (xhr.status === 201 || xhr.status === 204) {
								resolve();
							} else {
								reject();
							}
						}
					}.bind(this);
					if (sap.hybrid) {
						xhr.send(imageData);
					} else {
						xhr.send(this.createBlob(imageData));
					}
				}
			}.bind(this));
		},
		createBlob: function (base64Str) {
			var byteCharacters = atob(base64Str);
			var byteNumbers = new Array(byteCharacters.length);
			for (var i = 0; i < byteCharacters.length; i++) {
				byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			const byteArray = new Uint8Array(byteNumbers);
			return new Blob([byteArray], {
				type: "image/jpeg"
			});
		},
		checkForPhotoUpload: function (photos) {
			return new Promise(function (resolve, reject) {
				if (photos.length > 0) {
					var promises = [];
					photos.forEach(function (photo) {
						promises.push(this.onPhotoUpload(photo.Src));
					}.bind(this));

					Promise.all(promises)
						.then(
							function (results) {
								return Promise.all(results.map(function (data) {
									return this.sendUploadRequest(data.imageAsBase64, data.fileName, data.serviceUrl, data.etag);
								}.bind(this)));
							}.bind(this))
						.then(function () {
							resolve();
						});
				} else {
					resolve();
				}
			}.bind(this));
		}
	});
});