sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"com/twobm/mobileworkorder/util/Formatter"
], function (Controller, Formatter) {
	"use strict";

	return sap.ui.controller(
		"com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.workOrderDetailsSummary.blocks.GeneralBlockCustom", {
			formatter: Formatter,

			formatPMActivityType: function (type, description) {
				if (type) {
					return description + " (" + type + ")";
				}
			},

			formatPriority: function (Priority, description) {
				if (Priority) {
					return description + " (" + Priority + ")";
				}
			}
		});
});