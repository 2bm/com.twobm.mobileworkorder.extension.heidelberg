sap.uxap.BlockBase.extend("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.workOrderDetailsSummary.blocks.configuration.GeneralBlock", {
    metadata: {
        views: {
            Collapsed: {
                viewName: "com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.workOrderDetailsSummary.blocks.GeneralBlockCustom",
                type: "XML"
            },
            Expanded: {
                viewName: "com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.workOrderDetailsSummary.blocks.GeneralBlockCustom",
                type: "XML"
            }
        }
    }
});