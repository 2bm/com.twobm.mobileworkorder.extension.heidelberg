sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"../../model/models"
], function (Controller, History, MessageBox, Filter, models) {
	"use strict";

	return sap.ui.controller("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.workOrderCreate.WorkOrderCreateCustom", {
		onSuggestionItemSelected: function (oEvent) {
			var oItem = oEvent.getParameter("selectedItem");
			var oText = oItem ? oItem.getKey() : "";
			this.byId("selectedKeyIndicator").setText(oText);
		},

		onRouteMatched: function (oEvent) {
			var direction = sap.ui.core.routing.History.getInstance().getDirection();

			if (direction !== "Backwards") {

				this.getView().setModel(new sap.ui.model.json.JSONModel({
					WorkCenterSelectedText: "",
					WorkCenterSelectedID: "",
					OrderTypeSelectedType: "",
					OrderTypeSelectedText: "",
					OrderPrioritySelectedType: "",
					OrderPrioritySelectedText: "",
					OrderPMActivitySelectedType: "",
					OrderPMActivitySelectedText: "",
					Geolocation: ""
				}), "viewModel");

				var oArguments = oEvent.getParameter("arguments");
				var notificationId = oArguments.notificationId;

				if (notificationId !== "NONOTIFICATIONID") {
					//If notificationId is specified create order from notification
					this.clearViewModel(notificationId);
				} else {
					this.clearViewModel("");

					//Ensure that notification model is cleared when no notif id is given
					this.clearCreateWorkOrderModel(); //Clear the notification model
				}

				this.newEntry = this.getView().getModel().createEntry("/OrderSet", {
					success: function (oData, oResponse) {
						this.getView().setBusy(false);
						this.getRouter().navTo("workOrderDetails", {
							workOrderContext: this.newEntry.getPath().substr(1)
						}, true);

					}.bind(this),
					error: function (error) {
						if (error.statusCode === 0) {
							return;
						}

						this.getView().setBusy(false);
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				});
			}

			var selectObjectForNewWorkOrderModel = this.getView().getModel("selectObjectForNewNotificationModel");

			if (!jQuery.isEmptyObject(selectObjectForNewWorkOrderModel.getData())) {
				if (this.newEntry) {
					if (selectObjectForNewWorkOrderModel.getData().equipmentNo !== "NONE" && selectObjectForNewWorkOrderModel.getData().equipmentDesc !==
						"NONE") {
						this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equipment", selectObjectForNewWorkOrderModel.getData().equipmentNo);
						this.getView().getModel().setProperty(this.newEntry.getPath() + "/Equidescr", selectObjectForNewWorkOrderModel.getData().equipmentDesc);
					}

					this.getView().getModel().setProperty(this.newEntry.getPath() + "/Funcldescr", selectObjectForNewWorkOrderModel.getData().funcLocDesc);
					this.getView().getModel().setProperty(this.newEntry.getPath() + "/FunctLoc", selectObjectForNewWorkOrderModel.getData().functionalLoc);
				}
			}
			this.getView().setBindingContext(this.newEntry);
		},

		onNavigateBack: function (oEvent) {
			this.clearCreateWorkOrderModel();

			//Delete created entry
			this.getView().getModel().deleteCreatedEntry(this.newEntry);

			//reset any error state
			this.getView().byId("workOrderShortDesc").setValueState("None");
			this.getView().byId("workOrderType").setValueState("None");
			this.getView().byId("workCenterInput").setValueState("None");
			this.getView().byId("workOrderPMActType").setValueState("None");
			this.getView().byId("WorkOrderPriority").setValueState("None");

			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			this.getView().setBindingContext(null);

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("dashboard", true);
			}
		},

		onSaveWorkOrder: function () {
			if (!this.ValidateOrderData()) {
				sap.m.MessageToast.show(this.getI18nText("NotificationCreate-SaveValidation-Failed"));
				return;
			}

			// Check for empty dates
			if (this.getView().getBindingContext().getObject().StartDate === undefined) {
				this.getView().getModel().setProperty("StartDate", new Date(),
					this.getView().getBindingContext());
			}

			if (this.getView().getBindingContext().getObject().FinishDate === undefined) {
				this.getView().getModel().setProperty("FinishDate", new Date(),
					this.getView().getBindingContext());
			}

			// Add value for order type and work center 
			this.getView().getModel().setProperty("NotifNo", this.getView().getModel("viewModel").getData().NotificationId,
				this.getView().getBindingContext());

			this.getView().getModel().setProperty("OrderType", this.getView().getModel("viewModel").getData().OrderTypeSelectedType,
				this.getView().getBindingContext());
			this.getView().getModel().setProperty("OrderTypeTxt", this.getView().getModel("viewModel").getData().OrderTypeSelectedText,
				this.getView().getBindingContext());

			this.getView().getModel().setProperty("Priority", this.getView().getModel("viewModel").getData().OrderPrioritySelectedType,
				this.getView().getBindingContext());
			this.getView().getModel().setProperty("PriorityText", this.getView().getModel("viewModel").getData().OrderPrioritySelectedText,
				this.getView().getBindingContext());

			this.getView().getModel().setProperty("Pmacttype", this.getView().getModel("viewModel").getData().OrderPMActivitySelectedType,
				this.getView().getBindingContext());

			this.getView().getModel().setProperty("MnWkctrId", this.getView().getModel("viewModel").getData().WorkCenterSelectedID, this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("MnWkCtr", this.getView().getModel("viewModel").getData().WorkCenterSelectedType, this.getView()
				.getBindingContext());
			//These attributes needed to make order look complete in offline mode also
			this.getView().getModel().setProperty("Assignedtome", "X", this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("AssignedBy", this.getView().getModel("appInfoModel").getData().UserFullName, this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("AssignedTo", this.getView().getModel("appInfoModel").getData().UserFullName, this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("Personresp", this.getView().getModel("appInfoModel").getData().Persno, this.getView()
				.getBindingContext());
			this.getView().getModel().setProperty("OrderStatus", "INITIAL", this.getView()
				.getBindingContext());

			this.getView().setBusy(true);

			// Geolocation
			// this.getLocation(true).then(function (geo) {
			// 	var lat = geo.latitude.toString().substring(0, 12);
			// 	var long = geo.longitude.toString().substring(0, 12); 
			// 	var geoLoc = lat + "#" + long;

			// 	this.getView().getModel().setProperty("Sortfield", geoLoc, this.getView().getBindingContext());

			// 	// lookup longitude latitude
			// 	var funcLoc = this.getView().getModel().getProperty(this.newEntry.getPath() + "/FunctLoc");
			// 	this.readLatLong(encodeURI(funcLoc)).then(function (funcObj) {
			// 		// set longitude
			// 		this.getView().getModel().setProperty("Longitude", funcObj.Longitude,
			// 			this.getView().getBindingContext());
			// 		// set latitude
			// 		this.getView().getModel().setProperty("Latitude", funcObj.Latitude,
			// 			this.getView().getBindingContext());

					this.getView().getModel().submitChanges({
						success: jQuery.proxy(function (oData, oResponse) {
							this.getView().setBusy(false);
							this.clearCreateWorkOrderModel();
						}, this),
						error: jQuery.proxy(function (error) {
							this.getView().setBusy(false);
							this.errorCallBackShowInPopUp(error);
						}, this)
					});
			// 	}.bind(this), function (error) {
			// 		// error handling
			// 		this.errorCallBackShowInPopUp(error);
			// 	}.bind(this));

			// }.bind(this));
		},

		ValidateOrderData: function () {
			var validationError = false;

			//reset
			this.getView().byId("workOrderShortDesc").setValueState("None");
			this.getView().byId("workOrderType").setValueState("None");
			this.getView().byId("workOrderPMActType").setValueState("None");
			this.getView().byId("WorkOrderPriority").setValueState("None");
			this.getView().byId("workCenterInput").setValueState("None");

			if (!this.getView().getBindingContext().getObject().ShortText) {
				this.getView().byId("workOrderShortDesc").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().byId("workOrderType").getValue()) {
				this.getView().byId("workOrderType").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().byId("workOrderPMActType").getValue()) {
				this.getView().byId("workOrderPMActType").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().byId("workCenterInput").getValue()) {
				this.getView().byId("workCenterInput").setValueState("Error");
				validationError = true;
			}

			if (!this.getView().getModel("viewModel").getData().CreateFromNotification && !this.getView().getBindingContext().getObject().FunctLoc) {
				validationError = true;
			}

			if (validationError) {
				return false;
			}

			return true;
		},

		// Get Geolocation
		getLocation: function (enableHighAccuracy) {
			return new Promise(function (resolve, reject) {

				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function (position) {
							var obj = {};
							// Accuracy in meters, latitude and longitude
							obj.accuracy = position.coords.accuracy;
							obj.latitude = position.coords.latitude;
							obj.longitude = position.coords.longitude;
							resolve(obj);
						},
						function (error) {
							console.warn(error.code + ' ' + error.message);
							resolve(null);
						}, {
							enableHighAccuracy: enableHighAccuracy
						}
					);
				} else {
					resolve(null);
				}
			});
		},

		// Order Type
		onFilterOrderTypeSelected: function (oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().OrderTypeSelectedType = selectedItem.getTitle();
			viewModel.getData().OrderTypeSelectedText = selectedItem.getDescription();
			viewModel.refresh();

			this.getView().byId("workOrderType").setValueState("None");

			viewModel.getData().OrderPrioritySelectedType = '';
			viewModel.getData().OrderPrioritySelectedText = '';
			this.getView().byId("WorkOrderPriority").setEditable(true);
			this.getView().byId("WorkOrderPriority").setValue('');
			this.getView().byId("WorkOrderPriority").setValueState("None");

			viewModel.getData().OrderPMActivitySelectedType = '';
			viewModel.getData().OrderPMActivitySelectedText = '';
			this.getView().byId("workOrderPMActType").setEditable(true);
			this.getView().byId("workOrderPMActType").setValue('');
			this.getView().byId("workOrderPMActType").setValueState("None");

		},

		// Priority
		onOrderPriorityValueHelpRequest: function (oEvent) {
			this._orderPriorityPopover = sap.ui.xmlfragment("OrderPriorityPopover",
				"com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.workOrderCreate.fragments.OrderPriorityFilterSelect",
				this);
			this._orderPriorityPopover.getAggregation('_dialog').getSubHeader().setVisible(false);
			this._orderPriorityPopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
			this._orderPriorityPopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
			this._orderPriorityPopover.getAggregation('_dialog').setVerticalScrolling(true);
			
			
			var aFilters = [];

			var aSorters = [];
			aSorters.push(new sap.ui.model.Sorter("PriorityType", false));

			this._orderPriorityPopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
				path: "/OrderTypesSet('" + this.getView().getModel("viewModel").getData().OrderTypeSelectedType + "')/OrderPriorities",
				template: new sap.m.StandardListItem({
					title: "{PriorityText}",
					description: "{PriorityType}"
				}),
				parameters: {
					operationMode: "Client"
				},
				filters: aFilters,
				sorter: aSorters
			});

			this.getView().addDependent(this._orderPriorityPopover);

			this._orderPriorityPopover.open();
		},

		onWorkOrderPriorityConfirm: function (oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().OrderPrioritySelectedType = selectedItem.getDescription();
			viewModel.getData().OrderPrioritySelectedText = selectedItem.getTitle();
			viewModel.refresh();

			this.getView().byId("WorkOrderPriority").setValueState("None");
		},

		// PM Activity Type
		onOrderPMActivityValueHelpRequest: function (oEvent) {
			this._orderPMActivityTypePopover = sap.ui.xmlfragment("OrderPMActivityTypePopover",
				"com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.workOrderCreate.fragments.OrderPMActivityFilterSelect",
				this);
			this._orderPMActivityTypePopover.getAggregation('_dialog').getSubHeader().setVisible(false);
			this._orderPMActivityTypePopover.getAggregation('_dialog').getContent()[1].setGrowingScrollToLoad(true);
			this._orderPMActivityTypePopover.getAggregation('_dialog').getContent()[1].setGrowingThreshold(20);
			this._orderPMActivityTypePopover.getAggregation('_dialog').setVerticalScrolling(true);
			
			var aFilters = [];

			var aSorters = [];
			aSorters.push(new sap.ui.model.Sorter("PMActType", false));

			this._orderPMActivityTypePopover.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
				path: "/OrderTypesSet('" + this.getView().getModel("viewModel").getData().OrderTypeSelectedType + "')/OrderActivityTypes",
				template: new sap.m.StandardListItem({
					title: "{PMActDesc}",
					description: "{PMActType}"
				}),
				parameters: {
					operationMode: "Client"
				},
				filters: aFilters,
				sorter: aSorters
			});

			this.getView().addDependent(this._orderPMActivityTypePopover);

			this._orderPMActivityTypePopover.open();
		},

		onOrderPMActivityTypeConfirm: function (oEvent) {
			var selectedItem = oEvent.getParameter("selectedItem");

			var viewModel = this.getView().getModel("viewModel");
			viewModel.getData().OrderPMActivitySelectedType = selectedItem.getDescription();
			viewModel.getData().OrderPMActivitySelectedText = selectedItem.getTitle();
			viewModel.refresh();

			this.getView().byId("workOrderPMActType").setValueState("None");
		},

		formatPMActivity: function (OrderPMActivitySelectedText, OrderPMActivitySelectedType) {
			if (OrderPMActivitySelectedText) {
				return OrderPMActivitySelectedType + " " + OrderPMActivitySelectedText;
			}
		},

		formatPriority: function (OrderPrioritySelectedText, OrderPrioritySelectedType) {
			if (OrderPrioritySelectedText) {
				return OrderPrioritySelectedType  + " " +  OrderPrioritySelectedText;
			}
		},

		formatOrderType: function (orderTypeSelectedText, orderTypeSelectedType) {
			if (orderTypeSelectedText) {
				return orderTypeSelectedType + " " + orderTypeSelectedText;
			}
		},
		
		formatSelectedWorkCenter: function (type, description) {
			if (type) {
				return type + " " + description;
			}

			return "";
		},
	});
});