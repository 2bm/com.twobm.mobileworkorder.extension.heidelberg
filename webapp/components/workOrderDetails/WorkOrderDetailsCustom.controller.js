sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox"
], function (Controller, History, MessageBox) {
	"use strict";
	return sap.ui.controller("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.workOrderDetails.WorkOrderDetailsCustom", {
		/*onAfterRendering: function () {
			this.getEventBus().publish("getPhotosondemand", {});
		},*/
		onRouteMatched: function (oEvent) {
			//Are we navigating to this view??
			//if not do nothing
			var oArguments = oEvent.getParameter("arguments");
			var contextPath = '/' + oArguments.workOrderContext;
			var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

			//this.oContext is the current context of the view
			//this context is the context that was set when the view was shown the last time
			//therefore the new contextPath can be different from the contextPath/context
			//that was shown the last time the view was shown
			if (!this.getView().getBindingContext() || this.getView().getBindingContext().getPath() !== contextPath) {
				//Reset model to the new context
				this.ExpandLoaded = false;
				//this.oContext = givenContext;
				this.getView().setBindingContext(givenContext);
				this.getView().bindElement(contextPath);

				if (!this.getView().getBindingContext()) {
					this.scrollToTop();
				}
			}

			// //do we have this context loaded in our model? We should always have a timeregistration entry
			// if (this.ExpandLoaded) { //this.getView().getBindingContext().getObject()) {

			// 	//if yes, refresh the model to reflect in memory model any changes done remotely to the order
			// 	this.getView().getBindingContext().getModel().refresh(); //using true as argument got strange errors to arise

			//Set edit mode
			if (this.getView().getBindingContext().getObject()) {
				this.updateEditModeModel(this.getView().getBindingContext().getObject().OrderStatus);
			}
			var eventBus = sap.ui.getCore().getEventBus();
			var data = {
				noteLongTextField: ""
			};

			eventBus.publish("longTextDisplayMode", data);
			this.getEventBus().publish("getPhotosondemand", {
				"view": "/OrderSet"
			});
			//eventBus.publish("photoBlock", "getPhotosondemand", {});
		},

		// setUserStatusTaskStarted: function(orderStatus) {
		// 	var message = "";
		// 	if (orderStatus === "INITIAL") {
		// 		message = this.getI18nText("WorkOrderDetails-orderStatusMessageNotStarted");
		// 	} else if (orderStatus === "INPROGRESS") {

		// 		if (this.timerIsRunningForOrder("completeorder")) {
		// 			return;
		// 		}
		// 		message = this.getI18nText("WorkOrderDetails-orderStatusMessageInProgress");
		// 	}

		// 	var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
		// 	var that = this;
		// 	sap.m.MessageBox.show(message, {
		// 		icon: sap.m.MessageBox.Icon.None,
		// 		title: this.getI18nText("WorkOrderDetails-orderStatusTitle"),
		// 		actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
		// 		defaultAction: sap.m.MessageBox.Action.NO,
		// 		styleClass: bCompact ? "sapUiSizeCompact" : "",
		// 		onClose: function(oAction, object) {
		// 			if (oAction === sap.m.MessageBox.Action.YES) {
		// 				// Set the order status to "inProgress" and post it
		// 				var newStatus = "";
		// 				if (orderStatus === "INITIAL") {
		// 					newStatus = "INPROGRESS";
		// 				}
		// 				// Set the order status to "completed" and post it
		// 				else if (orderStatus === "INPROGRESS") {
		// 					newStatus = "COMPLETED";
		// 				} else {
		// 					return;
		// 				}

		// 				var oContext = that.getView().getBindingContext();
		// 				that.getView().getModel().setProperty("OrderStatus", newStatus, oContext);
		// 				that.updateOrderStatus();
		// 			}
		// 		}
		// 	});
		// },

		getOrderPriorityColor: function (priorityText) {
			switch (priorityText) {
			case "1":
				return "Negative";
			case "2":
				return "Critical";
			case "3":
				return "Grey";
			case "4":
				return "Positive";
			default:
				return null;
			}
		},
		getProcessType: function (txt) {

			var sResult = "";

			if (txt == "Maintenance Order") {
				sResult = "General Maintenance";
			}
			if (txt == "Inspection Order") {
				sResult = "Inspection";
			}
			if (txt == "Planned Maintenance Order") {
				sResult = "Preventive Maintenance";
			}
			if (txt == "Corrective Maintenance Order") {
				sResult = "Corrective Maintenance";
			}

			return sResult;
		},

		isInspectionOrder: function (processType) {
			return processType === "Inspection Order" ? true : false;
		},

		isPlannedMaintenanceOrder: function (processType) {
			return processType === "Planned Maintenance Order" ? true : false;
		},

		isMaintenanceOrder: function (processType) {
			return (processType === "Planned Maintenance Order" || processType === "Corrective Maintenance Order") ? true : false;
		},

		isCorrectiveMaintenanceOrder: function (processType) {
			return processType === "Corrective Maintenance Order" ? true : false;
		},

		getHeaderIcon: function (processType) {
			return (processType === "Planned Maintenance Order" || processType === "Corrective Maintenance Order") ?
				"sap-icon://clinical-tast-tracker" : "sap-icon://search";
		},

		isLocationValid: function (latitude, longitude) {
			if (!isNaN(latitude) && Number(latitude) > 0) {
				return true
			};
			return false;
		}
	});
});