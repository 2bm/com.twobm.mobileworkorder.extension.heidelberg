sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History"
], function (Controller, JSONModel, History) {
	"use strict";
	return sap.ui.controller("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.structureBrowser.StructureBrowserCustom", {
		getFunctionalLocationswithoutParewnt: function () {
			this.currentSearchId = "";
			this.viewModel.length = 0;
			var filters = new Array();
			var filterByName = new sap.ui.model.Filter("ParentFuncLoc", sap.ui.model.FilterOperator.EQ, "");
			filters.push(filterByName);
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("FunctionalLocation", false, false));
			var url = "/FunctionalLocationsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function (result) {
					var results = result.results;
					var children = [];
					if (this.currentSearchId !== "")
						return;
					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];
						children[children.length] = {
							name: currentResult.FunctionalLocation,
							description: currentResult.Description,
							id: currentResult.FunctionalLocation,
							parentId: currentResult.ParentFuncLoc,
							leaf: currentResult.Isleaf,
							type: "FUNCTIONAL_LOCATION",
							level: 0,
							uniqueId: this.createGUID(),
							latitude: currentResult.Latitude,
							longitude: currentResult.Longitude,
							room:  currentResult.Room,
							sortfield:  currentResult.SortField
						};
						this.viewModel.splice(i, 0, children[i]);
					}
					this.getView().getModel("viewModel").refresh();
					this.getView().setBusy(false);
				}.bind(this),
				error: function (error) {
					this.getView().setBusy(false);
				}
			});
		},
		getFunctionalLocationsByParent: function (item) {
			var itemIndex = this.viewModel.findIndex(function (currentItem) {
				return currentItem.uniqueId === item.uniqueId;
			});
			var filters = new Array();
			var filterByName = new sap.ui.model.Filter("ParentFuncLoc", sap.ui.model.FilterOperator.EQ, item.id);
			filters.push(filterByName);
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("FunctionalLocation", false, false));
			var url = "/FunctionalLocationsSet";
			item.busy = true;
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function (result) {
					var results = result.results;
					var children = [];
					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];
						children[children.length] = {
							name: currentResult.FunctionalLocation,
							description: currentResult.Description,
							id: currentResult.FunctionalLocation,
							parentId: currentResult.ParentFuncLoc,
							uniqueParentId: item.uniqueId,
							leaf: currentResult.Isleaf,
							type: "FUNCTIONAL_LOCATION",
							level: item.level + 1,
							uniqueId: this.createGUID(),
							latitude: currentResult.Latitude,
							longitude: currentResult.Longitude,
							room:  currentResult.Room,
							sortfield:  currentResult.SortField
						};
						this.viewModel.splice(itemIndex + 1 + i, 0, children[i]);
					}
					this.getEquipmentByParentFunctionalLocation(item);
				}.bind(this),
				error: function (error) {
					item.busy = false;
				}
			});
		},

		getEquipmentByParentFunctionalLocation: function(item) {
			var itemIndex = this.viewModel.findIndex(function(currentItem) {
				return currentItem.id === item.id;
			});

			var filters = new Array();
			var filterByFLParent = new sap.ui.model.Filter("Funcloc", sap.ui.model.FilterOperator.EQ, item.id);
			filters.push(filterByFLParent);
			var filterByNoEQUParent = new sap.ui.model.Filter("ParentEquipment", sap.ui.model.FilterOperator.EQ, "");
			filters.push(filterByNoEQUParent);
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false, false));
			
			var url = "/EquipmentsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {
					var results = result.results;
					var children = [];

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.Equipment,
							description: currentResult.Description,
							id: currentResult.Equipment,
							parentId: currentResult.Funcloc,
							uniqueParentId: item.uniqueId,
							parentEquipmentId: currentResult.ParentEquipment,
							leaf: currentResult.Isleaf,
							type: "EQUIPMENT",
							level: item.level + 1,
							uniqueId: this.createGUID(),
							parentFunctionalLocationName: currentResult.Funcloc,
							parentFunctionalLocationdescription: currentResult.Funclocdesc,
							latitude: currentResult.Latitude,
							longitude: currentResult.Longitude,
							room:  currentResult.Room,
							sortfield:  currentResult.SortField
						};

						this.viewModel.splice(itemIndex + 1 + i, 0, children[i]);
					}

					item.busy = false;
					this.getView().getModel("viewModel").refresh();
				}.bind(this),
				error: function(error) {
					item.busy = false;
				}
			});
		},

		getEquipmentByParentEquipment: function(item) {
			var itemIndex = this.viewModel.findIndex(function(currentItem) {
				return currentItem.uniqueId === item.uniqueId;
			});

			var filters = new Array();
			var filterByEQParent = new sap.ui.model.Filter("ParentEquipment", sap.ui.model.FilterOperator.EQ, item.id);
			filters.push(filterByEQParent);
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false, false));
			
			var url = "/EquipmentsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {
					var results = result.results;
					var children = [];

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.Equipment,
							description: currentResult.Description,
							id: currentResult.Equipment,
							parentId: currentResult.Funcloc,
							uniqueParentId: item.uniqueId,
							parentEquipmentId: currentResult.ParentEquipment,
							leaf: currentResult.Isleaf,
							type: "EQUIPMENT",
							level: item.level + 1,
							uniqueId: this.createGUID(),
							parentFunctionalLocationName: currentResult.Funcloc,
							parentFunctionalLocationdescription: currentResult.Funclocdesc,
							latitude: currentResult.Latitude,
							longitude: currentResult.Longitude,
							room:  currentResult.Room,
							sortfield:  currentResult.SortField
						};

						this.viewModel.splice(itemIndex + 1 + i, 0, children[i]);
					}

					item.busy = false;
					this.getView().getModel("viewModel").refresh();
				}.bind(this),
				error: function(error) {
					item.busy = false;
				}
			});
		},

		searchStructure: function(sValue) {

			if (sValue === "") {

				this.getFunctionalLocationswithoutParewnt();
				return;
			} else {
				var searchId = this.createGUID();
				this.currentSearchId = searchId;
			}

			var searchString = sValue.toLowerCase();
			//this.getView().setBusy(true);
			var filters = new Array();
			filters.push(new sap.ui.model.Filter("Searchstring", sap.ui.model.FilterOperator.Contains, searchString));
			//var filterByName = new sap.ui.model.Filter("ParentFuncLoc", sap.ui.model.FilterOperator.EQ, "");
			//filters.push(filterByName);
			var url = "/FunctionalLocationsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				success: function(result) {
					var results = result.results;
					var children = [];
					this.viewModel.length = 0;

					if (searchId !== this.currentSearchId) {
						return;
					}

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.FunctionalLocation,
							description: currentResult.Description,
							id: currentResult.FunctionalLocation,
							parentId: currentResult.ParentFuncLoc,
							leaf: currentResult.Isleaf,
							type: "FUNCTIONAL_LOCATION",
							level: 0,
							uniqueId: this.createGUID(),
							latitude: currentResult.Latitude,
							longitude: currentResult.Longitude,
							room:  currentResult.Room,
							sortfield:  currentResult.SortField

						};

						this.viewModel.splice(i, 0, children[i]);
					}

					this.searchEquipment(searchString, searchId);
				}.bind(this),
				error: function(error) {
					this.getView().setBusy(false);
				}
			});
		},

		searchEquipment: function(searchString, searchId) {

			var filters = new Array();
			filters.push(new sap.ui.model.Filter("Searchstring", sap.ui.model.FilterOperator.Contains, searchString));
			
			var sorters = new Array();
			sorters.push(new sap.ui.model.Sorter("Description", false,false));
			
			var url = "/EquipmentsSet";
			this.getView().getModel().read(url, {
				filters: filters,
				sorters: sorters,
				success: function(result) {

					if (searchId !== this.currentSearchId) {
						if (this.currentSearchId === "")
							this.getView().setBusy(false);

						return;
					}

					var results = result.results;
					var children = [];

					for (var i = 0; i < results.length; i++) {
						var currentResult = results[i];

						children[children.length] = {
							name: currentResult.Equipment,
							description: currentResult.Description,
							id: currentResult.Equipment,
							parentId: currentResult.Funcloc,
							parentEquipmentId: currentResult.ParentEquipment,
							leaf: currentResult.Isleaf,
							type: "EQUIPMENT",
							level: 0,
							uniqueId: this.createGUID(),
							parentFunctionalLocationName: currentResult.Funcloc,
							parentFunctionalLocationdescription: currentResult.Funclocdesc,
							latitude: currentResult.Latitude,
							longitude: currentResult.Longitude,
							room:  currentResult.Room,
							sortfield:  currentResult.SortField

						};

						this.viewModel.splice(i, 0, children[i]);
					}

					this.viewModel.sort(function(itemA, itemB) {
						if (itemA.description > itemB.description)
							return 1;
						else if (itemA.description < itemB.description)
							return -1;
						else return 0;
					});

					this.getView().getModel("viewModel").refresh();
					this.getView().setBusy(false);
				}.bind(this),
				error: function(error) {
					this.getView().setBusy(false);
				}
			});
		},

		onDetailsPress: function (oEvent) {
			if (this.getView().getModel("viewModel").getProperty(oEvent.getSource().getBindingContext("viewModel").getPath()).type ===
				"EQUIPMENT") {
				var objecContext = "/EquipmentsSet('" + this.getView().getModel("viewModel").getProperty(oEvent.getSource().getBindingContext(
					"viewModel").getPath()).name + "')";

				setTimeout(function () {
					this.getRouter().navTo("equipmentDetails", {
						objectContext: objecContext.substr(1)
					}, false);
				}.bind(this, objecContext), 1);
			} else {
				var funcObjectContext = "/FunctionalLocationsSet('" + this.getView().getModel("viewModel").getProperty(oEvent.getSource().getBindingContext(
					"viewModel").getPath()).name + "')";

				setTimeout(function () {
					this.getRouter().navTo("functionalLocationDetails", {
						objectContext: funcObjectContext.substr(1)
					}, false);
				}.bind(this, funcObjectContext), 1);
			}
		},
	});
});