sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox"
], function (Controller, History, MessageBox) {
	"use strict";
	return sap.ui.controller("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.dashboard.DashboardCustom", {

		onPressNotifications: function () {
			setTimeout(function () {
				this.getRouter().navTo("notificationList", {}, false);
			}.bind(this), 1);
		},

		onPressCreateNotification: function () {
			setTimeout(function () {
				//Reset create notification model
				//this.getOwnerComponent().resetNewNotificationModel();
				this.getRouter().navTo("notificationCreate", {}, false);
			}.bind(this), 1);
		},

		onPressWorkorders: function () {
			setTimeout(function () {
				this.getRouter().navTo("workOrderList", {}, false);
			}.bind(this), 1);
		},

		onPressCreateWorkOrder: function () {
			setTimeout(function () {
				this.getRouter().navTo("workOrderCreate", {
					notificationId: "NONOTIFICATIONID" // nothing when entered from dashboard
				}, false);
			}.bind(this), 1);
		},

		onPressFunctionalLocations: function () {
			setTimeout(function () {
				this.getRouter().navTo("structureBrowser", {});
			}.bind(this), 1);
		},

		// getDashBoardLogo: function () {
		// 	return jQuery.sap.getModulePath("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension") + "/images/DashboardLogo.png";
		// },

		getDashboardTitle: function () {
			return jQuery.sap.getModulePath("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension") + "/images/DashboardLogo.png";
		}
	});
});