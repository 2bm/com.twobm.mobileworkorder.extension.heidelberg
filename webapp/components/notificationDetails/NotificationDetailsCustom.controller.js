sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/SimpleType",
	"sap/ui/model/ValidateException",
	"sap/ui/core/format/NumberFormat",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/m/Dialog"
], function (Controller, History, SimpleType, ValidateException, NumberFormat, MessageToast, Dialog, MessageBox) {
	"use strict";

	return sap.ui.controller(
		"com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.notificationDetails.NotificationDetailsCustom", {

			_onRouteMatched: function (oEvent) {
				var oArguments = oEvent.getParameter("arguments");
				var contextPath = '/' + oArguments.notificationContext;
				var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

				//this.oContext is the current context of the view
				//this context is the context that was set when the view was shown the last time
				//therefore the new contextPath can be different from the contextPath/context
				//that was shown the last time the view was shown
				if (!this.getView().getBindingContext() || this.getView().getBindingContext().getPath() !== contextPath) {
					//Reset model to the new context
					this.ExpandLoaded = false;
					//this.oContext = givenContext;
					this.getView().setBindingContext(givenContext);
					this.getView().bindElement(contextPath);

					if (!this.getView().getBindingContext()) {
						this.scrollToTop();
					}
				}
				this.getEventBus().publish("getPhotosondemand", {"view" : "/NotificationsSet"});
			},
			onNavigationButtonPress: function (oEvent) {
				var oHistory = History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
					window.history.go(-1);
				} else {
					/*var oRouter = this.getRouter();
					oRouter.navTo("notificationList", {}, true);*/
					var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
					oRouter.navTo("notificationList", true);
				}

				this.scrollToTop();
			},
		});
});