sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"sap/ui/model/Filter"
], function (Controller, Filter) {
	"use strict";
	return sap.ui.controller("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.notificationDetails.blocks.ItemsBlockCustom", {
		//    onInit: function () {
		//        this.createPopover();
		//    },
		//    createPopover: function () {
		//        if (!this.popover) {
		//            this.popover = sap.ui.xmlfragment("com.twobm.mobileworkorder.components.notificationDetails.fragments.ItemPopover", this);
		//            this.getView().setModel(new sap.ui.model.json.JSONModel({ isEditing: false }), "ViewModel");
		//            this.getView().addDependent(this.popover);
		//        }
		//    },
		//    onItemPress: function (oEvent) {
		//        var oBindingContext = oEvent.getSource().getBindingContext();
		//        var data = {
		//            "block": "item",
		//            "itemContext": oBindingContext.getPath().substr(1)
		//        };
		//        this.goToItemDetailsPage(data);
		//    },
		//    goToItemDetailsPage: function (data) {
		//        var eventBus = sap.ui.getCore().getEventBus();
		//        eventBus.publish("BlockNavigationNotification", data);
		//    },
		//    editItemPress: function (oEvent) {
		//        this.codeGroupSelected = "/CodeGroupsSet('B" + this.getView().getModel().getProperty(oEvent.getSource().getBindingContext() + "/DlCodegrp") + "')";
		//        this.damageCodeGroupSelected = "/CodeGroupsSet('C" + this.getView().getModel().getProperty(oEvent.getSource().getBindingContext() + "/DCodegrp") + "')";
		//        this.getView().getModel("ViewModel").setProperty("/isEditing", true);
		//        this.popover.setBindingContext(oEvent.getSource().getBindingContext());
		//        this.popover.open();
		//    },
		//    onCreate: function () {
		//        this.getView().getModel("ViewModel").setProperty("/isEditing", false);
		//        var path = this.getView().getBindingContext().getPath() + "/NotifItemsSet";
		//        this.newEntry = this.getView().getModel().createEntry(path, { properties: { ItemSortNo: "" } });
		//        this.popover.setBindingContext(this.newEntry);
		//        this.popover.open();
		//    },
		//    onSubmit: function () {
		//        this.popover.setBusy(true);
		//        if (this.getView().getModel().hasPendingChanges()) {
		//            this.getView().getModel().submitChanges({
		//                success: function () {
		//                    this.popover.setBusy(false);
		//                    if (this.newEntry) {
		//                        this.newEntry = null;
		//                    }
		//                    this.popover.close();
		//                }.bind(this),
		//                error: function (error) {
		//                    this.popover.setBusy(false);
		//                    self.errorCallBackShowInPopUp(error);
		//                }.bind(this)
		//            });
		//        } else {
		//            this.popover.setBusy(false);
		//            this.popover.close();
		//        }
		//    },
		//    onDelete: function () {
		//        this.popover.setBusy(true);
		//        this.getView().getModel().remove(this.popover.getBindingContext().getPath(), {
		//            success: function () {
		//                this.popover.setBusy(false);
		//                this.popover.close();
		//            }.bind(this),
		//            error: function (error) {
		//                this.popover.setBusy(false);
		//                self.errorCallBackShowInPopUp(error);
		//            }.bind(this)
		//        });
		//    },
		//    onPopopoverClose: function (oEvent) {
		//        var isEditing = this.getView().getModel("ViewModel").getProperty("/isEditing");
		//        var pendingChanges = this.getView().getModel().hasPendingChanges();
		//        if (isEditing && pendingChanges) {
		//            var sPathArr = [];
		//            var sPath = oEvent.getSource().getBindingContext().getPath();
		//            sPathArr.push(sPath);
		//            this.getView().getModel().resetChanges(sPathArr);
		//        }
		//        if (this.newEntry) {
		//            this.getView().getModel().deleteCreatedEntry(this.newEntry);
		//            this.newEntry = null;
		//        }
		//        this.popover.close();
		//    },

		handleValueHelpCodeGroup: function (oEvent) {
			if (!this.valueHelpCodeGroupDialog) {
				this.valueHelpCodeGroupDialog = sap.ui.xmlfragment(this.createId("ItemCodeGroup"),
					"com.twobm.mobileworkorder.components.notificationDetails.fragments.ItemPartCodeGroupValueHelp", this);
				this.setupCodeGroupSelectTemplate();
				this.getView().addDependent(this.valueHelpCodeGroupDialog);
			}
			var aFilters = [];
			aFilters.push(new sap.ui.model.Filter("CatalogType", sap.ui.model.FilterOperator.EQ, "B"));
			var aSorters = [];
			aSorters.push(new sap.ui.model.Sorter("Codegroup", false));
			this.getView().getModel().read("/CodeGroupsSet", {
				filters: aFilters,
				sorter: aSorters,
				success: function (oData, oResponse) {

					var comp = 'Codegroup';
					var arr = oData.results;

					//store the comparison values in array
					var unique = arr.map(e => e[comp]).
						// store the keys of the unique objects
					map((e, i, final) => final.indexOf(e) === i && i)
						// eliminate the dead keys & return unique objects
						.filter((e) => arr[e]).map(e => arr[e]);

					this.valueHelpCodeGroupDialog.setModel(new sap.ui.model.json.JSONModel({
						CodeGroupsSet: unique
					}));

					this.valueHelpCodeGroupDialog.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
						path: "/CodeGroupsSet",
						template: this.codeGroupSelectTemplate,
					});

				}.bind(this),

				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			});
			this.valueHelpCodeGroupDialog.open();
		},

		//    setupCodeGroupSelectTemplate: function () {
		//        this.codeGroupSelectTemplate = new sap.m.StandardListItem({
		//            title: "{CodegroupText}",
		//            description: "{Codegroup}",
		//            active: true
		//        });
		//    },
		//    handleValueHelpCode: function (oEvent) {
		//        if (!this.valueHelpCodeDialog) {
		//            this.valueHelpCodeDialog = sap.ui.xmlfragment(this.createId("ItemCode"), "com.twobm.mobileworkorder.components.notificationDetails.fragments.ItemPartCodeValueHelp", this);
		//            this.setupCodeSelectTemplate();
		//            this.getView().addDependent(this.valueHelpCodeDialog);
		//        }
		//        var aFilters = [];
		//        aFilters.push(new sap.ui.model.Filter("CodeCatGroup", sap.ui.model.FilterOperator.EQ, this.codeGroupSelected));
		//        var aSorters = [];
		//        aSorters.push(new sap.ui.model.Sorter("Code", false));
		//        this.valueHelpCodeDialog.getAggregation("_dialog").getContent()[1].bindAggregation("items", {
		//            path: "/CodeCatalogSet",
		//            template: this.codeSelectTemplate,
		//            filters: aFilters,
		//            sorter: aSorters
		//        });
		//        this.valueHelpCodeDialog.open();
		//    },
		//    setupCodeSelectTemplate: function () {
		//        this.codeSelectTemplate = new sap.m.StandardListItem({
		//            title: "{CodeText}",
		//            description: "{Code}",
		//            active: true
		//        });
		//    },
		handleDamageValueHelpCodeGroup: function (oEvent) {
			if (!this.valueHelpDamageCodeGroupDialog) {
				this.valueHelpDamageCodeGroupDialog = sap.ui.xmlfragment(this.createId("ItemDamageCodeGroup"),
					"com.twobm.mobileworkorder.components.notificationDetails.fragments.ItemDamageCodeGroupValueHelp", this);
				this.setupDamageCodeGroupSelectTemplate();
				this.getView().addDependent(this.valueHelpDamageCodeGroupDialog);
			}
			var aFilters = [];
			aFilters.push(new sap.ui.model.Filter("CatalogType", sap.ui.model.FilterOperator.EQ, "C"));
			var aSorters = [];
			aSorters.push(new sap.ui.model.Sorter("Codegroup", false));
			this.getView().getModel().read("/CodeGroupsSet", {
				filters: aFilters,
				sorter: aSorters,
				success: function (oData, oResponse) {

					var comp = 'Codegroup';
					var arr = oData.results;

					//store the comparison  values in array
					var unique = arr.map(e => e[comp]).
						// store the keys of the unique objects
					map((e, i, final) => final.indexOf(e) === i && i)
						// eliminate the dead keys & return unique objects
						.filter((e) => arr[e]).map(e => arr[e]);

					this.valueHelpDamageCodeGroupDialog.setModel(new sap.ui.model.json.JSONModel({
						CodeGroupsSet: unique
					}));

					this.valueHelpDamageCodeGroupDialog.getAggregation('_dialog').getContent()[1].bindAggregation("items", {
						path: "/CodeGroupsSet",
						template: this.damageCodeGroupSelectTemplate,
					});

				}.bind(this),

				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			});
			this.valueHelpDamageCodeGroupDialog.open();
		},
		//    setupDamageCodeGroupSelectTemplate: function () {
		//        this.damageCodeGroupSelectTemplate = new sap.m.StandardListItem({
		//            title: "{CodegroupText}",
		//            description: "{Codegroup}",
		//            active: true
		//        });
		//    },
		//    handleDamageValueHelpCode: function (oEvent) {
		//        if (!this.valueDamageHelpCodeDialog) {
		//            this.valueDamageHelpCodeDialog = sap.ui.xmlfragment(this.createId("ItemDamageCode"), "com.twobm.mobileworkorder.components.notificationDetails.fragments.ItemDamageCodeValueHelp", this);
		//            this.setupDamageCodeSelectTemplate();
		//            this.getView().addDependent(this.valueDamageHelpCodeDialog);
		//        }
		//        var aFilters = [];
		//        aFilters.push(new sap.ui.model.Filter("CodeCatGroup", sap.ui.model.FilterOperator.EQ, this.damageCodeGroupSelected));
		//        var aSorters = [];
		//        aSorters.push(new sap.ui.model.Sorter("Code", false));
		//        this.valueDamageHelpCodeDialog.getAggregation("_dialog").getContent()[1].bindAggregation("items", {
		//            path: "/CodeCatalogSet",
		//            template: this.damageCodeSelectTemplate,
		//            filters: aFilters,
		//            sorter: aSorters
		//        });
		//        this.valueDamageHelpCodeDialog.open();
		//    },
		//    setupDamageCodeSelectTemplate: function () {
		//        this.damageCodeSelectTemplate = new sap.m.StandardListItem({
		//            title: "{CodeText}",
		//            description: "{Code}",
		//            active: true
		//        });
		//    },
		//    handleValueHelpSearchCodeGroup: function (oEvent) {
		//        var sValue = oEvent.getParameter("value");
		//        var oFilter = new Filter("Codegroup", sap.ui.model.FilterOperator.Contains, sValue);
		//        oEvent.getSource().getBinding("items").filter([
		//            oFilter,
		//            new Filter("CatalogType", sap.ui.model.FilterOperator.EQ, "2")
		//        ]);
		//        oEvent.getSource().getBinding("items").refresh();
		//    },
		//    handleValueHelpCloseCodeGroup: function (oEvent) {
		//        var selectedItem = oEvent.getParameter("selectedItem");
		//        if (selectedItem) {
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/DlCodegrp", selectedItem.getDescription());
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/TxtGrpcd", selectedItem.getTitle());
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/DlCode", "");
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/TxtObjptcd", "");
		//            this.codeGroupSelected = selectedItem.getBindingContext().getObject().CodeCatGroup;
		//        }
		//    },
		//    handleValueHelpAfterCloseCodeGroup: function () {
		//        this.valueHelpCodeGroupDialog.destroy();
		//    },
		//    handleValueHelpCloseCode: function (oEvent) {
		//        var selectedItem = oEvent.getParameter("selectedItem");
		//        if (selectedItem) {
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/DlCode", selectedItem.getDescription());
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/TxtObjptcd", selectedItem.getTitle());
		//        }
		//    },
		//    handleValueHelpAfterCloseCode: function () {
		//    },
		//    handleValueDamageHelpCloseCodeGroup: function (oEvent) {
		//        var selectedItem = oEvent.getParameter("selectedItem");
		//        if (selectedItem) {
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/DCodegrp", selectedItem.getDescription());
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/StxtGrpcd", selectedItem.getTitle());
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/DCode", "");
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/TxtProbcd", "");
		//            this.damageCodeGroupSelected = selectedItem.getBindingContext().getObject().CodeCatGroup;
		//        }
		//    },
		//    handleValueDamageHelpAfterCloseCodeGroup: function () {
		//    },
		//    handleValueDamageHelpCloseCode: function (oEvent) {
		//        var selectedItem = oEvent.getParameter("selectedItem");
		//        if (selectedItem) {
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/DCode", selectedItem.getDescription());
		//            this.getView().getModel().setProperty(this.popover.getBindingContext().getPath() + "/TxtProbcd", selectedItem.getTitle());
		//        }
		//    },
		//    isItemCodeGrpEmpty: function (itemGroupValue) {
		//        if (itemGroupValue !== undefined && itemGroupValue !== "") {
		//            return true;
		//        } else {
		//            return false;
		//        }
		//    },
		//    isDamageCodeGrpEmpty: function (itemDamageGroupValue) {
		//        if (itemDamageGroupValue !== undefined && itemDamageGroupValue !== "") {
		//            return true;
		//        } else {
		//            return false;
		//        }
		//    }
	});
});