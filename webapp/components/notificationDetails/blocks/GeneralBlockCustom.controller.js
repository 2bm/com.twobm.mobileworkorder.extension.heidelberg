sap.ui.controller("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.notificationDetails.blocks.GeneralBlockCustom", {

		formatWorkCenter: function(name, text){
			var out = name;
			if(text){
				out = out + " - " + text;
			}
			return out;
		}

});