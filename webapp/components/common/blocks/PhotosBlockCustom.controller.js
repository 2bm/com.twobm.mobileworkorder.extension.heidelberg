sap.ui.define([
	"com/twobm/mobileworkorder/util/Controller",
	"com/twobm/mobileworkorder/controls/PictureViewer/PictureViewer",
	'sap/m/Button',
	'sap/m/Dialog'
], function (Controller, PictureViewer, Button, Dialog) {
	"use strict";

	return sap.ui.controller("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension.components.common.blocks.PhotosBlockCustom", {

		// onAfterRendering: function () {
		// 	var viewModel = this.getView().getModel("photoModel");
		// 	if (viewModel.getData().Items.length === 0) {
		// 		this.loadPhotos();
		// 	}
		// },
		onInit: function () {
			this.ImageModel = new sap.ui.model.json.JSONModel();
			this.createPhotoViewerPopover();
			var arry = [];
			this.photoModel = new sap.ui.model.json.JSONModel({
				"Items": arry,
				"Ondemand": false
			});
			this.getView().setModel(this.photoModel, "photoModel");
			this.getEventBus().subscribe("getPhotosondemand", this.loadPhotos, this);
			//Explicitly call the  first time as event was raised before we subscribed
			setTimeout(function () {
				this.loadPhotos();
			}.bind(this), 500);
			
		},
		loadPhotos: function (a, b, data) {
			var oModel = this.getView().getModel("photoModel");
			oModel.setProperty("/Items", "");
			oModel.refresh();
			var photoPromises = [this.getPhotos(oModel), this.getPhotosOnDemand(oModel)];
			Promise.all(photoPromises).then(function (values) {
				this.getView().byId("photosList").getBinding("items").refresh(true);
				oModel.refresh(true);
			}.bind(this));
		},

		getPhotos: function (photoModel) {
			return new Promise(function (resolve, reject) {
				var photoData = photoModel.getData();
				if (this.getView().getBindingContext()) {
					var sPath = this.getView().getBindingContext().getPath();
					var uri = sPath + "/PhotosSet";
					this.getView().setBusy(true);
					this.getView().getModel().read(uri, {
						success: function (data) {
							this.getView().setBusy(false);
							photoData.Items = data.results;
							photoModel.refresh();
							resolve();
						}.bind(this),
						error: function (error) {
							this.getView().setBusy(false);
							//this.errorCallBackShowInPopUp(error);
							reject();
						}.bind(this)
					});
				}
			}.bind(this));
		},

		getPhotosOnDemand: function (photoModel) {
			return new Promise(function (resolve, reject) {
				var photoData = photoModel.getData();
				if (this.getView().getBindingContext()) {
					var sPath = this.getView().getBindingContext().getPath();
					var uri = sPath + "/PhotosOnDemandSet";
					this.getView().setBusy(true);
					this.getView().getModel().read(uri, {
						success: function (data) {
							this.getView().setBusy(false);
							var oData = photoModel.getProperty("/Items");
							oData.push.apply(oData, data.results);
							if (this.getView().getModel("device").getData().isHybridApp) {
								if (data.results.length > 0) {
									photoData.Ondemand = true;
								}
							}
							photoModel.refresh();
							resolve();
						}.bind(this),
						error: function (error) {
							this.getView().setBusy(false);
							//this.errorCallBackShowInPopUp(error);
							reject();
						}.bind(this)
					});
				}
			}.bind(this));
		},


		itemFactory: function (sId, oContext) {
			var path = oContext.getObject().__metadata.media_src;
			var uri = oContext.getObject().__metadata.uri;
			var isHybrid = this.getView().getModel("device").getData().isHybridApp;
			if (!isHybrid) {
				path = path.replace(/^.*\/\/[^\/]+/, '');
			}
			var thumbimg = jQuery.sap.getModulePath("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension") + "/images/placeholder.png";

			var imagethumb = new sap.m.Image();
			imagethumb.setSrc(thumbimg);
			imagethumb.setWidth("8em");
			imagethumb.setHeight("8em");
			imagethumb.setDensityAware(false);
			imagethumb.addStyleClass("sapUiSmallMarginEnd");
			imagethumb.addStyleClass("sapUiSmallMarginTop");
			imagethumb.attachPress(function (oEvent) {
				this.downloadImage(oEvent);
			}.bind(this));

			var image = new sap.m.Image();
			image.setSrc(path);
			image.setWidth("8em");
			image.setHeight("8em");
			image.setDensityAware(false);
			image.addStyleClass("sapUiSmallMarginEnd");
			image.addStyleClass("sapUiSmallMarginTop");
			image.attachPress(function (oEvent) {
				this.clickPreviewPhoto(oEvent);
			}.bind(this));

			var mediaOff = oContext.getProperty("@com.sap.vocabularies.Offline.v1.mediaIsOffline");
			if (!mediaOff) {
				mediaOff = oContext.getProperty("downloadoffline");
			}
			//	return imagethumb;

			if (isHybrid) {
				if (!mediaOff) {
					return imagethumb;
				} else {
					return image;
				}
			} else {
				return image;
			}
		},

		clickPreviewPhoto: function (oEvent) {
			//var bindinContext = oEvent.getSource().getBindingContext();
			var bindingContext = oEvent.getSource().getBindingContext("photoModel");
			var uri = "/" + bindingContext.getObject().__metadata.uri.split("/").pop()
			this.getImageSrc(bindingContext.getObject())
				.then(function (imageSrc) {
					this.pictureViewer.setSourceSrc(imageSrc);
					this.pictureViewer.setDocImagePath(uri);
					this.pictureViewer.setImageFileName(bindingContext.getObject().Filename);

					// open dialog by use of fragment controller
					this.drawPhotoController.openDrawPhotoDialog();
				}.bind(this))
				.catch(function (error) {
					alert(error);
				}.bind(this));
		},
		getImageSrc: function (image) {
			return new Promise(function (resolve, reject) {
				var path = image.__metadata.media_src;
				if (sap.hybrid) {
					if (window.cordova.require("cordova/platform").id === "ios") {
						this.getFileContentAsBase64(path)
							.then(function (base64Image) {
								resolve(base64Image);
							})
							.catch(function (eror) {
								reject(error);
								this.errorCallBackShowInPopUp(error);
							}.bind(this))
					} else {
						resolve(path);
					}
				} else {
					resolve(path.replace(/^.*\/\/[^\/]+/, ''));
				}
			}.bind(this));
		},
		getFileContentAsBase64: function (path) {
			return new Promise(function (resolve, reject) {
				window.resolveLocalFileSystemURL(path, success, error);

				function error(e) {
					reject(error);
				}

				function success(fileEntry) {
					fileEntry.file(function (file) {
						var reader = new FileReader();
						reader.onloadend = function (e) {
							var content = this.result;
							resolve(content);
						};
						// The most important point, use the readAsDatURL Method from the file plugin
						reader.readAsDataURL(file);
					});
				}
			});
		},

		onPhotoUpload: function (imageAsBase64) {
			var isHybridApp = this.getView().getModel("device").getData().isHybridApp;

			var date = new Date();
			var month = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
			var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
			var hour = (date.getHours() < 10 ? "0" : "") + date.getHours();
			var min = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
			var sec = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
			var dateStr = day + month + date.getFullYear() + "_" + hour + min + sec;
			var userName = this.getView().getModel("appInfoModel").getData().UserName;

			var fileName = "Photo_" + userName + "_" + dateStr + ".jpg";

			var fileData = {
				Filename: fileName,
				Changedate: new Date(),
				Createdby: userName
			};

			var parameters = {
				headers: {
					"slug": fileName,
					"Mimetype": 'image/jpeg'
				},
				success: function (data) {
					var serviceUrl;
					var etag;
					if (data["@com.sap.vocabularies.Offline.v1.isLocal"] || data["@com.sap.vocabularies.Offline.v1.mediaIsOffline"]) {
						serviceUrl = data.__metadata.edit_media;
						etag = data.__metadata.media_etag;
					} else {
						var uri = "/" + data.__metadata.uri.split("/").pop();
						serviceUrl = this.getView().getModel().sServiceUrl + uri + "/$value";
						/*serviceUrl = this.getView().getModel().sServiceUrl + "/PhotosSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
							"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')/$value";*/
					}
					//Upload the file stream to local db
					this.sendUploadRequest(imageAsBase64, fileName, serviceUrl, etag);
					/*this.getPhotos();
					this.getPhotosOnDemand();*/
				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};

			//Create document entry
			var entityuri = this.getView().getBindingContext().getObject().__metadata.type.split(".").pop();
			if (entityuri === "Equipments" || entityuri === "FunctionalLocations") {
				var createPath = this.getView().getBindingContext().getPath() + "/PhotosOnDemandSet";
			} else {
				var createPath = this.getView().getBindingContext().getPath() + "/PhotosSet";
			}
			this.getView().getModel().create(createPath, fileData, parameters);
		},
		downloadImage: function (oEvent) {
			var downloadButton = oEvent.getSource();
			var data = oEvent.getSource().getBindingContext("photoModel").getObject();
			// var uri = "/PhotosOnDemandSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
			// 	"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')";
			var uri = "/" + data.__metadata.uri.split("/").pop();
			var fileUrl = this.getView().getModel().sServiceUrl + uri + "/$value";
			var loading = jQuery.sap.getModulePath("com.twobm.mobileworkorder.HeidelbergWorkOrderExtension") + "/images/loading.gif";
			if (this.getView().getModel("device").getData().isHybridApp) {
				var syncStatusModel = this.getView().getModel("syncStatusModel");

				if (!syncStatusModel.getData().Online) {
					sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-DownloadNotPossible"));
					return;
				}

				var streamID = data.Objky + data.Docnumber;
				sap.hybrid.store.registerStreamRequest(streamID, uri,
					function () {
						downloadButton.setBusy(true);
						downloadButton.setSrc(loading);
						sap.m.MessageToast.show("Loading.......");
						sap.hybrid.store.refresh(function (oData) {
								downloadButton.setBusy(false);
								sap.m.MessageToast.show("Finished!");
								var sPath = downloadButton.getBindingContext("photoModel").getPath();
								this.getView().getModel("photoModel").setProperty(sPath + "/downloadoffline", true);
								this.getView().byId("photosList").getBinding("items").refresh(true);
							}.bind(this),
							function (error) {
								//alert("Failed to download stream: " + error.Message);
							}.bind(this), [streamID]);
					}.bind(this),
					function (error) {
						//Stream has probably already been registered for offline handling
						this.getView().byId("photosList").getBinding("items").refresh(true);
					}.bind(this));
			} else {

				// Online in browser - just open the file link and the file will be downloaded by browser
				window.open(fileUrl);
			}
		},
		/*clickPreviewPhoto: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("photoModel").getObject().__metadata.media_src;
			var data = oEvent.getSource().getBindingContext("photoModel").getObject();
			if (!this.getView().getModel("device").getData().isHybridApp) {
				path = path.replace(/^.*\/\/[^\/]+/, '');
			}

			this.drawPhotoViewerPopover.getModel("ImageModel").getData().ImagePath = path;
			this.drawPhotoViewerPopover.getModel("ImageModel").refresh();
			this.pictureViewer.setSourceSrc(this.drawPhotoViewerPopover.getModel("ImageModel").getData().ImagePath);

			var uri = "/" + data.__metadata.uri.split("/").pop();

			// var uri = "/PhotosOnDemandSet(" + "Doctype='" + data.Doctype + "',Objky='" + data.Objky +
			// 	"',Docnumber='" + data.Docnumber + "',Docpart='" + data.Docpart + "',Docversion='" + data.Docversion + "')";
			this.pictureViewer.setDocImagePath(uri);
			this.pictureViewer.setImageFileName(data.Filename);

			// open dialog by use of fragment controller
			this.drawPhotoController.openDrawPhotoDialog();
		},*/

		downloadAllFiles: function (oEvent) {
			var downloadButtonAll = oEvent.getSource();
			var documentsPath = this.getView().byId("photosList").getBindingContext().getPath();

			var syncStatusModel = this.getView().getModel("syncStatusModel");

			if (!syncStatusModel.getData().Online) {
				sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-DownloadNotPossible"));
				return;
			}

			//set buy cursor and disable documents list
			downloadButtonAll.setBusy(true);
			//this.getView().byId("photosList").setShowOverlay(true);
			var newStreamsRegistered = false;
			var items = this.getView().getModel("photoModel").getData().Items;
			for (var i = 0; i < items.length; i++) {

				var myData = items[i]['@com.sap.vocabularies.Offline.v1.mediaIsOffline'];
				if (myData || items[i].IsUrl) {
					//nothing
				} else {
					var path = "/" + items[i].__metadata.uri.split("/").pop();
					items[i].downloadoffline = true;
					newStreamsRegistered = true;
					sap.hybrid.store.registerStreamRequest(items[i].Objky + items[i].Docnumber, path,
						function () {

						}.bind(this),
						function (error) {
							//Stream has probably already been registered for offline handling
						});
				}
			}
			if (newStreamsRegistered) {
				//Call store.refresh
				sap.hybrid.store.refresh(function (data) {
						this.getView().byId("photosList").getBinding("items").refresh(true);
						downloadButtonAll.setBusy(false);
						sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-AllFilesDownloaded"));
					}.bind(this),
					function (error) {
						downloadButtonAll.setBusy(false);
						this.getView().byId("photosList").getBinding("items").refresh(true);
						//					this.getView().byId("photosList").setShowOverlay(false);
						//console.log("Failed to download stream");
					}.bind(this));
			} else {
				downloadButtonAll.setBusy(false);
				//			this.getView().byId("photosList").setShowOverlay(false);
				sap.m.MessageToast.show(this.getI18nText("Common-DocumentsBlock-AllFilesDownloaded"));
			}

		},
		saveDrawedImage: function () {
			var docImagePath = this.pictureViewer.getDocImagePath();

			// check if document is newly created (it will contain a temporary id instead of docype, docnumber and docpar)
			if (!docImagePath.includes("Doctype") && !docImagePath.includes("Docnumber") && !docImagePath.includes("Docpar")) {
				// generate random string
				var date = new Date();
				var month = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
				var day = (date.getDate() < 10 ? "0" : "") + date.getDate();
				var hour = (date.getHours() < 10 ? "0" : "") + date.getHours();
				var min = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
				var sec = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
				var randomChageSetId = day + month + date.getFullYear() + "_" + hour + min + sec;
				// delete existing path
				this.getView().getModel().remove(docImagePath, {
					changeSetId: randomChageSetId,
				});

				// convert canvas to imageAsBase64
				this.pictureViewer.drawImage().then(function (imageAsBase64) {
					// create doc and upload image
					this.onPhotoUpload(imageAsBase64);
				}.bind(this));
			} else {
				// convert canvas to imageAsBase64
				this.pictureViewer.drawImage().then(function (imageAsBase64) {
					// update image in existing doc
					this.onUpdateDrawImageUpload(imageAsBase64, docImagePath);;
				}.bind(this));
			}

			// close dialog by use of fragment controller
			/*			this.getPhotos();
						this.getPhotosOnDemand()*/
			this.drawPhotoController.closeDrawPhotoDialog();
			//this.closeDrawPhotoViewDialog();
		},

		sendUploadRequest: function (imageData, fileName, serviceUrl, etag) {
			if (cordova.require("cordova/platform").id == "ios" && sap.Xhook && window.webkit && window.webkit.messageHandlers) {
				var headers = {};
				headers['Accept'] = "application/json";
				headers['X-CSRF-Token'] = this.getView().getModel().getSecurityToken();
				headers['Content-Type'] = "image/png";
				headers['if-match'] = etag;
				headers['content-encoding'] = "base64";
				headers['slug'] = fileName;
				sap.OData.request({
						requestUri: serviceUrl,
						method: "PUT",
						headers: headers,
						body: imageData
					},
					function (data, response) {
						this.loadPhotos();
						this.getView().byId("photosList").getBinding("items").refresh();
					}.bind(this),
					function (error) {
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				);
			} else {
				var xhr = new XMLHttpRequest();
				xhr.open("PUT", serviceUrl, false);
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("content-type", "image/png"); // Not used by backend, but necessary for transport to backend
				xhr.setRequestHeader("content-encoding", "base64");
				xhr.setRequestHeader("x-csrf-token", this.getView().getModel().getSecurityToken());
				xhr.setRequestHeader("slug", fileName);
				xhr.setRequestHeader("if-match", etag);
				xhr.onreadystatechange = function () {
					if (xhr.readyState === 4) {
						if (xhr.status === 201 || xhr.status === 204) {
							this.loadPhotos();
							this.getView().byId("photosList").getBinding("items").refresh();
						} else {
							// TODO: Add error handling
						}
					}
				}.bind(this);

				if (sap.hybrid) {
					xhr.send(imageData);
				} else {
					xhr.send(this.createBlob(imageData));
				}
			}
		},
		createBlob: function (base64Str) {
			var byteCharacters = atob(base64Str);
			var byteNumbers = new Array(byteCharacters.length);
			for (var i = 0; i < byteCharacters.length; i++) {
				byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			const byteArray = new Uint8Array(byteNumbers);
			return new Blob([byteArray], {
				type: "image/jpeg"
			});
		},

		isOnDemand: function (onDemand) {
			if (onDemand) {
				return true;
			}
			return false;
		},

	});
});