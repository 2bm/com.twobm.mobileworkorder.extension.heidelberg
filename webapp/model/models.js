sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {
		createDeviceModel: function () {
			Device.isNotPhone = !Device.system.phone;
			Device.isPhone = Device.system.phone;
			Device.isHybridApp = this.getIsHybridApp();
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createSyncModel: function () {
			var oModel = new JSONModel({
				SyncColor: "",
				SyncIcon: "sap-icon://overlay",
				LastSyncTime: "",
				Online: false,
				PendingLocalData: false,
				IsSynching: false,
				InErrorState: false,
				Errors: [],
				OrderErrors: [],
				NoticationErrors: [],
				ErrorListContextObject: "", //The object to limit the errors shown in the error list. For instance Order
				ErrorListContextID: "", // The object ID to limit the errors shown in the error list. For instance Order ID
				WentOfflineTime: null
			});

			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		},

		createAppInfoModel: function () {
			var oModel = new JSONModel({
				AppVersion: "AppVersion",
				AppName: "AppName",
				WebSite: "http://www.mobileworkorder.info/",
				UserName: "",
				UserFirstName: "",
				UserFullName: "",
				UserPosition: "",
				UserImage: "",
				Persno: "",
				UILanguage: sap.ui.getCore().getConfiguration().getLanguage(),
				UILanguageText: "",
				HasNoImage: false
			});

			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createNewNotificationModel: function () {
			var oModel = new JSONModel({
				notificationPath: "",
				notificationShortText: "",
				notificationLongText: "",
				workCenterSelectedText: "",
				workCenterSelectedID: "",
				notificationTypeSelectedType: "",
				notificationTypeSelectedText: "",
				notificationPrioritySelectedType: "",
				notificationPrioritySelectedText: "",
				photos: [],
				equipmentNo: "",
				equipmentDesc: "",
				functionalLoc: "",
				funcLocDesc: "",
				longText: "",
				shortText: "",
				breakdownIndicator: false
			});

			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		},

		createTimeRegistrationTimerModel: function () {
			var oModel = new JSONModel({
				Started: false,
				OrderId: "",
				OrderIdNumber: "",
				OrderShortText: "",
				StartDateTime: ""
			});

			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		},

		createLanguagesModel: function () {
			var oModel = new JSONModel({
				Languages: [{
					LanguageCode: ["ar", "ar"],
					LanguageText: "اللغة العربية",
					Image: "/images/flags/allwhite.png",
					RTL: true
				}, {
					LanguageCode: ["cs-CZ", "cs-cz"],
					LanguageText: "Český",
					Image: "/images/flags/cz.png",
					RTL: false
				}, {
					LanguageCode: ["zh-CN", "zh-cn"],
					LanguageText: "简体中文",
					Image: "/images/flags/zh.png",
					RTL: false
				}, {
					LanguageCode: ["da-DK", "da-dk"],
					LanguageText: "Dansk",
					Image: "/images/flags/da.png",
					RTL: false
				}, {
					LanguageCode: ["de-DE", "de-de"],
					LanguageText: "Deutsch",
					Image: "/images/flags/de.png",
					RTL: false
				}, {
					LanguageCode: ["en-UK", "en-uk"],
					LanguageText: "English (UK) ",
					Image: "/images/flags/uk.png",
					RTL: false
				}, {
					LanguageCode: ["en-US", "en-us"],
					LanguageText: "English (US)",
					Image: "/images/flags/us.png",
					RTL: false
				}, {
					LanguageCode: ["es-ES", "es-es"],
					LanguageText: "Español (Spain)",
					Image: "/images/flags/es.png",
					RTL: false
				}, {
					LanguageCode: ["es-MX", "es-mx"],
					LanguageText: "Español (México)",
					Image: "/images/flags/mx.png",
					RTL: false
				}, {
					LanguageCode: ["fr-FR", "fr-fr"],
					LanguageText: "Française",
					Image: "/images/flags/fr.png",
					RTL: false
				},{
					LanguageCode: ["hu-HU", "hu-hu"],
					LanguageText: "Magyar",
					Image: "/images/flags/hu.png",
					RTL: false
				}, {
					LanguageCode: ["nb-NO", "nb-no"],
					LanguageText: "Norsk bokmål",
					Image: "/images/flags/no.png"
				}, {
					LanguageCode: ["sv-SE", "sv-se"],
					LanguageText: "Svenska",
					Image: "/images/flags/se.png"
				}, {
					LanguageCode: ["ro-RO", "ro-ro"],
					LanguageText: "Românesc",
					Image: "/images/flags/ro.png"
				}, {
					LanguageCode: ["pl-PL", "pl-pl"],
					LanguageText: "Polski",
					Image: "/images/flags/pl.png"
				}, {
					LanguageCode: ["nl-NL", "nl-nl"],
					LanguageText: "Nederlands",
					Image: "/images/flags/nl.png"
				}]
			});

			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createOnlineModel: function(){
				return new sap.ui.model.odata.v2.ODataModel({
					serviceUrl: fiori_client_appConfig.fioriURL + "/com.twobm.mobileworkorder.online" + sap.hybrid.servicePath
				});	
		},

		getIsHybridApp: function () {
			if (sap.hybrid) {
				return true;
			}
			return false;
		},
		
		createChecklistModel: function () {
			var model = new JSONModel({
				Checklists: [{
					Id: "1",
					Title: "Test",
					Description: "Empty test checklist",
					Revisions: [{
						Revision: "1",
						Created: "Feb 14, 2020",
						CreatedBy: "John Johnson (JJO)",
						Status: "Active"
					}],
					Sections: [{
						SectionTitle: "Loading ...",
						Questions: []
					}]
				}, {
					Id: "2",
					Title: "Engine Overhaul",
					Description: "Checlist for engine overhaul. To be performed after each engine overhaul to comply with regulatory diretive AAX5432.",
					Revisions: [{
						Revision: "3",
						Created: "Feb 14, 2020",
						CreatedBy: "John Johnson (JJO)",
						Status: "Active"
					}, {
						Revision: "2",
						Created: "Jan 25, 2020",
						CreatedBy: "John Johnson (JJO)",
						Status: ""
					}, {
						Revision: "1",
						Created: "Dec 2, 2019",
						CreatedBy: "John Johnson (JJO)",
						Status: ""
					}],
					Responses: [{

						Created: "Feb 14, 2020",
						CreatedBy: "John Johnson (JJO)",
						OrderNumber: "20004392"
					}, {

						Created: "Jan 25, 2020",
						CreatedBy: "John Johnson (JJO)",
						OrderNumber: "20004392"
					}, {

						Created: "Dec 2, 2019",
						CreatedBy: "John Johnson (JJO)",
						OrderNumber: "20004392"
					}],
					AssignmentParameters: [{
						Parameter: "Order Type",
						Value: "PM1",
						Description: "Planned Maintenance Order"
					}, {
						Parameter: "PM Activity Type",
						Value: "103",
						Description: "Engine Overhaul"
					}],
					Sections: [{
						"SectionTitle": "A Machine Details",
						"Questions": [{
							"QuestionHeader": "Standard Question",
							"QuestionTitle": "General Information",
							"QuestionType": "Standard",
							"Fields": [{
								"type": "header",
								"title": "General"
							}, {
								"type": "input",
								"title": "Manufacturer"
							}, {
								"type": "input",
								"title": "Serial number"
							}, {
								"type": "input",
								"title": "Manuf. year"
							}, {
								"type": "header",
								"title": "Motor Type"
							}, {
								"type": "radio",
								"title": "",
								"RadioButtons": [{
									"Key": "Ex",
									"Title": "Ex"
								}, {
									"Key": "Industrial",
									"Title": "Industrial"
								}]
							}, {
								"type": "header",
								"title": "Ex Protection"
							}, {
								"type": "radio",
								"title": "",
								"RadioButtons": [{
									"Key": "d",
									"Title": "d"
								}, {
									"Key": "de",
									"Title": "de"
								}, {
									"Key": "e",
									"Title": "e"
								}, {
									"Key": "N",
									"Title": "N"
								}, {
									"Key": "nA",
									"Title": "nA"
								}]
							}, {
								"type": "header",
								"title": "Standard"
							}, {
								"type": "radio",
								"title": "",
								"RadioButtons": [{
									"Key": "IEC",
									"Title": "IEC"
								}, {
									"Key": "BS",
									"Title": "BS"
								}, {
									"Key": "EN",
									"Title": "EN"
								}, {
									"Key": "UL",
									"Title": "UL"
								}]
							}, {
								"type": "header",
								"title": "Motor Group"
							}, {
								"type": "radio",
								"title": "",
								"RadioButtons": [{
									"Key": "I",
									"Title": "I"
								}, {
									"Key": "II",
									"Title": "II"
								}]
							}, {
								"type": "header",
								"title": "Gas Group"
							}, {
								"type": "radio",
								"title": "",
								"RadioButtons": [{
									"Key": "A",
									"Title": "A"
								}, {
									"Key": "B",
									"Title": "B"
								}, {
									"Key": "C",
									"Title": "C"
								}]
							}, {
								"type": "header",
								"title": "More Details"
							}, {
								"type": "input",
								"title": "Power",
								"unit": "kW/Hp"
							}, {
								"type": "input",
								"title": "Volt",
								"unit": "V"
							}, {
								"type": "input",
								"title": "Amp",
								"unit": "A"
							}, {
								"type": "input",
								"title": "RPM"
							}, {
								"type": "input",
								"title": "Temp. Rise"
							}, {
								"type": "input",
								"title": "Category"
							}, {
								"type": "input",
								"title": "DE Bearing"
							}, {
								"type": "input",
								"title": "NDE Bearing"
							}, {
								"type": "input",
								"title": "Temp. Class"
							}, {
								"type": "input",
								"title": "Frame Size"
							}, {
								"type": "input",
								"title": "IP Rating"
							}, {
								"type": "input",
								"title": "Frequency ",
								"unit": "Hz"
							}, {
								"type": "input",
								"title": "Insulation Class"
							}, {
								"type": "input",
								"title": "Mounting Code"
							}],
							"Points": []
						}]
					}, {
						SectionTitle: "B Initial Inspection (As Received)",
						Questions: [{
							"QuestionType": "Standard",
							"QuestionTitle": "B1 Insulator Resistance",
							"Fields": [{
								"type": "header",
								"title": "Phase To Earth"
							}, {
								"type": "input",
								"title": "U1-E",
								"unit": "M Ohm"
							}, {
								"type": "input",
								"title": "V1-E",
								"unit": "M Ohm"
							}, {
								"type": "input",
								"title": "W1-E",
								"unit": "M Ohm"
							}, {
								"type": "header",
								"title": "Phase To Phase"
							}, {
								"type": "input",
								"title": "U1-V1",
								"unit": "M Ohm"
							}, {
								"type": "input",
								"title": "V1-W1",
								"unit": "M Ohm"
							}, {
								"type": "input",
								"title": "W1-U1",
								"unit": "M Ohm"
							}]
						}, {
							"QuestionType": "Standard",
							"QuestionTitle": "B2 Stator Winding Resistance",
							"Fields": [{
								"type": "header",
								"title": "Phase To Phase"
							}, {
								"type": "input",
								"title": "U1-U2 to U1-V1",
								"unit": "Ohm"
							}, {
								"type": "input",
								"title": "V1-V2 to V1-W1",
								"unit": "Ohm"
							}, {
								"type": "input",
								"title": "W1-W2 to W1-U1",
								"unit": "Ohm"
							}]
						}]
					}, {
						SectionTitle: "C Detailed Visual Inspection (As Received)",
						Questions: [{
							"QuestionHeader": "Standard Question",
							"QuestionTitle": "1 - Stator Frame",
							"QuestionType": "Standard",
							"Fields": [{
								"type": "header",
								"title": "1 - Stator Frame"
							}, {
								"type": "radio",
								"title": "Field1",
								"RadioButtons": [{
									"Key": "Good",
									"Title": "Good"
								}, {
									"Key": "Damaged",
									"Title": "Damaged"
								}, {
									"Key": "N/A",
									"Title": "N/A"
								}]
							}, {
								"type": "header",
								"title": "Damage Description"
							}, {
								"type": "comment",
								"title": "Field1"
							}, {
								"type": "header",
								"title": "Planned Action"
							}, {
								"type": "comment",
								"title": "Field1"

							}]
						}, {
							"QuestionHeader": "Standard Question",
							"QuestionTitle": "2 - Coupling / Sheave / Impeller / Gear",
							"QuestionType": "Standard",
							"Fields": [{
								"type": "header",
								"title": "2 - Coupling / Sheave / Impeller / Gear"
							}, {
								"type": "radio",
								"title": "Field1",
								"RadioButtons": [{
									"Key": "Good",
									"Title": "Good"
								}, {
									"Key": "Damaged",
									"Title": "Damaged"
								}, {
									"Key": "N/A",
									"Title": "N/A"
								}]
							}, {
								"type": "header",
								"title": "Damage Description"
							}, {
								"type": "comment",
								"title": "Field1"
							}, {
								"type": "header",
								"title": "Planned Action"
							}, {
								"type": "comment",
								"title": "Field1"

							}]
						}, {
							"QuestionHeader": "Standard Question",
							"QuestionTitle": "3 - Mounting / Footing",
							"QuestionType": "Standard",
							"Fields": [{
								"type": "header",
								"title": "3 - Mounting / Footing"
							}, {
								"type": "radio",
								"title": "Field1",
								"RadioButtons": [{
									"Key": "Good",
									"Title": "Good"
								}, {
									"Key": "Damaged",
									"Title": "Damaged"
								}, {
									"Key": "N/A",
									"Title": "N/A"
								}]
							}, {
								"type": "header",
								"title": "Damage Description"
							}, {
								"type": "comment",
								"title": "Field1"
							}, {
								"type": "header",
								"title": "Planned Action"
							}, {
								"type": "comment",
								"title": "Field1"

							}]
						}]
					}, {
						"SectionTitle": "G - Rotor Straightness Check",
						"Questions": [{
							"QuestionHeader": "Point On Image Question",
							"QuestionTitle": "Rotor Shaft Dimensions Check",
							"QuestionType": "PointOnImage",
							"Fields": [{
								"type": "header",
								"title": "Dimensions"
							}, {
								"type": "input",
								"title": "FieldOD (x-axis)",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "OD (y-axis)",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "Horizontal Length",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "Run-Out",
								"unit": "mm"
							}],
							"Points": [{
								"CordX": 207.70000000000002,
								"CordY": 172.05,
								"Number": 2,
								"Active": false,
								"Fields": []
							}, {
								"CordX": 72.85000000000001,
								"CordY": 173.6,
								"Number": 1,
								"Active": true,
								"Fields": [{
									"type": "header",
									"title": "Dimensions"
								}, {
									"type": "input",
									"title": "FieldOD (x-axis)",
									"unit": "mm"
								}, {
									"type": "input",
									"title": "OD (y-axis)",
									"unit": "mm"
								}, {
									"type": "input",
									"title": "Horizontal Length",
									"unit": "mm"
								}, {
									"type": "input",
									"title": "Run-Out",
									"unit": "mm"
								}]
							}],
							"Image": "/9j/4QjTRXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAAAExAAIAAAAhAAAAcgEyAAIAAAAUAAAAk4dpAAQAAAABAAAAqAAAAPAAFfkAAAAnEAAV+QAAACcQQWRvYmUgUGhvdG9zaG9wIDIxLjAgKE1hY2ludG9zaCkAMjAyMDowMjoyMyAxNDowOTowNQAAAASShgAHAAAAEgAAAN6gAQADAAAAAf//AACgAgAEAAAAAQAABOegAwAEAAAAAQAAAV4AAAAAQVNDSUkAAABTY3JlZW5zaG90AAYBAwADAAAAAQAGAAABGgAFAAAAAQAAAT4BGwAFAAAAAQAAAUYBKAADAAAAAQACAAACAQAEAAAAAQAAAU4CAgAEAAAAAQAAB30AAAAAAAAASAAAAAEAAABIAAAAAf/Y/+0ADEFkb2JlX0NNAAH/7gAOQWRvYmUAZIAAAAAB/9sAhAAMCAgICQgMCQkMEQsKCxEVDwwMDxUYExMVExMYEQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMAQ0LCw0ODRAODhAUDg4OFBQODg4OFBEMDAwMDBERDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAtAKADASIAAhEBAxEB/90ABAAK/8QBPwAAAQUBAQEBAQEAAAAAAAAAAwABAgQFBgcICQoLAQABBQEBAQEBAQAAAAAAAAABAAIDBAUGBwgJCgsQAAEEAQMCBAIFBwYIBQMMMwEAAhEDBCESMQVBUWETInGBMgYUkaGxQiMkFVLBYjM0coLRQwclklPw4fFjczUWorKDJkSTVGRFwqN0NhfSVeJl8rOEw9N14/NGJ5SkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2N0dXZ3eHl6e3x9fn9xEAAgIBAgQEAwQFBgcHBgU1AQACEQMhMRIEQVFhcSITBTKBkRShsUIjwVLR8DMkYuFygpJDUxVjczTxJQYWorKDByY1wtJEk1SjF2RFVTZ0ZeLys4TD03Xj80aUpIW0lcTU5PSltcXV5fVWZnaGlqa2xtbm9ic3R1dnd4eXp7fH/9oADAMBAAIRAxEAPwD1VJJJJSkkkklKSSWNm/WbFw8qzGfRc51Ttpc304J2tt032td9Gz91KlOwhHIG97Gse8sgOIAiSN35xb+aqfS8qrqdVuU0WsabC1rHPIIDWs7UvLPpe5WcZoZdkNExvadSSfoM/elJSzS+7IsaTZU1jWQ3Qakvl2m/wRPs/wDwtn+d/sTM/pdv9Sv8tqMkpF9n/wCFs/zv9iX2f/hbP85FSSUi+zjvZYf7ZH/U7UPKqFeLc9j7A5lbnNO93IBI5crKDm/0O/8A4t//AFJSUp+S9jHPdQ+Ggk6s4Gv+kRgZAPih5P8ARrf6jvyFCGLjmjRgBLPPwSU2UlzNP1ucKawcTXYObfL/AIpdBj5Vd2PVcSGeqxr9pcDG4B0I0VJk6H61X77fvCmCCAQZB4IQU//Q9RvtNVe8DcZa0AmBLnBnMO/eVOzLtyMfLa2vaKS6p5fvbJADnOq3Vt9Wv3/zjfZvVy+s217GkNMtcCRIlpD9RLf3UHLbk/ZbZew+x2gYZ47fpElJPsmN/omf5oQcUY+R6u7DNHpWuqHqMaN4b/hqud1Nn5iP9qx/3wm+1Y/7/wCB/uS1Ur7Ji/6Fn+aFwn1kezH6vktYw7Tcxu1ggDdXT+kP8hv5y7v7VR+9+B/uXD/WEtf1nKc0kgvaBA/4KnxTo7oLtfVrJdj9DyL2VPvNdzyKaxL3aV6MW0x5bZkvDS4ja7YOT7B7f6yzPqhp023Xd+ndqe/trWtT/SMj4s/6lA7pCPFsdbabXMdU6ymlxrd9Jpd6p2P/AJTVaQW/0y3/AIuv/qrkZBSPJtfTRZbXW697GktqaQC4jhjS72+5ESSSUpCzP6Hf/wAW/wD6koqFmf0S/wD4t/8A1JSUituuc/IodSW1NqDmXkgteXB+6sN+k309qTLr/UrpFBNDqdxydwgO0Ap9P+c9zffvR7v5h/8AUP5E1P8AMV/1B+RJT5wywtbUNhLS2HOn6MN9vb3b3e1d/wBPrq/ZuM702uPoVmIEn2NXBMa302gucTtHfnRd303IrHTsUQ/+Zr/wb/3G/wAhOkgNmljX1Me+ltb3NBdXodpI9zNzfpbU2EAMdoAgAuED+s5P9pr/AHbP+23/APkE2GCKACC0y4wdDBc5wTUv/9H0S63Ia3MvN7mVYpJFdbGOJa2tlp2+p9J7tzlQPXGitlhszIsL2tHoVyDWWMfv9vsb+l/Rvf8Azn5i0H+j692z7Ru3j1PT3bd21nH/AFv00vZ3+1/9P+CSkVOTfdg1Z9d9wZY5hFV1bGOhz21ubawMD63bf8xaqznfZYHq/adm5v0/U2zubs3f29q0UlKWRm/VvCzcmzJttua+0gua0t2iGtr9u6t/5ta10yQvopz+m4tfTGXYzBa9hs3tc5u4kFlf51bWt+k1WMd4ffkEAjVn0gR+b5qwqtnoes/+d36b/T3xx7foe3dtSUkZ/TLf+Lr/AOquRlUxtv2q7bvj06/5zdP0rv8ASK3qkpSSbXw/FL3eA+//AGJKXQsv+iXf8W78hRJf4D7/APYhZZf9lu0H827uf3T/ACUlM360O82n8iDTdb9mZtpsJ2CPoeH/ABigPsvpjd6u3brPrcQrbNuxuyNkDbHEfmpKcir6r9L9FgsZYHhoDh6r+Y1/ORLvVxsfINDb7m4eyunGoLdxaGV+1nqD32e/8+xaioZH7N+0P9Xd62nqbPU8PZv9H2btiOvVTnftHONb7PsPUPa5rQ2W7nB4tdub7f8AB+kxlv8Axv8Ao/0llrFtybsf7RYzIxXtvrrFdzgdzS+prn7do/R2Nsf/AC/+LtU/8k/8J8/XTs/Y/qMj6e5uzf6kbpHp/wA57d2/6H8tJT//2f/tEPBQaG90b3Nob3AgMy4wADhCSU0EBAAAAAAABxwCAAACAAAAOEJJTQQlAAAAAAAQ6PFc8y/BGKGie2etxWTVujhCSU0EOgAAAAAA5QAAABAAAAABAAAAAAALcHJpbnRPdXRwdXQAAAAFAAAAAFBzdFNib29sAQAAAABJbnRlZW51bQAAAABJbnRlAAAAAENscm0AAAAPcHJpbnRTaXh0ZWVuQml0Ym9vbAAAAAALcHJpbnRlck5hbWVURVhUAAAAAQAAAAAAD3ByaW50UHJvb2ZTZXR1cE9iamMAAAAMAFAAcgBvAG8AZgAgAFMAZQB0AHUAcAAAAAAACnByb29mU2V0dXAAAAABAAAAAEJsdG5lbnVtAAAADGJ1aWx0aW5Qcm9vZgAAAAlwcm9vZkNNWUsAOEJJTQQ7AAAAAAItAAAAEAAAAAEAAAAAABJwcmludE91dHB1dE9wdGlvbnMAAAAXAAAAAENwdG5ib29sAAAAAABDbGJyYm9vbAAAAAAAUmdzTWJvb2wAAAAAAENybkNib29sAAAAAABDbnRDYm9vbAAAAAAATGJsc2Jvb2wAAAAAAE5ndHZib29sAAAAAABFbWxEYm9vbAAAAAAASW50cmJvb2wAAAAAAEJja2dPYmpjAAAAAQAAAAAAAFJHQkMAAAADAAAAAFJkICBkb3ViQG/gAAAAAAAAAAAAR3JuIGRvdWJAb+AAAAAAAAAAAABCbCAgZG91YkBv4AAAAAAAAAAAAEJyZFRVbnRGI1JsdAAAAAAAAAAAAAAAAEJsZCBVbnRGI1JsdAAAAAAAAAAAAAAAAFJzbHRVbnRGI1B4bEBiAAAAAAAAAAAACnZlY3RvckRhdGFib29sAQAAAABQZ1BzZW51bQAAAABQZ1BzAAAAAFBnUEMAAAAATGVmdFVudEYjUmx0AAAAAAAAAAAAAAAAVG9wIFVudEYjUmx0AAAAAAAAAAAAAAAAU2NsIFVudEYjUHJjQFkAAAAAAAAAAAAQY3JvcFdoZW5QcmludGluZ2Jvb2wAAAAADmNyb3BSZWN0Qm90dG9tbG9uZwAAAAAAAAAMY3JvcFJlY3RMZWZ0bG9uZwAAAAAAAAANY3JvcFJlY3RSaWdodGxvbmcAAAAAAAAAC2Nyb3BSZWN0VG9wbG9uZwAAAAAAOEJJTQPtAAAAAAAQAJAAAAABAAIAkAAAAAEAAjhCSU0EJgAAAAAADgAAAAAAAAAAAAA/gAAAOEJJTQQNAAAAAAAEAAAAHjhCSU0EGQAAAAAABAAAAB44QklNA/MAAAAAAAkAAAAAAAAAAAEAOEJJTScQAAAAAAAKAAEAAAAAAAAAAjhCSU0D9QAAAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+AAAAAAAcAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAA4QklNBAAAAAAAAAIAADhCSU0EAgAAAAAAAgAAOEJJTQQwAAAAAAABAQA4QklNBC0AAAAAAAYAAQAAAAI4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAADdwAAAAYAAAAAAAAAAAAAAV4AAATnAAAAIQBTAGMAcgBlAGUAbgBzAGgAbwB0ACAAMgAwADIAMAAtADAAMgAtADIAMwAgAGEAdAAgADEANAAuADAANAAuADUANgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAE5wAAAV4AAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAQAAAAAAAG51bGwAAAACAAAABmJvdW5kc09iamMAAAABAAAAAAAAUmN0MQAAAAQAAAAAVG9wIGxvbmcAAAAAAAAAAExlZnRsb25nAAAAAAAAAABCdG9tbG9uZwAAAV4AAAAAUmdodGxvbmcAAATnAAAABnNsaWNlc1ZsTHMAAAABT2JqYwAAAAEAAAAAAAVzbGljZQAAABIAAAAHc2xpY2VJRGxvbmcAAAAAAAAAB2dyb3VwSURsb25nAAAAAAAAAAZvcmlnaW5lbnVtAAAADEVTbGljZU9yaWdpbgAAAA1hdXRvR2VuZXJhdGVkAAAAAFR5cGVlbnVtAAAACkVTbGljZVR5cGUAAAAASW1nIAAAAAZib3VuZHNPYmpjAAAAAQAAAAAAAFJjdDEAAAAEAAAAAFRvcCBsb25nAAAAAAAAAABMZWZ0bG9uZwAAAAAAAAAAQnRvbWxvbmcAAAFeAAAAAFJnaHRsb25nAAAE5wAAAAN1cmxURVhUAAAAAQAAAAAAAG51bGxURVhUAAAAAQAAAAAAAE1zZ2VURVhUAAAAAQAAAAAABmFsdFRhZ1RFWFQAAAABAAAAAAAOY2VsbFRleHRJc0hUTUxib29sAQAAAAhjZWxsVGV4dFRFWFQAAAABAAAAAAAJaG9yekFsaWduZW51bQAAAA9FU2xpY2VIb3J6QWxpZ24AAAAHZGVmYXVsdAAAAAl2ZXJ0QWxpZ25lbnVtAAAAD0VTbGljZVZlcnRBbGlnbgAAAAdkZWZhdWx0AAAAC2JnQ29sb3JUeXBlZW51bQAAABFFU2xpY2VCR0NvbG9yVHlwZQAAAABOb25lAAAACXRvcE91dHNldGxvbmcAAAAAAAAACmxlZnRPdXRzZXRsb25nAAAAAAAAAAxib3R0b21PdXRzZXRsb25nAAAAAAAAAAtyaWdodE91dHNldGxvbmcAAAAAADhCSU0EKAAAAAAADAAAAAI/8AAAAAAAADhCSU0EFAAAAAAABAAAAAM4QklNBAwAAAAAB5kAAAABAAAAoAAAAC0AAAHgAABUYAAAB30AGAAB/9j/7QAMQWRvYmVfQ00AAf/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAC0AoAMBIgACEQEDEQH/3QAEAAr/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APVUkkklKSSSSUpJJY2b9ZsXDyrMZ9FznVO2lzfTgna23Tfa130bP3UqU7CEcgb3sax7yyA4gCJI3fnFv5qp9Lyqup1W5TRaxpsLWsc8ggNaztS8s+l7lZxmhl2Q0TG9p1JJ+gz96UlLNL7sixpNlTWNZDdBqS+Xab/BE+z/APC2f53+xMz+l2/1K/y2oySkX2f/AIWz/O/2JfZ/+Fs/zkVJJSL7OO9lh/tkf9TtQ8qoV4tz2PsDmVuc073cgEjlysoOb/Q7/wDi3/8AUlJSn5L2Mc91D4aCTqzga/6RGBkA+KHk/wBGt/qO/IUIYuOaNGAEs8/BJTZSXM0/W5wprBxNdg5t8v8Ail0GPlV3Y9VxIZ6rGv2lwMbgHQjRUmTofrVfvt+8KYIIBBkHghBT/9D1G+01V7wNxlrQCYEucGcw795U7Mu3Ix8tra9opLqnl+9skAOc6rdW31a/f/ON9m9XL6zbXsaQ0y1wJEiWkP1Et/dQctuT9ltl7D7HaBhnjt+kSUk+yY3+iZ/mhBxRj5Hq7sM0ela6oeoxo3hv+Gq53U2fmI/2rH/fCb7Vj/v/AIH+5LVSvsmL/oWf5oXCfWR7Mfq+S1jDtNzG7WCAN1dP6Q/yG/nLu/tVH734H+5cP9YS1/WcpzSSC9oED/gqfFOjugu19Wsl2P0PIvZU+813PIprEvdpXoxbTHltmS8NLiNrtg5PsHt/rLM+qGnTbdd36d2p7+2ta1P9IyPiz/qUDukI8Wx1tptcx1TrKaXGt30ml3qnY/8AlNVpBb/TLf8Ai6/+quRkFI8m19NFltdbr3saS2ppALiOGNLvb7kRJJJSkLM/od//ABb/APqSioWZ/RL/APi3/wDUlJSK265z8ih1JbU2oOZeSC15cH7qw36TfT2pMuv9SukUE0Op3HJ3CA7QCn0/5z3N9+9Hu/mH/wBQ/kTU/wAxX/UH5ElPnDLC1tQ2EtLYc6fow329vdvd7V3/AE+ur9m4zvTa4+hWYgSfY1cExrfTaC5xO0d+dF3fTcisdOxRD/5mv/Bv/cb/ACE6SA2aWNfUx76W1vc0F1eh2kj3M3N+ltTYQAx2gCAC4QP6zk/2mv8Ads/7bf8A+QTYYIoAILTLjB0MFznBNS//0fRLrchrcy83uZVikkV1sY4lra2Wnb6n0nu3OVA9caK2WGzMiwva0ehXINZYx+/2+xv6X9G9/wDOfmLQf6Pr3bPtG7ePU9Pdt3bWcf8AW/TS9nf7X/0/4JKRU5N92DVn133BljmEVXVsY6HPbW5trAwPrdt/zFqrOd9lger9p2bm/T9TbO5uzd/b2rRSUpZGb9W8LNybMm225r7SC5rS3aIa2v27q3/m1rXTJC+inP6bi19MZdjMFr2Gze1zm7iQWV/nVta36TVYx3h9+QQCNWfSBH5vmrCq2eh6z/53fpv9PfHHt+h7d21JSRn9Mt/4uv8A6q5GVTG2/artu+PTr/nN0/Su/wBIreqSlJJtfD8Uvd4D7/8AYkpdCy/6Jd/xbvyFEl/gPv8A9iFll/2W7Qfzbu5/dP8AJSUzfrQ7zafyINN1v2Zm2mwnYI+h4f8AGKA+y+mN3q7dus+txCts27G7I2QNscR+akpyKvqv0v0WCxlgeGgOHqv5jX85Eu9XGx8g0Nvubh7K6cagt3FoZX7WeoPfZ7/z7FqKhkfs37Q/1d3raeps9Tw9m/0fZu2I69VOd+0c41vs+w9Q9rmtDZbucHi125vt/wAH6TGW/wDG/wCj/SWWsW3Jux/tFjMjFe2+usV3OB3NL6muft2j9HY2x/8AL/4u1T/yT/wnz9dOz9j+oyPp7m7N/qRuken/ADnt3b/ofy0lP//ZADhCSU0EIQAAAAAAVwAAAAEBAAAADwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAAABQAQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAIAAyADAAMgAwAAAAAQA4QklNBAYAAAAAAAcABAAAAAEBAP/hD5xodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ4IDc5LjE2NDAzNiwgMjAxOS8wOC8xMy0wMTowNjo1NyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMC0wMi0yM1QxNDowNTowMSswMTowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjAtMDItMjNUMTQ6MDk6MDUrMDE6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMjAtMDItMjNUMTQ6MDk6MDUrMDE6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvanBlZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyIgcGhvdG9zaG9wOklDQ1Byb2ZpbGU9IkRpc3BsYXkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6ZjQ3M2Q3MWQtMmNmNi00ZGYzLThlNTEtODZjNTRiZTZhNWE0IiB4bXBNTTpEb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6NmE5NzU4ZGQtMWU2YS0wNTRhLThiZTEtNDIxZjFlMjQzYTliIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6YTQzMjQ5OWEtZTdkOS00MmY5LTlhZmUtMzQ2NTkyNDQ5NjAwIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6YTQzMjQ5OWEtZTdkOS00MmY5LTlhZmUtMzQ2NTkyNDQ5NjAwIiBzdEV2dDp3aGVuPSIyMDIwLTAyLTIzVDE0OjA5OjA1KzAxOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjEuMCAoTWFjaW50b3NoKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY29udmVydGVkIiBzdEV2dDpwYXJhbWV0ZXJzPSJmcm9tIGltYWdlL3BuZyB0byBpbWFnZS9qcGVnIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJkZXJpdmVkIiBzdEV2dDpwYXJhbWV0ZXJzPSJjb252ZXJ0ZWQgZnJvbSBpbWFnZS9wbmcgdG8gaW1hZ2UvanBlZyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6ZjQ3M2Q3MWQtMmNmNi00ZGYzLThlNTEtODZjNTRiZTZhNWE0IiBzdEV2dDp3aGVuPSIyMDIwLTAyLTIzVDE0OjA5OjA1KzAxOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjEuMCAoTWFjaW50b3NoKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6YTQzMjQ5OWEtZTdkOS00MmY5LTlhZmUtMzQ2NTkyNDQ5NjAwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmE0MzI0OTlhLWU3ZDktNDJmOS05YWZlLTM0NjU5MjQ0OTYwMCIgc3RSZWY6b3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOmE0MzI0OTlhLWU3ZDktNDJmOS05YWZlLTM0NjU5MjQ0OTYwMCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/+IPrElDQ19QUk9GSUxFAAEBAAAPnGFwcGwCEAAAbW50clJHQiBYWVogB+QAAQAaAA0AHgA2YWNzcEFQUEwAAAAAQVBQTAAAAAAAAAAAAAAAAAAAAAAAAPbWAAEAAAAA0y1hcHBsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARZGVzYwAAAVAAAABiZHNjbQAAAbQAAASCY3BydAAABjgAAAAjd3RwdAAABlwAAAAUclhZWgAABnAAAAAUZ1hZWgAABoQAAAAUYlhZWgAABpgAAAAUclRSQwAABqwAAAgMYWFyZwAADrgAAAAgdmNndAAADtgAAAAwbmRpbgAADwgAAAA+Y2hhZAAAD0gAAAAsbW1vZAAAD3QAAAAoYlRSQwAABqwAAAgMZ1RSQwAABqwAAAgMYWFiZwAADrgAAAAgYWFnZwAADrgAAAAgZGVzYwAAAAAAAAAIRGlzcGxheQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG1sdWMAAAAAAAAAJgAAAAxockhSAAAAFAAAAdhrb0tSAAAADAAAAexuYk5PAAAAEgAAAfhpZAAAAAAAEgAAAgpodUhVAAAAFAAAAhxjc0NaAAAAFgAAAjBkYURLAAAAHAAAAkZubE5MAAAAFgAAAmJmaUZJAAAAEAAAAnhpdElUAAAAFAAAAohlc0VTAAAAEgAAApxyb1JPAAAAEgAAApxmckNBAAAAFgAAAq5hcgAAAAAAFAAAAsR1a1VBAAAAHAAAAthoZUlMAAAAFgAAAvR6aFRXAAAACgAAAwp2aVZOAAAADgAAAxRza1NLAAAAFgAAAyJ6aENOAAAACgAAAwpydVJVAAAAJAAAAzhlbkdCAAAAFAAAA1xmckZSAAAAFgAAA3BtcwAAAAAAEgAAA4ZoaUlOAAAAEgAAA5h0aFRIAAAADAAAA6pjYUVTAAAAGAAAA7ZlbkFVAAAAFAAAA1xlc1hMAAAAEgAAApxkZURFAAAAEAAAA85lblVTAAAAEgAAA95wdEJSAAAAGAAAA/BwbFBMAAAAEgAABAhlbEdSAAAAIgAABBpzdlNFAAAAEAAABDx0clRSAAAAFAAABExwdFBUAAAAFgAABGBqYUpQAAAADAAABHYATABDAEQAIAB1ACAAYgBvAGoAac7st+wAIABMAEMARABGAGEAcgBnAGUALQBMAEMARABMAEMARAAgAFcAYQByAG4AYQBTAHoA7QBuAGUAcwAgAEwAQwBEAEIAYQByAGUAdgBuAP0AIABMAEMARABMAEMARAAtAGYAYQByAHYAZQBzAGsA5gByAG0ASwBsAGUAdQByAGUAbgAtAEwAQwBEAFYA5AByAGkALQBMAEMARABMAEMARAAgAGMAbwBsAG8AcgBpAEwAQwBEACAAYwBvAGwAbwByAEEAQwBMACAAYwBvAHUAbABlAHUAciAPAEwAQwBEACAGRQZEBkgGRgYpBBoEPgQ7BEwEPgRABD4EMgQ4BDkAIABMAEMARCAPAEwAQwBEACAF5gXRBeIF1QXgBdlfaYJyAEwAQwBEAEwAQwBEACAATQDgAHUARgBhAHIAZQBiAG4A/QAgAEwAQwBEBCYEMgQ1BEIEPQQ+BDkAIAQWBBoALQQ0BDgEQQQ/BDsENQQ5AEMAbwBsAG8AdQByACAATABDAEQATABDAEQAIABjAG8AdQBsAGUAdQByAFcAYQByAG4AYQAgAEwAQwBECTAJAgkXCUAJKAAgAEwAQwBEAEwAQwBEACAOKg41AEwAQwBEACAAZQBuACAAYwBvAGwAbwByAEYAYQByAGIALQBMAEMARABDAG8AbABvAHIAIABMAEMARABMAEMARAAgAEMAbwBsAG8AcgBpAGQAbwBLAG8AbABvAHIAIABMAEMARAOIA7MDxwPBA8kDvAO3ACADvwO4A8wDvQO3ACAATABDAEQARgDkAHIAZwAtAEwAQwBEAFIAZQBuAGsAbABpACAATABDAEQATABDAEQAIABhACAAQwBvAHIAZQBzMKsw6TD8AEwAQwBEAAB0ZXh0AAAAAENvcHlyaWdodCBBcHBsZSBJbmMuLCAyMDIwAABYWVogAAAAAAAA8M8AAQAAAAEZEVhZWiAAAAAAAACBbwAAPJz///+5WFlaIAAAAAAAAE3dAAC07QAACtpYWVogAAAAAAAAJ4oAAA52AADImmN1cnYAAAAAAAAEAAAAAAUACgAPABQAGQAeACMAKAAtADIANgA7AEAARQBKAE8AVABZAF4AYwBoAG0AcgB3AHwAgQCGAIsAkACVAJoAnwCjAKgArQCyALcAvADBAMYAywDQANUA2wDgAOUA6wDwAPYA+wEBAQcBDQETARkBHwElASsBMgE4AT4BRQFMAVIBWQFgAWcBbgF1AXwBgwGLAZIBmgGhAakBsQG5AcEByQHRAdkB4QHpAfIB+gIDAgwCFAIdAiYCLwI4AkECSwJUAl0CZwJxAnoChAKOApgCogKsArYCwQLLAtUC4ALrAvUDAAMLAxYDIQMtAzgDQwNPA1oDZgNyA34DigOWA6IDrgO6A8cD0wPgA+wD+QQGBBMEIAQtBDsESARVBGMEcQR+BIwEmgSoBLYExATTBOEE8AT+BQ0FHAUrBToFSQVYBWcFdwWGBZYFpgW1BcUF1QXlBfYGBgYWBicGNwZIBlkGagZ7BowGnQavBsAG0QbjBvUHBwcZBysHPQdPB2EHdAeGB5kHrAe/B9IH5Qf4CAsIHwgyCEYIWghuCIIIlgiqCL4I0gjnCPsJEAklCToJTwlkCXkJjwmkCboJzwnlCfsKEQonCj0KVApqCoEKmAquCsUK3ArzCwsLIgs5C1ELaQuAC5gLsAvIC+EL+QwSDCoMQwxcDHUMjgynDMAM2QzzDQ0NJg1ADVoNdA2ODakNww3eDfgOEw4uDkkOZA5/DpsOtg7SDu4PCQ8lD0EPXg96D5YPsw/PD+wQCRAmEEMQYRB+EJsQuRDXEPURExExEU8RbRGMEaoRyRHoEgcSJhJFEmQShBKjEsMS4xMDEyMTQxNjE4MTpBPFE+UUBhQnFEkUahSLFK0UzhTwFRIVNBVWFXgVmxW9FeAWAxYmFkkWbBaPFrIW1hb6Fx0XQRdlF4kXrhfSF/cYGxhAGGUYihivGNUY+hkgGUUZaxmRGbcZ3RoEGioaURp3Gp4axRrsGxQbOxtjG4obshvaHAIcKhxSHHscoxzMHPUdHh1HHXAdmR3DHeweFh5AHmoelB6+HukfEx8+H2kflB+/H+ogFSBBIGwgmCDEIPAhHCFIIXUhoSHOIfsiJyJVIoIiryLdIwojOCNmI5QjwiPwJB8kTSR8JKsk2iUJJTglaCWXJccl9yYnJlcmhya3JugnGCdJJ3onqyfcKA0oPyhxKKIo1CkGKTgpaymdKdAqAio1KmgqmyrPKwIrNitpK50r0SwFLDksbiyiLNctDC1BLXYtqy3hLhYuTC6CLrcu7i8kL1ovkS/HL/4wNTBsMKQw2zESMUoxgjG6MfIyKjJjMpsy1DMNM0YzfzO4M/E0KzRlNJ402DUTNU01hzXCNf02NzZyNq426TckN2A3nDfXOBQ4UDiMOMg5BTlCOX85vDn5OjY6dDqyOu87LTtrO6o76DwnPGU8pDzjPSI9YT2hPeA+ID5gPqA+4D8hP2E/oj/iQCNAZECmQOdBKUFqQaxB7kIwQnJCtUL3QzpDfUPARANER0SKRM5FEkVVRZpF3kYiRmdGq0bwRzVHe0fASAVIS0iRSNdJHUljSalJ8Eo3Sn1KxEsMS1NLmkviTCpMcky6TQJNSk2TTdxOJU5uTrdPAE9JT5NP3VAnUHFQu1EGUVBRm1HmUjFSfFLHUxNTX1OqU/ZUQlSPVNtVKFV1VcJWD1ZcVqlW91dEV5JX4FgvWH1Yy1kaWWlZuFoHWlZaplr1W0VblVvlXDVchlzWXSddeF3JXhpebF69Xw9fYV+zYAVgV2CqYPxhT2GiYfViSWKcYvBjQ2OXY+tkQGSUZOllPWWSZedmPWaSZuhnPWeTZ+loP2iWaOxpQ2maafFqSGqfavdrT2una/9sV2yvbQhtYG25bhJua27Ebx5veG/RcCtwhnDgcTpxlXHwcktypnMBc11zuHQUdHB0zHUodYV14XY+dpt2+HdWd7N4EXhueMx5KnmJeed6RnqlewR7Y3vCfCF8gXzhfUF9oX4BfmJ+wn8jf4R/5YBHgKiBCoFrgc2CMIKSgvSDV4O6hB2EgITjhUeFq4YOhnKG14c7h5+IBIhpiM6JM4mZif6KZIrKizCLlov8jGOMyo0xjZiN/45mjs6PNo+ekAaQbpDWkT+RqJIRknqS45NNk7aUIJSKlPSVX5XJljSWn5cKl3WX4JhMmLiZJJmQmfyaaJrVm0Kbr5wcnImc951kndKeQJ6unx2fi5/6oGmg2KFHobaiJqKWowajdqPmpFakx6U4pammGqaLpv2nbqfgqFKoxKk3qamqHKqPqwKrdavprFys0K1ErbiuLa6hrxavi7AAsHWw6rFgsdayS7LCszizrrQltJy1E7WKtgG2ebbwt2i34LhZuNG5SrnCuju6tbsuu6e8IbybvRW9j74KvoS+/796v/XAcMDswWfB48JfwtvDWMPUxFHEzsVLxcjGRsbDx0HHv8g9yLzJOsm5yjjKt8s2y7bMNcy1zTXNtc42zrbPN8+40DnQutE80b7SP9LB00TTxtRJ1MvVTtXR1lXW2Ndc1+DYZNjo2WzZ8dp22vvbgNwF3IrdEN2W3hzeot8p36/gNuC94UThzOJT4tvjY+Pr5HPk/OWE5g3mlucf56noMui86Ubp0Opb6uXrcOv77IbtEe2c7ijutO9A78zwWPDl8XLx//KM8xnzp/Q09ML1UPXe9m32+/eK+Bn4qPk4+cf6V/rn+3f8B/yY/Sn9uv5L/tz/bf//cGFyYQAAAAAAAwAAAAJmZgAA8qcAAA1ZAAAT0AAAClt2Y2d0AAAAAAAAAAEAAQAAAAAAAAABAAAAAQAAAAAAAAABAAAAAQAAAAAAAAABAABuZGluAAAAAAAAADYAAK4AAABSAAAAQ8AAALDAAAAmQAAADYAAAE9AAABUQAACMzMAAjMzAAIzMwAAAAAAAAAAc2YzMgAAAAAAAQ6rAAAHIf//8m8AAAlvAAD8R///+1D///2cAAAD1AAAvuhtbW9kAAAAAAAABhAAAKAwAAAAANIfswAAAAAAAAAAAAAAAAAAAAAA/+4ADkFkb2JlAGQAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAEHBwcNDA0YEBAYFA4ODhQUDg4ODhQRDAwMDAwREQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgBXgTnAwERAAIRAQMRAf/dAAQAnf/EAaIAAAAHAQEBAQEAAAAAAAAAAAQFAwIGAQAHCAkKCwEAAgIDAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAACAQMDAgQCBgcDBAIGAnMBAgMRBAAFIRIxQVEGE2EicYEUMpGhBxWxQiPBUtHhMxZi8CRygvElQzRTkqKyY3PCNUQnk6OzNhdUZHTD0uIIJoMJChgZhJRFRqS0VtNVKBry4/PE1OT0ZXWFlaW1xdXl9WZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3OEhYaHiImKi4yNjo+Ck5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6voRAAICAQIDBQUEBQYECAMDbQEAAhEDBCESMUEFURNhIgZxgZEyobHwFMHR4SNCFVJicvEzJDRDghaSUyWiY7LCB3PSNeJEgxdUkwgJChgZJjZFGidkdFU38qOzwygp0+PzhJSktMTU5PRldYWVpbXF1eX1RlZmdoaWprbG1ub2R1dnd4eXp7fH1+f3OEhYaHiImKi4yNjo+DlJWWl5iZmpucnZ6fkqOkpaanqKmqq6ytrq+v/aAAwDAQACEQMRAD8A9U4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq1XFXVxVsYq7FXYq7FXYq1XFXVxV2KuxV2KuxV2Kt0xV1MVaxV2KuxV2KuOKrQSQcKtVfienLtirlkIA5D4u+NK71B4HGlbVqnbGlbJoK40rXqDGld6gxpXeoMVd6gxVwavTFW64q4tTfFWvUGKu9QYq1yOFW+RxVr1KdcVd6q++BXeoD0rirfLFXcsVdyxV3LFXcsVdyxV3I4q7k2NK7k2NK1yPiMVdyPiMVdyPiMVXKTTArXqYaV3Jz0xpW15ftYFbxV2Kt4q7FXYq7FVrMRsCK9hiqxJH9Or05jsMVXoxYVxVdTFXUxV1MVdTFXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FWsVdirsVdXFXYq3TFXUxVquKurirYxV2KuOKtVxV1cVdXFXVxV2Kt0xV1MVdirsVdirsVdirsVdirsVdirsVdir/AP/Q9U4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXHFWsVdirYxV2KuxV2KuOKtYq7FXYq7FXYq7FXYq3irsVaxV2KuxV2KqN3c21rbSXFy4igjHKSRugGKpQ3nDyxHGJFv4ijdDU9/owqufzf5aXi31+L4vc/wBMVaPnHyzX/jpQj2qf6Yq1/jLyz/1c4fvP9MVaPnbysm7apCPpP9MKrT588pAV/SkJ+k/0xVr/AB/5R/6uUP3n+mNK7/H/AJR/6uUP3n+mNK7/AB/5R/6uUP3n+mNK7/H/AJR/6uUP3n+mNK2vnzyif+lpCPpP9MVb/wAdeUv+rpD95/pirTefPKQFf0pCfpP9MVa/x/5R/wCrlD95/pjStf4/8o/9XOH7z/TGlb/x35S/6ukP3n+mKu/x35S/6ukP3n+mKtHz75SBp+k4T9J/pirX+P8Ayl/1cofvP9MVaPn/AMp9tSh+8/0xVr/H/lX/AKuUP3n+mNK7/H/lX/q5Q/ef6Y0rv8f+Vf8Aq5Q/ef6Y0rv8f+Vf+rlD95/pjSu/x/5V/wCrlD95/pjSu/x/5V/6uUP3n+mNK4+fvK3bUofvP9MaV3+PvLH/AFcYvvP9MVa/x95Y/wCrjF95/piq3/HXln/lqj+84q3/AI68tf8ALSn3nFXf468tf8tKfecVQ9x56j5j9G2rXsFPiljIoH7rvTelDiqh/jvWB08v3DeBBXf8cVaPnzXe3ly5+9f640rh568wn/pnLn71/rjSt/458wf9S5c/ev8AXGld/jnzB/1Llz96/wBcCu/x15h/6l26+9f64Fd/jrzD/wBS7dfev9cVd/jrzD/1Lt196/1xV3+OvMP/AFLt196/1xVRl8966rqz+WrojpyqtBX6cVV/LfnaTVPM95pUtjJbSQRJI3Mg7MT4YqzEJKJAefwfy4qq4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FWsVdirsVdirhireKuxVrFXYq2MVdirjirWKuxV2KuxVsYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FX//0fVOKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVxxVrFXYq2MVdirsVdirjirWKuxV2KuxV2KuxV2Kt1xV1cVaxV2KuxV2Ksa/MiQx+RdZcdVtyR94xVJvJ/lXT73yjp8lxCpkliR60rhtU9/wXorKg9Ffh/ycbVU/wboX/LKh/wBjjat/4O0H/llT/gcbVtfJ3l8H4rONvmuNquPk/wAt/wDVvh/4HG1a/wAIeXP+rdD/AMDhtXf4Q8uf9W6H/gcbV3+EPLn/AFbof+BxtXf4Q8uf9W6H/gcbVseUfLn/AFb4R/scFq3/AIR8uf8AVvh/4HG1cfKPlyn/ABz4f+BxtVv+EPLn/Vuh/wCBw2rv8IeXP+rdD/wONq3/AIP8t/8AVvh/4HBau/wf5b/5YIf+BxtWx5Q8tj/pXwn/AGONq3/hHy1/1bof+BxtXf4S8tj/AKV0P/A42rf+FPLf/Vuh/wCBxtXf4U8t/wDVuh/4HG1d/hTy3/1bof8AgcbV3+FPLf8A1bof+BxtXf4U8t/9W6H/AIHG1d/hTy3/ANW6H/gcbVr/AAp5c7adB/wONq7/AAr5e/6t0H/A42rv8K+Xv+rdB/wONq3/AIX0H/lgi/4HG1d/hfQf+WCL/gcbV3+F9B/5YIv+BxtUTbaTp1rGY4LZI0J5FVFBWlP4Y2qJEEQGyj5Y2rfADooxVsL7Yq3x9sVdx9sVdQeGBXUHhiruPtiruPtiqjch+FFUEVFa4q890BHX84NeYj4DaQcfnU4q9H5MW6bd8VX1xV1cVdXFXVxV1cVdXFXVxV2KuxV2KuxV2KuxV2KuxV2KtYq7FXYq7FXDFW8VdirWKuxVsYq7FXHFWsVdirsVdirYxV2KuxV1cVdXFXYq7FXYq7FXYq7FXYq7FXYq/wD/0vVOKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVxxVrFXYq7FXYq7FXYqxj8zP+UC1v/mGP6xiqv5D/AOUP0f8A5hlxVPk6YquxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVsYq7FXHFWsVdirYxV2KrJfs4q8+0X/ybOtf8wsP6zir0Jep+eKt4q7FXYq7FXYq7FXYq3irsVdirsVdirsVdirsVdirWKuxV2KuxVwxVvFXYq7FXYq7FXYq7FXYq44q1irsVbGKuxVxxVrFXYq3irsVdirsVdirsVdirsVdir//0/VOKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV1RirqjFXEjFWsVdirqjFXVGKuxV2KsY/Mz/lAtb/5hj+sYqr+Q/8AlD9H/wCYZcVT5OmKrsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVbGKuxVxxVrFXYq2MVdiqyX7OKvPNHkjT82da5MF/0WHqQO5xVn63NtU/vU6/zDFW/rVt/v1P+CGKt/Wbb/fqf8EMVa+tW3+/U/wCCGKu+tW3+/U/4IYq761bf79T/AIIYq761bf7+T/ghira3FuzhFlQsdwoYV+7FVXFXYq7FXYq7FXYq7FXYq7FWsVdirsVdirhireKuxV2KuxV2KuxV2KuxVxxVrFXYq2MVdirjirWKuxVvFXYq7FXYq7FXYq7FXYq7FX//1PVOKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVxxVrFXYq3irsVaxV2KuxV2KuxVvFXYq1irsVUpqVWoJ+WKsU8yecP0RqMdutvI5dCRQVySpfF501qSNGFjNQnb4MVRX+LNa/5Ypf+AxV3+LNa/5Ypf8AgMVW/wCKNdO4s5P+AxV3+J9e/wCWST/gMVcPMfmR/wC6t2SnXkmKt/p/zX/vof8AAY0qQ+eta8yS+UNVjnjpC0BDnhTaowUrNfIf/KH6P/zDLgVkCdMVdirsVdirsVdirsVdirsVdirsVdirsKuxV2KuxV2KuxV2KuxV2KuwK7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq2MVdirjirWKuxVsYq7FVKVgrAkE/LCEF5S3l6z1f81NaWV5Ub6rFXg5XufDChkkf5YaKsbo1xc1Y1r67/ANcVWj8rdC/5abr/AJHv/XFV4/K/Qv8Alouv+R7/ANcVa/5VdoX/AC03X/I9/wCuEFk7/lV2hf8ALRdf8j3/AK4bVqT8r9BMYH1m6G439d/64LVa/wCVWilqrcXX/I9/648SsfOgWWkfmlpEcFxOzfVJfgeVmU7r1UmmJV6vF9gZBVw64q3irsVaOKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVsYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FWsVdirhireKuxV2KuxV//V9U4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXHFWsVdireKuxVrFXYq7FXYq7FW8VdirWKuxVZID1DUA64hXnnnRnHm7TlLng0RNP9kMkrPYniSOOMDsNvowKr8l8cVdyXxxVwB7HDat0PjgtWiD88KtU/ycCsZ/Msf86HrW3/AB7H9YxVE+Q/+UP0j/mGTArIE6Yq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYVdirsVdirsVdirsVdirsVdgV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVsYq7FXHFWsVdirYxV2KrJCwOwrhCC890csPzY1qi1/wBFh3+k4UPQFAatVpirZRf5cVa4j+XFW+C/y5G0u4L/AC42lp41K/Z79MbVwf8AeFafTjavO9fVR+b2jUXf6nNv9K42r0aL7AxVcOuKt4q7FWjirsVdirsVdirsVdirsVdirsVdirsVdirsVbGKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVrFXYq4Yq3irsVdirsVf/9b1RXFW8VdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVccVaxV2Kt4q7FVpJBNenbFWo2ZgSfoxVytUb4quqMVdUYq7kMCu5DFVpkAPQ4q16g8DirTuGUih3xCvPPOzBfNumih/uj/AMSGStXoMPp8EJ6lR+rAqtxGKu4jFXYq7FXYq7FWM/mX/wAoHrX/ADDH9YxVV8iE/wCEdHH/AC7LirIB9nFXDpireKuxV2KuxV2KuxV2KuxV2KuxV2FXYq7FXYq7FXYFdirsVdirsVdirsVdirsVdirsVdirsVdirqYq3TFXUxV1MVdirjirWKuxV2KuriqyVA43JHyxRTzvRBw/NzW1BqPqsPX5nDa09HAwLTdMVprgMNrTdMjSXUxpVkmwqMaVogcie+NK878xsw/NXR2HX6pKPxXDSvRFJVVA74VbqQ4HbFV1cCurirVd8Vd9OKuqB1OKrVLczWnH9nFVpuFDceJ+dMVVcVdirsVdirsVdirsVbpirqYq7FXYq7FXYq7FXYq7FWq4q6uKurirq4q6uKurirq4q7FW6Yq6mKuxV2KuxV2Kv//X9UYq3irsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirjirWKuxVvFXYqpzdVHjiqxnKyCMdxXFVkl9axyGIsOa9VxVb+kLbxGKu/SFt4jFXfpC28RkqV36QtvEY0q06ja1/vwvtgpWv0jaf8tAxpWmv7dgeNwNsaVgfm+4R/N+mgShqxH/iQwK9Ej4hItqkqN/oxVEYq7FWsVdirsVdirGfzL/5QPWv+YY/rGKqvkT/lEtH/AOYZcVZAPsHFXL0xVvFXYq7FXYq7FXYq7FXYq7FXYq7CrsVdirsVdgV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVsYq7FXYq7FXYq44q1irsVdirsVaPTFXnWkf+Te1r/mFh/WcVejjFXYq7FXYq7FVkv2R88Var8VMVeW/mFd3tl5/wBKubK1a8nW2cCFdiQWG+FU8Pm/zbxj/wCddm7ftD+uFWz5w82+qB/h2b/gh/XFWv8AGHm//qXpv+CH9cVd/jDzf/1L03/BD+uKtr5r83Nv/h+Yf7If1wFW/wDFPm7/AKsEv3j+uBVkvmnzdxr/AIcmkp+zyH9cVQV7+YPmG31DR7a40WS1F/OYjVh2BPjir0JDKyBn+AnquKqo6Yq7FXYq7FXYq7FXYq3irsVdirsVdirsVdirsVdirWKuxV2KuxV2KuxV2KtjFXYq7FXYq7FXYq7FX//Q9UYq3irsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirq4q6uKtYq7FXYq3XFXVxVYwLMKduuKrHUep6ngKYq8wjtLnW/zL1jTLm4khhtbeOSP0mpuxIxVkv8Ayry1/wCrhc/8F/birf8Ayru1/wCrhc/8F/birX/Ku7b/AKuFz/wX9uS4ld/yru2/6uFz/wAF/bjxK2Py8tP+W64Pvy/tx4lb/wCVeWf/AC23H/Bf248SrW/Lu0I/3uuB8m/twEqxDzP5Zg03zXps/wBYlkRY6fEa7lhgV6vFz9KLiARxWpPhTFURXFXVxVrFXYq7FXYqxn8y/wDlA9a/5hj+sYqq+RP+US0f/mGXFWQD7BxVy9MVbxV2KuxV2KuxV2Kurirq4q6uKtVxVuuG1dXG1dXG1dXG1awK3XFXVxV1cVdXFXVxV1cVdXFXVxV1cVdXFXVxV1cVdXFXYq2MVdXFXVxV1cVdirjirWKuxV2KuxVa7BVqcVed6SCPzb1lz0NtDT7zir0YHFW8Vdirq4q6uKrX3GKrR9s4q8/80MB+ZGlMRUC2kH4rirPl3RCAMKtkfvAKDG1X8B4Yq7gPDFWqU6DArt/DFXHlQ7dsVeafmaZU8x+TFrTlfsGp/qHFXpLRgihJxVeDQYaV3LGlbBrgV2KuxV2KuxVuuKurirsVdirsVdirsVdXFXVxVrFXYq7FXYq7FXYq7FWxirsVdirsVdirsVdir//R9UYq3irsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirWKuxV2KuxV2KuxV2KtL9o4qsf7J+eKvO/L3/k4vMP/ADCQ/wDEjir0cYqu7Yq1irsVdirsVcemKvOvzD/47th/qj/iQxV6Bbf3Ef8AqL+rFVXFXYq7FXYq7FXYqxn8y/8AlA9a/wCYY/rGKqvkT/lEtH/5hlxVkA+wcVcvTFW8VdirsVdirsVdhV2KuxV2KuxV2BXYq7FXYq7CFdirsVdirsVdirsVdirsVdirsVdirsVdirhgVsYq1irsVbGKuxV2KuxV2KtYq7FVOf7H0jFXn+mf+TV1f/mGh/WcVeieGKt4q7FWsVdirjiqxfttirz3zV/5MPS/+Yd/+JDFXoEX90nyxVef7wYqvxV2KtHFXYq49MVeafml/wApP5L/AOY8/wDJs4q9Kbriq3JK7FWwaYq3XArTE9sSrlrgVf2xVrFXYq2MVdirsVdirjirWKuxV2KuxV2KuxV2Kt4q7FXYq7FXYq7FXYq7FXYq/wD/0vVGKt4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq1irqjFXVGKuxV2KuxV2KtL9o4qsf7J+eKvO/LxH/K4vMP/MJD/wASOKvR1xVd2xVrFXYq7FXYq0emKvO/zD/47lge1Bv/ALIYq9Atv7iP/UX9WKquKuxV2KuxV2KuxVjP5l/8oHrX/MMf1jFVXyJ/yiWj/wDMMuKsgH2cVcvTFW8VdirsVdirsVdhV2KuxV2KuOKuwK7FXYq7FXYVdirsVdirsVdirsVdirsVdirsVdirsVdirhgVsYq1irsVbGKuxV2KuxV2KtYq7FVOb7H0jFXn+m/+TV1b/mGh/WcVeieGKt4q7FWsVdirRxVav2zirz3zV/5MPS/+Yd/+JDFXoEX90nyGKrz/AHgxVfirsVaOKuxVx6Yq8y/NWSNPMvkxnYKBfmpYgD+7Pjir0N9T00He7h/5GL/XCq39J6b/AMtcP/Ixf64Vd+k9N/5a4f8AkYn9cVW/pPTa7XcP/Ixf64q79Kab/wAtcP8AyMT+uKu/Sumg/wC9cP8AyMT+uJVptZ04f8fUP/Ixf65FVh1zTxStzEASBUuvU/TiqYqwYAg1B7jFW8VbGKuxV2KuxVxxVrFXYq7FXYq7FXYq7FW8VdirsVdirsVdirsVdirsVf/T9UYq3irsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsirskqlKwVS3XjvQYQryi7/AD0srbUr+D9GXFx9QlML+mR1pkgFUl/5yAtwhK+XL6nWtV/pjSCpRf8AORVtUhvLl996/wBMSEKg/wCch7Jjt5eva/6y/wBMaVo/85Cw/wDUu3v3r/THhVaf+chof+pdvvvXDSuf/nIBf3RXQLwBzQiq4eFVRvz8hRf+ODeEk9Kr/THhVD/ll5qh8wfmjr9wthNZubSEkykGvxHwwK9nXpkSreBXYGTsVdirsVabFXnn5k/712h78k/4kMVZ9a/7zw/6i/qxVWxV2KuxV2KuxV2Ksb/Mj/lBdZ/5hz+sYqu8g/8AKI6X/wAw64qnrfs/PFVXFXYq7FXYq7FXYq7CrsVdirsVdirsVdirsVdirsVdirsVdirsVdgV2KuxV2KuxV2FXYq7FXYq4YFdirsVdirYxV2KuOKtYq7FXYq7FWv2sVedQf8Ak09Q/wCMMX6zir0UdTireKuxVsYq7FVp6/RiqmeqfPFWAebf+U603/jC3/Ehir0CL7Kf6o/Viq7+b54qvHTFXYqtOApbxQtetBTJBi8U/wCcirZJ7vyqhYqfrjbqSD9g+GSCCwq88rW5mDvcTb/8Wt/XJMVVfLNgQP3k/wDyNbCha/lWxP8Auy4/5Gtirdv5Z0xDR5Z6/wDGZsVb/wAKaa8tRLPT/jM2KtS+UbISBhJcU2H962KUTH5PsTOpMk//ACNbClLfN2j2dnosBR5wwv7eh9Vv9+DIlIfS2hE/o633r+7Xr/qjIFkEevU4GTYwIbxV2KuxV2Kt4q7FWsVdirsVdirsVdirsVbGKuxV2KuxV2KuxV2Kv//U9UYq3irsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsirskql8RcgqOPjhV8z6ZY8tZ85NIeP+5McPlxyYVk31ZI4+KsXPEVB+WSAQUu+qMzMaUyVIU1taPSu+NKrm2k8caVTa2k8caVowMHj5sQK7YFVbKxMlwTLIQgO2Kph+U/Jfza8xRhy0Ys4eP8AwRyBV7mvTIlW8BUOwMnYq7FXYq02KvPPzJ/3rtP9ZP8AiQxVn1r/ALzw/wCov6sVVsVdirsVdirsVdirG/zI/wCUG1n/AJhz+sYqu8gf8ojpf/MOuKp637PzxVVxV2KuxV2KuxV2Kuwq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYFdirsVdirsVdhV2KuxV2KuGBXYq7FXYq2MVdirjirWKuxV2KuxVr9rFXnUH/k09Q/4wxfrOKvRR1OKt4q7FWxirsVWt1+jFVM/sfPFWAebf+U507/jC3/Ehir0CL7Kf6o/Viq4ftfPFV46Yq7FVpwFLeKGiQOuSYvGPz/lRdT8q8hUfXW/4gcmEFJtQu7QlRwFaeGFitScUFIhTJMqba7AH92MU0t5xOvqFAD0pitKiyqichGDitKsN6p3eIUGKCFVNTj+uKojWlcWKU+eprdtAhJjWv1+3/5ODIlkHveh/wDHPg/4xr+oZCTII9ftHAybGBDeKuxV2KuxVvFXYq1irsVdirsVdirsVdirYxV2KuxV2KuxV2KuxV//1fVGKt4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq07cRWlcVWet/knArfJmFV2+eKt/FirvixV3xYq74sVbFe+Kt1xV1cVarirq4q7c4q6jYq4V74q3irsVarirq4q6uKurirq4q6uKuJxVr4sICLd8WFFrHK1HI7jthW3zZpt3z1rzgioxK6iKH/Y5YAm05hlvJYxJJQA/D0psMKCUHPcOk5j7HDbG0BI0zXFFJxtbRSienxtja2pO8gPU/fja2qG4nMcY2ohqPHAtouOeZowDTcjpitov8pi8f5teYE/ZNnD/wASOQZPeP2cgmm/DAml1MVdTFXUxV1MVaIxV53+ZgpdWdP5k/4kMVZ9af7zxf8AGNf1DFVbFXYq6mKt0xV1MVdTFWOfmIobyRrAPQ25/WMVWfl8xPlPTx/LCoGKsi4gmnhviq/FXYq7FXYq7FXYq7FXUxV1MVdTFXUxV1MVdTFXUxV1MVdTFXUxV1MVdTFWqYq3irsVdirsVdirqYq6mKupirqYq7FXUxVumKupirqYq7FXHFWsVdirdMVdTFVr7CoxV5zHt+aN8fGKKv3nFXo9KH54q3TFXUxV2KuxVojviqmR9j54qwDziOPnLT3HURMP+GGKs/g3jQ/5I/Viq5dy2KrxirsVaIwFbdhCFrCowhDx38/lX6/5Vr/y2N/xA5IISa5SPkuw6ZJaUh6XbJJtZJEhxW1ExRV4sTT54ra5nVV4odvfFbVIGR14P067YoLbRwoxlWvJdxvixS38wViXy9ZlSate2zHfv6gyJZB9CaCf9xNu3f01/wCIjIFkEedl5DAyXV98CHV98aKuqO5xoq7kvjgoq7kvjjRV3IeIw7q7kPEY7q6te+DdXYq76cIV1ffCrRO2xxVYzkJXkOWNK3yJUFWHvjSrgxINDvgVpWbiS3UYqujYstTiq7FXYq7FXYq//9b1RireKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KrZCQuxpiqkJjWjUp41w0rZIpVXA+nFW/VT+cffjSu9WP+cffjSu9WP+cffjSu9WP+cffjSu9WP+cffjSu9aL+cY0rvWi/nGNK19Yh/nH340rvrEP84+/Gla9eL/fg+/Gld68X+/B94xpXCeKv94D9ONKu+sQ/zj78aV31iH+cffjStfWIf5x9+NK76xD/ADj78aV31iH+cffjSu+sQ/zj78aV31iH+cffjSu+sQ/zj78aVr6xD/vwffjStevF/vwffixd68X+/B9+FVOR0pyULI3zwq+c9DuLg6x5xAg4f7kftA/5OTCo+G5kSNecperfYPbCgqLqJbquLFQkPp3NBirc07hqYqvR4+NWxV0kqcAV6YqrW9wOKj/KGKUz/KxuX5t6/wD8wcH/ABI5Asnu/wCzkGTj2wJXYodirsVdirjirzv8zv8Aeqz/ANZP+JDFWe2n9xF/xjX9QxVWxV2KtjFXYq7FXYqx78w/+UK1f/mHP6xiqj+X3/KKWP8AxiXFWSj7R+WKt4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FWxirsVdirsVdirjirWKuxVsYq7FVsn2cVedJ/5NC9/wCMUX6zir0Y9cVbxV2KuxV2KtHviqz+X54q8/8AOf8Ayl2n/wDGM/8AEhirP7f+7T/VH6sVXL1OKr8VdirRwFDsQrRyQQ8c/P8ANL7yr/zGN/xA5IKkV0/xL8smqjwI3wqpSzlcVU6tLHyHXFV9vGafFiq/jxckdKYoLaAvBIfAYsUp/MAufLltwHJ1uYCq+JDigyJZB6loPmjzuNKhA0EkBAAeR6UGRZBMz5m88GIf7gP+HyJZLj5h88Hf9Akf7LJhS1+n/PH/AFYj/wAFh2Q79PeeG2/QR/4LHZXfpvzx/wBWM/8ABY2Fd+m/PH/VjP8AwWNhW/0x53/6sp/4LGwrv0x53/6sp/4LGwrX6T86tudNaP8AyeWOyu/SXnX/AJYG/wCCx2Vr6/51bb6iw+nAVb+t+dP+WJvvyKu+t+dDt9Sb78VcW84SVU2rbe+KqM115ygtJX+qt8AJ6+AxVMPIev3moWYF6pW4/aUmtN8iVZYGryGKrohRMVXYq7FXYq7FX//X9UYq3irsVdirsVdirsVdirsVdirsVdirsVdirEvzQuHg8qu6O0bGaNQyEg/EadRhCpZo/kT6xp8cl1e3HOQAjjK3cV8clxKrD8tYuZP1+64/8ZW/rjxBV/8AyrK0/wCW+7/5HN/XHiCu/wCVY2f/AC33f/I5v648QV3/ACrGz/5b7v8A5HN/XHiCu/5VjZ/8t93/AMjm/rjxBXD8sbKu9/d0/wCMzf1x4lXf8qx0/wD5b7z/AJGt/XHiV3/KsdP/AOW+8/5Gt/XHiVr/AJVfp3/Lfef8jW/rjxK7/lV+nf8ALfef8jW/rjxK0fyt00/8f95/yOb+uDiV3/KrNM/6uF5/yOb+uPErY/K7TQai/vP+Rzf1x4lb/wCVY6d/y33n/I1v64eJXf8AKsdO/wCW+8/5Gt/XHiVr/lV+nf8ALfef8jW/rjxK7/lV+nf8t95/yNb+uPErv+VX6d/y33n/ACNb+uPErv8AlV+nf8t95/yNb+uPErv+VX6d/wAt95/yNb+uPErv+VX6d/y33n/I1v648Su/5Vfp3/Lfef8AI1v64DJWv+VWaZ/y33v/ACOb+uRtFO/5VZpn/Lfe/wDI5v64bRSh/wAqn0tbj1hqN/yH7PrPx/XkgU08Y8t2Vrp2s+a7YySuxv8A4SxJ2C98mCtJwlq0sfJDQKSd8KCEMGdbivYHFgtc8pS/tiqkebvUkUxVEO0Xp8e+KtGL9xGAftGgxVfChWgPZhiqa/lQ4P5s663ZrSAD/gjkCze9k9F75Bk6tTTuMCV9cUOrirq4q6uKtE74q88/NFSj2kx+yHjH3sMVZ5ZmsEX/ABjT9QxVXxV2KurirdcVdXFXVxVj35g1PkvVwO8B/WMVUfy/28rWS9/SXFWSA/GR7YquxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KurireKuxV2KuxVxxVqmKupirdcVdXFVr77d8Vec14fmddsejRxAfecVejBqn5Yqurirq4q6uKurirR74qpk04D3xVgPnYFPNenuenpn/iQxVn1v/dIf8kfqxVcvVsVX1xV1cVawFDsQrRNBU5ILTxz8/VL3nlZh0F43/EDkgtMcu5PiFO2TRTQk23wotTljBFcVtbblVX2xW25plH2cVtqOX1AVH2qYoJVrVW9GRD9phtihKvOAY6Fbv8AspeQKfnzGRLIPoXy43LR7dgB9hf1DIFkE2oOI2yKVwAxVug8MG6tU8Md1dTHdXUx3V1MVdTFXUHhjurqDwx3V1B4YVdxxV3HFVJVZZGJ6N0wqhbuJjp90rfyOR9xxtWH/l1C3qz1/Z3/ABwKzwgDfFVybCmKrsVdirsVdir/AP/Q9UYq3irsVdirsVdirsVdirsVdirsVdirsVdirDPzb/5RE/8AMTB/xLFWQ6L/AMcu1/1V/wCIjFUw8cVbxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2RV2SCtE0BOFXzHbiRvNHmggf8fx/wCI5YFTCCWVIGB2G+FBQcT85Grha1sjHmRirkj98VbeP3xVUZiscA9ziqorEt9OKUf+U7U/NbWv+YWH/iRyBZB9AH7YOQZNj7ZwJXYodirsVdirR6jFWAfmv/vNbf8AGWL/AImMVZxZf3EX/GJP1DFURirsVdirsVdirsVSDz9/yhurf8YD+sYqoeQf+Uas/wDjGuKsjH2z8sVX4q7FXYq7FXYq7FXYq7FXYq7FXYq7CrsVdirsVdgV2KuxV2KuxV2KuxV2KuxV2Kuwq7FXYq7FXDArsVbGKuxV2KuxV2KuxV2KtYq7FWv2sVecTf8Aky5/9WP+OKvRk6t88VXYq7FXYq7FXdjiqm/VPnirAvPf/KSWH+p/xsMVZ7b/ANxH/qj9WKtj9rFV+KuxV2RKuwhWiKjJK8c/PskXHlin/LW3/EDkgrHJE5bnJqhnkQHC1lsyoVxQpxlSPpxVf+674qqRmFSSOtMVRFo0ZJxVLvN4i/w0n/MdB/xMZEsg998sf8cS3/1B+oZAsgm37IyJSvGKuxV2KuxVxxVrFXYq7FXYq4Yq3irsVWv9pcVQ93/vFc/8Y3/UcVYf+XX95dfL+OKs3fpiq4dcVbxV2KuxV2Kv/9H1RireKuxV2KuxV1cVdUeOKurirqjxxV1RirqjxxV1R44q6oxV1RirC/za/wCURP8AzEwf8SxVkWi/8cu1/wBVf+IjFUw8cVbxV2KuxV2KuxV2KuxVrkPHFW8VdirsVdirsVdirsVdirsVdirsVdirsVdkVdkgqnIHoaZIK+a9OEv+JfNP/Maf+I5MKjLflIJFbYAE4WJQABSVgPHCxWsCW98VXLHIOoOKtukh6VxVUaNuEFQeuKERFES3Q9cUov8ALFeH5qax2rbQ/wDEjkGT6BFNsgyd+0cCV1cUOqMVdirXIeOKuPUYq8//ADX/AN5rb/jLF/xMYqzmy/3nh/4xJ+oYqiMVdirqjxxV1RirqjFXYqkHn7/lDtWHf0D+sYqh/IBH+GrP/jGuKskH2ziq/FXYq7FXYq7FXYq7FXYq7FXYq7FXYVdirsVdirVRXAreKtVHjirqjrXFW6jFXYq7FXYq7FXYq7CrsVdirsVaGBW8VbGKuxV2KuxV2KuxV2KtYq7FWv2sVecTf+TKn/1Y/wCOKvRk6t88VbJA6mmKuJANCdz0GKt1A74q6oxVwIINDiqm3VPnirAvPf8Ayklh/qf8bDFWe2/9xH/qj9WKtj9rFV1RWld/DFW6jFXZEq1Uda4QrTmg2yQV5B+eyhrjyx4/W2/4gckFSCQBfl3ySrJ4rbY5JrKxYrfjiqytuh4gE99hiq4CFv2TTxpiq+OKDc0PTwxVVthAp6fLbFUr84PD/htBQ1+vQdv8sZEpD3vyx/xxLf8A1B+oZAsgm37IyJSvGKuxV2KuxVx6Yq1UYq6oxV2KuxVwxVvFXYqsb7S4qoXn+8Vz/wAY3/UcVYf+XR/e3Y70/jirN36YquHXFW8VdirsVdir/9L1RireKuxV2KuxVTdvj48agjrhVjms+cLbS7j6oYiznwI74aVDr59iBVDaPUjxGNKtTz0kjkfVH29xiq1/PbLJwjs3P0jFWl88zOSraTMCOm67/jkVaj8+XEshi/RE4496r/XFWx5+uakfoef4duq/1xVd/j25/wCrRP8Aev8AXFWKfmV5xuLvy2ITp0tuDcQn1GIoKN88Vej+XpvW0i1Y9eC/8RGKpoKCuKrsVdirsVdirsVdiriNjiqDUoWMA/vE+KnzxVeJC/wpIOabOPfFVWJ2ZfiFCNsVX4q4Yq3irsVdirsVdirsVdirsVdirsirskq1hscIV81WEpXzL5p/5jj/AMRywKrQTtVq99sKCpSMqMSeuFCHdnLgr1rtiqqLg+rwnuUT/JIxVua4ZSFh+Kv7Q6YqrTS3AitwxqamppixKvbTzA0r3xQiPy2Mh/NfVi3T6vD+s5Bm9/8A2lyDIOYVqMCVhlVAFA5n2xQ4SEjmYyD0piq2N5Fd2dqhvsR9xirSKrqVj+HerYqqlh8S0+yMVYD+bH/HPtv+M0P/ABPFWeWf+8cH/GNP+IjFVbFXYqokBOZc1DdBirTTLEgPHZjQfTirZkhVipYB27Yq3SRRUGoxVI/Orq3lPU67H0T+sYqhvy7/AOUeh/1V/VirKG7YquxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVbQepXvTCqwcxM38pxVLdQ8w6JYzelc3ccMjAn4icVVdL1KyvLTlbTLcDfdMVRo5kLx+EeBxVVHTArsVdirsVdirsVdirsVdirsVbGKuxV2KuxV2KuxV2KuxV2KuxVp+mEK84vP/JjP8o/44VejMwUVOAKh5OLzUf7K7r88VS7Xtd03R7R9Qv5AixCqVNKjFWGx/nRpc86xfUn9NmCiTkKbnrhVnVrdG+hhuLSQLG27jrgVGw+nwbg3Lf4vnira9FxVgf5gf8AHbsfkP8AiQxCs6i/uIf9Vf1YhV6dWxVT4ASNMRQ0pirF9O876fqHmS50pXAls34OK96Vwq1L5+0+Lzf/AIdkYGdovVG/Y5AoLKwsfDgpwhW9wwHamFDyL89zS+8tf8xbf8QOTCsaecLHID/NklUHc12OFVkj3HD4cVW2Nw6tIsn2gpOKsfufOwg1T9Gn7UjcR9GKsntbktCGHUjFXLM5eLfviqE82lzokW//AB9w/wDExkSr3zyl/wAcG1/1R+oZBU3b+8GBVQYEuxS7FXYq0/2T8sVYPf8An6C18zWminaSSTgd/bFWaqUAqP2t8VXAACgxVvFXDFW8VcehxVT/AJcVUdR/3im/1G/UcVYZ+XP+91/8h+vFWcnofniqoOmKuxV2KuxV2Kv/0/VGKt4q7FXYq7FVkrMq1UVPvhCvNtYjkl88xJPGDGWTbr44VZ42l6XXl9XSq7dMCrv0ZpiUP1dBX2xVKPM2qeXfL1qbu9iRFpWtMVb8uavoXmGyN/ZcWiUlSQPD6cCpt9WsvTM8ca+NaYqvWztCoPpjffpiq76laf77H3YqxD80rC1byqQEAP1iHf8A2WKsg0AKunW6L0CL+oYqmaGrsD44qq4q7FXYq7FXYq7FXHptirGfO17c6Z5Y1XUrcAXcNuzDelKdMVeBS+d/NFj5atPMMV56k1yEeS3MgoCzU6DfFX0b5Z1CbUNFtLqZQskkSMwHiVBxVNcVcMVbxV2KuxV2KuxV2KuxV2KuxV2RV2SVb3wq+aLVlXzR5oB/5bj/AMRyYVFv6awqw6ljkkFLrnk0oA6YUITzFqkemaXNJ0lWMlfnTFXmehaT5s8zBdVW5eOL7RUPTr88VemaM00dn9Wm+KVRQufb3xVFuZgkCluVCd64sSqQySiQCv7QxQm/5ZB/+VpaqW/5Z4f1nIFm+gP21yDINnvgSpPKkbIGHxN3G+KHj/52ajf2zWFvZanPaSXNzFFWM0+2QMVSy9m82eRte0251W+ludID1llkbkSo8QMVe1aVfRX9jHdQ7LOodD0qCK4qi0UiI8vtd8VYF+bP/HPtv+M0P/E8VZ5Z/wC8dv8A8Y0/4iMVVsVdiqj8Rcq6jj2OKvGP+cgta1LT/wBHQ2l9LZrNcQoXjNNmYDFU98jeWp4blrg67PqLSULRysp4fKhxV6UeUcar1pQVxVJ/O6oPKWpkjf0T+sYqg/y6/wCUei/1V/VirKG7YquxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVTlIUA964QrvUIIBHXpir538431q35v2tjqtw8enPbzM3AcjyBHHbCr2PyNFpSaSP0a7SJvQuOJxKskCu1C2x8BgVVwK7FXYq7FXYq7FXYq7FXYq7FWxirsVdirsVdirsVdirsVdirsVafphCvObz/wAmM/yj/jhV6HKnNOOAKskjVyq1oV3OKvFfz3lubrzHoOlczFp9yHFwwO2x2whVTzPq1p5WsLCI6PbyI3pASkEk1A32xV6f5YvheaFa3dtEiCVa8F2AwKnUaKqGgAJ3NPHFWl6LirA/zA/47dj8h/xIYhWdRf3EP+qv6sQq9erYlViujOwqTtuD0xV81nVfNem/nBrp0rT4LiNrr4ebkdh4A4VRWh32sal+faPrdpHZ3I08cYoiWUgdDUgZAoL6KShcgdRhVf3rkkPJPz2TleeWv+Ytv+IHJBWH3SEJJ/rDJKpGOlPiwqvbkE2OKqMalXMjdG+HFXmOu2//ACErTUH2ZZGrir1K0iLx8F7D9WKrktZQ8W/fFUP5sikGiRb/APH3D/xMZEq9+8pf8cG1/wBUfqGQVN2/vBgVUGBLsUuxV2KuYgAk9B1xV4Fr4Wb867Hhui3Ir9xxV7wpQgCnQYquRuQriq7FXDFW8VcehxVT/lxVR1H/AHim/wBRv1HFWG/lz/vdqHyH68VZweh+eKqg6Yq7FXYq7FXYq//U9UYq3irsVdirsVWSKH+E9MKvPNVdj59RP2QUp+OFWf8ACgfj1JxVYXZpeNaKoqcCvFvzl1KyvvNmj+X9Rl9XS74SCeOJuLLStKthVBf848apFDNf+WYXK8JppERzyb0uRp+GRV7miKrLbwn93H9sHc4qionVgQP2dsVX4qxL8z1DeVnB/wB/xH8cVTPyv8ejwyN9qlK/IDFU3VRTl3OKr8VdirsVdirsVdirj0xVhn5jqR5N15x8TvaOop0xV86WPlryi35Zx3i3JfXgsZWD1Sd6mo4Yq+jfyqvL+78pW73sZjlSiKpFPhUAA4qzGmKupirsVdirsVdirsVdirsVdirsVdkVdklWMSCKd8IV8zSBE8zeZSOpvTX/AIHJhVVpYvRVT/NtkggoeQskysSOOFhbHfzAtDdaZLJFuUQnb2GK2xzyP5q07T/LEMMx4zslG3pv8sVtnUF3BNYq1u4HqUrXfritoyGFI44ozVuJqDimnRN+8J4nZsVpNvy1mb/lZ+oU25QxA/ecgUvoWgrXwyC2sPxRsTv8sCbU/TPpow+EqNuXbFLxT/nIBbQ3mi3EtwiLDe27MK0+ywxVQ/NXzFD5yNtoOlRtMj1SWZNwa+BHTFXsHl2xmsfL9hbv9uGFE+QApviqcAfCamp74qwH831C6DDKPti5gAPsXxVnViSbK3/4xp/xEYqiKYq6mKqElGlVSp2/a7Yq8R/5yQ0+O+g0qOSQLGt7A0vY8AwrvirJfJFv5BsdRm/QjyyXMvET1mLgU6bEbYq9K4FwrKdqYqk3nheXle/Hb0jX7xiqA/Llj+g0XsAKfdirKxv17YquxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVRuEBo3eowhWpn4yR7VqcVeIan5Im1T88bG4vYXOmC0nDEbfEStN8KvZ9J0Sw0qD0LNSsY7E1wKjqYq3gV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2Kt4q7FWsVdiq0mrU7YVed6kOP5ghh1bgD+OFXodWNQOxwKsdEDcz1bY4qwX81PIb+ZdPie0YJdWy0hffua9sQrBtV8r/mLqgtbDUJopLOAIFpEQ1FoBvX2xtXrvlHR30nRoLSRvsLSmNqnR60HSmBVqj4RirA/P4H6Vs27ig/4YY2rOEJEEFO6r+rFVVerfPFVJuTu6KQFK7H3xV5zpH5f3lv55u9WlIaGWXmdjvthtV9x5CvpfzPXzPbyIkSWwtyCPDIlXorAowZeh+1iEU2PtfD9nv8APJBaeVfnrQHQJP2luWI/4A5ILTD5nDRiv7e5+eTWkI8C9q/fhQ4BhsemKuaSMLwf7Naj54qxK78r3k/m+y1QmsMTlkNOxxVlllM6xSMvUMRihUjuZCYyw74otD+Y7hZNGQODtdRU/wCCGRKQ998oMToNr/qj9QyDIBOj/eDAml69MCt4q44q1iqjeLM1pMsJ4ylGEZO4DEbYq8x0H8u9a/xImtatMk10j81ZFKivyqcVeorRBTqT1xVcAANsVbxV2KuxV2KrDsyjFVK9HKznB/kb9WKsL/Lr/jp6kvYAU+/FWcHofniqopOKt4q7FXYq7FX/1fVGKt4q7FXYq7FWj1xV5zqv/kwF+cf8ckr0Nf2sCqY9NwxXpShOFXktr+VM2q+dNW1TV5WMCzctMqAeKECtMVW2X5W6joP5ijXdLJW0a3EUgFACT1ORV61CU4hwKSSdT44qrxIEG3fc4qvxVin5m/8AKLv/AMZov14qmPlT/jhQ/L+AxVOB9gYquxV2KuxV2KuxV2KuPTFUv1CyS7hms5YFe2nXhID3B64qxWH8rPJUM8YSxiVl/ZC7H8cVZnYwRwQLFFGI0TZVXpQbYqr4q7FXYq7FXYq7FXYq7FXYq7FXYq7Iq7JKpv1X54Qr5pmj5eZfMn/Maf1DJhXS2ZaJSv7JrkmJQF2ZWIUdsLBY8PqRejMKo44sD4HFWP3fkrRJLpIoFCop6AYqnot7OySKCMbLTt4YpTF7gfuyo2J2xZKqkiMtx/aGKo/8tGLfmXekin7qL9ZyBV9DjvkVaj+yfngKhYAauCee/Q4GSS635Q8v64FXU9NhueDBl9QE7jp3xVvS/JvlvT7n17SxihkBqoQHb5b4qnID+r8QpGBsMVX8KFmr17YqwL84f+Ubi/5ioP8AieKs4sf94rf/AIxJ/wARGKonFXYqouGMqkHYdRiqW6p5e0bVm46lYRXKLupkqdx074qh9M8neXbKcz2WnxWkjbu0YNWp41OKp2a7KnQYqlPnN1HlfUQephP6xiqWflz/AMcVfkP1Yqyte+KrsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVU5l5KB4GuEK09WCsBUriqmsUL3KzGMesooH7gYVROBXYq7ArsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVbxV2KtYq7FVp+3hV55qn/Kfr80/jhV6CtatTrXArYIcUYb4q0sZVq8tvDFW3AqDSuBW2QPTfFWtq08Birk+yMVYD+YZpqVmfdf+JDFWcxb28H+qv6sVVqU5YqplPgJj6+OKrlLiEV+13xVYsJClAxBJ5VyJVenFVEbHkffCreyniBtkgryj89949C/5iG/4ickFYXJGxjT5ZNVqch1wsWpK02xVDPC06tGPtqOWKohRcLa2wI7dcVQUErwepFSpJLYoKKtmdxH8OLFDeYkb9EJt/x9Rf8AEhkSyD6A8n/8cG1/1R+oZBkE7P8AeDAlevTAreKuOKtYq0emKtKG6k4q4gdQKnFXISVqRQ4quxV2KuxV2KrW+2uKqV5/vHP/AKjfqxVg/wCXZ/3L6mPYfrxVnfY/PFVUYq7FXYq7FXYq/wD/1vVGKt4q7FXYq7FWj1rirznV/h8/xk9ygH44bV6HxbenU4q4RUWg79cbVSNsxrvSn2afxxtV3C49fcr6HGlO9cCqghQEU7dMVbjVxXl47YquxVi35kxmTyy6jr6sZ+44qj/KZ5aFDT5fgMVTflSi9ziq/FXYq7FXYq7FXYq7FVnpDnyqa4q0Yi1eVPYjriroIFhTipJBNdzXFVTFXYq7FXYq7FXYq7FXYq7FXYq7FXVyKurkkWsYVIwhbfNs1vL/AIp8woCKyXZYfQBkwtqqwTqhUuu2SQUvlWjkVBOFjTkiaoLbjFacsVLkslNztitLWtpjLycr7bYppXktJykbArRTU0xW0WIpDCI9uTEGuK2ify4Vk/Me85EGsUXT5nIFL6EHfIJpqPYYrTjG3xU6nAlr0iW5V6imKtxwiNaL198VbRZCpDkV7UxVqnFaYqwL84v+UbiP/L1B/wATxVnFh/vFb/8AGJP+IjFUTXFXVxVY3Kh49e2KqZjlKUJFa7/LFV6xKvSu+Kt8SB8PXFUm83xF/LOoV+0Yj+sYqlf5buDoxH8lAfuxVliGoriq/FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FVvE869sVcAQT4YbVwDV3p7YquxtXYq7ArsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVbxV2KtUxV1MVaINa4q861f4fP0ZP7RQfrw2r0Piw5U6k4q54+QHjjatNECtKnG1XoG40bAruGKreAUmnhirk+yMVYB+Y6kX9mfdf+JDFWdW4/0aD/AFV/ViqvWoIxVyqF6Yq1Ri3tirfEcq98iVWmJS3LvhCLXEGmSC28o/PNhx0IH/lob/iJyQW2JSzRJHHUdRk1tqT0h074WNrAiUqSKYra1PTR5JB+0vEfPFbXysXsoUBAeMUJxW1ht4WiLinqAb4otTs7hSY0C7g4oQnmeVhpCdP96ov+JZEsg988mHnoFqw/l/gMgWQTs/3gwMl69MCG8VccVaxV2KtFQRTFXKvEEDFWwDTFXUxV2KuxV2KrG+2uKqV2f9FmXxRv1Yqwb8vTx1vU1PWi/rxVnwGx+eKqlcVdirsVdirsVf/X9UYq3irsVdirsVabFXnOuf8AKeQ/6yfxxV6MOuKt4q7FXYq7FXYq7FWM/mKaeWpD/wAWp+vFUT5Oaugw/M/wxVOD/eJiqrirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirWRCHZMIabqMKvm66kK+btb/wCYlv1DJhVjyNIWUGmFUE8Egfl1wqrJzIpTFW0URy/EcVauWDMApxVXS4CQhGO4xYlFK4Zkp4YoVPy5Df8AKybyvT04/wBZyBZvoYdMgWTSdMCV+KHYq7FXYqtk6YqwH84v+Uai/wCYq3/4nirN7D/eK3/4xJ/xEYqiMVdirsVdirsVdiqU+bP+Ucv/APjEf1jFUk/LX/jkTf6w/UcVZbF9j6cVVMVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdhV2KuxV2KuGBWxirWKuxV2KuxV2Kt4q7FXYq7FWmxV5zrf8AynkP+sn8cVejDFXYq7FWxirsVWt1PyxVan2RirAvzKNLy0+a/wDEhirOLU1tYP8AUX9WKq4G5xVvFXYq7AUOxCuPTJBDyX89tk0I/wDLw3/ETkgrCLmSqRj2yauaSNqb4WLRmjpSuKENNccRRemKrhNI0W2KqsDScGJ6UOKt2igcG71xVB6+eWlCv/LVH/xLIlkH0D5I/wCUdtvl/AZAsgnh+2MDJcvTAhvFXHFWsVdirsVdirYxV2KuOKtYq7FVj/bXFVG7/uJf9Rv1YqwfyB/ykGqfJf14qz8d/niq4Yq3irsVdirsVf/Q9UYq3irsVdirsVabpirznXCP8eQ/6yfxxV6KCK4quqPHFXVHjirqjxxV1R44q6uKuxVi/wCZFf8ADElP9+x/rxVX8lt/uBir4n+GKp5X40PanXFVXFXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FWsiEOyYQ0eowq+bbtf8Anb9br/y0t+oZMKpyAq1R49cIVZLKqr1BOFVkV6o2479tsVUJGeSXrTFWvTZXBLV+nFVS52TlXFiVe0mf1IxuRTFU2/Lxl/5WLd9j6cf6zkCyfQY6ZAsmk6YEr64odXFXVxV1cVWv0xVgH5x/8o1F/wAxVv8A8TxVnFh/vFb/APGJP+IjFURirsVdirsVdirsVSnzZ/yjl/8A8Yj+sYqkf5an/cRN/rD9RxVl0X2ad64qqYq7FXYq7FXYq7FXYq7FXYq7FXYq6uKurirq4q6uKurirq4q6uKurirq4q7FXYq7FXYq7FXVwq6uKurirq4q0MCrhirWKuxV2KuxV2Kt1xV1cVdirsVabFXnOt/8p5D/AKyfxxV6KMVbxV2Kt4q6uKrW6/Riq1PsjFWA/mZ/vVaHtVf+JDFWcWZ/0SD/AFV/ViqJHfFXYq7FXYCh1cIVa3TCEPJ/z42i0P8A5iG/4ickFYBIxISvhk1VoxEV3wsVFgnPYGmKtTQoaEfdiq6gji6Yqq2squjL7HFVtmx5qD0B3xQhvMgC6StN63UX/EsiWQe/+RyD5dtfl/AZAsgnp+2MDJcpFMCG64q44q1irsVdirsVbGKuxVxxVrFXYqsf7a4qo3f9xL/qN+rFWD+QP+Ug1T2C/rxVn47j3xVcMVbxV2KuxV2Kv//R9UYq3irsVdirsVWudsIV5n5vFzaebYr5Yy0YZa09q4VTqDz/AAPzrDV1NAtcVVf8eRf8sp+/FWv8eR/8sp+/FW/8dL/yyH78irv8dL/yyH78VWnz09fgtDT54q7/ABzN/wAsjffirHvPPnCe50F4hblCZE+KvvirLPIx9XyvC7faJP8ADFWRRrWJVOKqmKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuyKuySrT1GEK+bNULDzXrdP+Wlv1DJxVRUSSKRTxySChXgkD9MKGirhgKd8VVPQYy/TirpLdg4xVdPEhjoRU4sSjbNOJTivbFVfyCHH5l3JIoOEf6zkCyfRQ6ZBktXp9OBK/FDsVdirsVaPTFWAfnJ/yjUX/ADFW/wDxPFWb6f8A7x2//GJP+IjFUTirsVdirsVdirsVSvzR/wAo/ff8Yz+sYqkH5a/8cq5/4yD9WKsxX7Z+WKrsVdirsVdirsVdirsVdirsVdirsVdhV2KuxV2KuxV2KuxV2KuxV2BXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXDFW8VccVaxV2KtjFXYq0euKvOdc/5TyD/WT+OKvRh1OKt4q7FWsVdirR6HFWk/u8VYF+ZvW1/1k/4kMVZtY/7xwf6i/qGKq6dWxVdirsVdkSrsIVo5JXlP56JzXQgf+Whv+InJBWIvYI0aH2yaqX1RMLFctog3xVbJZITzxVdNbQtDTFXWunw8SR1xQVWGxQJL8sWKW+YLRRpcf/GeP/iWRLIPdPIv/KP23y/gMgWQT8/bGBk5OmBC7FXYq7FXYq7FXYq2MVdirjirWKuxVY/20xVZc/3Ev+o36sVYL+X3/KRaz/qr+vFWe/7tHyxVeOmKt4q7FXYq7FX/0vVGKt4q7FXYq7FVrU+nEKgpYbS5ek0IYjoSK5JVJND0pJaiBOTb0oMFqr/ojTv98J9wxtXfojTv98J9wxtWxpun0/uE+7Arf6O0/wD3wn3Yq2NN0/8A3wn3Yq3+jbD/AHyn3DFWO+fNMsT5ekpEqnmhqAMVV/I5A0GOMfZBb+GKsiWgoMVXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7Iq7JKpufiXJBXzreqp82a3X/lpb9QyUVVUEKJXxrkkFT/cscKFCdYgwxVdE0ZlxVu4MYYYqrxi2aMFhvixKLWWCN46L+z4Yqt8mTI/5izcVp8Mf6zkCye/jpkGS1en04Er8UOxV2KuxVo9MVYD+cn/KNRf8xVv/AMTxVm2n/wC8dv8A8Yk/4iMVROKuxV2KuxV2KuxVK/NH/KP33/GM/rGKpB+Wv/HKuf8AjIP1HFWYr9s/LFV2KuxV2KuxV2KuxV2KuxV2KuxV2Kuwq7FXYq7FXYq7FXYq7FXYq7ArsVdirsVdirsVdirsVdirsVdirsVdirYxV2KuOKtYq7FWxirsVaPXFXnOuf8AKeQf6yfxxV6MvU4q3irsVaxV2KuPQ4qtT+7xVgX5m9bX/WT/AIkMVZtY/wC8cH+ov6hiqunVsVXYq7FXZEq7CFcckryn88/s6FT/AJaG/wCInJBWKMz+knyyaqCcycLFuUuBiq+Jgbb4utTiq42zPFUHFVGL1ISd8UFVgmZo5flixQOv8zpaf8Z4/wDiWRLIPcPI3/KP23y/pkCyCfn7YwMnJ0wIXYq7FXYq7FXYq7FWxirsVccVaxV2KrH+2mKrLn+4l/1G/VirBfy+/wCUi1n/AFV/XirPf92j5YqqDpirsVdirsVdir//0/VGKt4q7FXYq7FWiiluXfFXcRirRjUsGPUdMVXUxV1MVdQYq6gxVriMVdxGKsc8/qv+HJamnxrv9+KteRuA0CIhgSWYV+7DSsgqGPwtuMVVBXFXYq0W98iVdyHjiruXvXFW6nFXVOKtVOSV1TjSu+LFXfF44q2K4CrdcCurirq4q6uKtVwhXYVdirsVdXArQJORQ2SckFU2J68akdMki3zxeSW483asrihadianvTJhbUuKuhPKq1NBhW1IFFOFVKZ0LDFWoJ4UuaOdq4quvZEeUCM7YqmFhbxcX9XcAfDixLbBhOpr8AG2Kt+S2YfmGzA7MIx+Jyssn0KetMiyco2wJXYodirsVdirTYqwH86hx8nJIPtC8thX/Z4qzbTv94rb/jEn/ERiqJxV2KuxV2KuGKt0xVLfMSBtEvAehjP6xirGvyxJOn3ynoswA+44qzQda9ziq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXUxV2KuxV2KuxV2KuxV2KuxV2KuxV2Kt0xV1MVaxV2KuxVumKupirWKuxV3fFXnPmb4PO1ky7F5VDfKhxV6KvU4quxV2KupirqYq03TFVpFKAdDirAvzO2ht27iSMD/ghirNrTazi9o1P4DFVdOgPj1xVdirsVdkSrsIVpskFeXfncimDSmPWOZivz4nJBWHCRzZo1d6DJqpSTmPphYoeS6mY17YquSdTGR4Yqr2F47IynoOmKrI5Xldg3QVpiqxGnVZeB7YopB63PcfolKn/d0f68iUvdPIJY+WbZmO9P6ZArbISakYE2vUUGBK7bFWvlirqHFXEHFVtf8AKxV1f8rFXcgP2sVdzX+bFXc1/mxV3Nf5sNK0XWmzb40rRB4Ak/EO+KqF7IVtXIO5BB+7GlYT5BkUeadaj6UVf14FZ4nIEs/jQfLFVYdMVdirsVdirsVf/9T1RireKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2Ksd8+xtJ5dlVaE81O/04qhfIdxCugxI7IGDt3GSVkSXNsHYc0G/iMVVPrNv/v1P+CH9cCu+s2/+/U/4If1xVo3Nr3lSv8ArDFXfWbP/fsf/BDGld9as/8Af0Y/2Q/rjSu+t2n+/wBP+CH9cVd9btP9/p/wQ/rirX1q0/3+n/BL/XFXfWrT/f6f8Ev9cVaN7ZjrcJ/wS/1wq19dsv8AloT/AIJf64q769ZD/j4T/gl/rirf1+x/3+n/AAQ/rjSu+v2X+/0/4If1xpXfX7L/AH+n/BD+uNK76/Zf7/T/AIIf1xpXfX7L/f6f8EP64KV31+y/3+n/AAQ/rirvr9l/v9P+CH9cVd9fsv8Af6f8EP64q0dQsaf36f8ABD+uKrRqVh/y0J/wS/1yLFttSsP9/p/wQ/rkgrk1KwYlRcRk/wCsv9cKvnbVopZPOOqNAUf9837Q8Bkwrdut36BE0aooJoVNcKoSaoc4VQ8iOSCMVUxZzPOGriqYLYOoDk4qrTPIIKqe2LEq7rI9qhH2qDFV3kZXHnleXX4P1nKyyfRB+0MiycnTAq7FXYq7FXYq02KsB/Oz/lCk/wCY22/4nirNtO/3htv+MMf/ABEYqicVdirsVdirYxV2Kpf5h/44t3/xjP68VYx+WH+8Oof8Zx+o4qzTwxVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirYxV2KtHFXYq7FW8VdirWKuxV2KvOvNX/KaWH/GZf1HFXoi9Tiq7FXYq7FXYq02KtP1XFWA/mf8A7zwf8ZY/+JYqza0/3ji/4xr/AMRGKq6dF+WKrsVdirsiVdhCtNkgrzD87f8AefTf+Mrf8RyQVhS/7wp8hk1WXAV4+QwsViCMwNtuMVQdvIpLj54qirJ0UPXFW4nUhivWpxVfAfglr4Yql/mQSDRKxiriRCgPQmu2RKs98kav+Y/+GbcJpduV3ofUPT7sirIP0t+ZIZP9xVt03/en+mRKrpNX/MnltpVtT/jKf6YErf0v+ZX/AFarf/kaf6YpbGr/AJlV/wCOVb/8jT/TFV36Y/Mn/q1W3/Iw/wBMVd+l/wAyTt+irb/kYf6Yq76/+Y3/AFbLf/kYf6Yq39f/ADG/6tlv/wAjD/TFXC9/MY/9Ky3/AORh/pirvrn5jf8AVst/+Rp/pirRuvzGP/Sttx/z1P8ATCFa9f8AMX/q3Qf8jD/TCrRn/MTvp8AHc+of6YquD+emTj9Wj/4M/wBMVWTQeepIiv1ePf8A4s/sxVGeTfL97p9zdXt8oS5nA5qDUbHxyJVljHkgr3xVcrHmR4DFV+KuxV2KuxV//9X1RireKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KoHW9Li1PT3s5a8X8DTcYqxCx/LeS0iWNLggBi32m7/ThtUSPIk3NmNwaE/wAzf1xtV/8AgCLvM9f9dsbV3+AIf9/P/wAG2Nq1/wAq8tmNWmkr/wAZGwiSu/5V1af7+k/5GNh4lcPy5sCaSSylfASMMeJV3/KttG/nn/5GtjxK1/yrbRv55/8Aka2PErX/ACrTR/55/wDka2RtXf8AKtNH/nn/AORrY2rv+VZaEd2acn/jM2Nq7/lWOgfzXH/I5sbVsflj5frubin/ABmbDarv+VY+XPG4/wCRzY8Su/5Vj5c8bj/kc2PErv8AlWPlzxuP+RzY8Su/5Vj5c8bj/kc2PErv+VY+XPG4/wCRzYLV3/KsfLn/AC8f8jmxtXf8qx8uf8vH/I5sbV3/ACrHy5/y8f8AI5sbV3/KsfLnjcf8jmxJVoflh5bH/LR/yObIIpx/LDy2f+Wj/kc2SC0sH5XeWkfmv1nmf+LmyQWnin6C0zTfOGpIglY+s1KuT2HjkwtJnCKRsqg0qaVwoKDlT4+mFFthF2rittSoymqsMVtcbiRo+HfxxW2gkgtvi3qNsUImCVk4IwrUYoVvKRMfn9Qeh9On45WWb6EP2hkWTk6YFXYq7FXYq7FWjirAfztP/OlJ/wAxtt/xPFWbad/vDbf8YY/+IjFUTirsVdirsVdirdcVS/X99Gux/wAVn9eKsX/LE/6FqA/4vH6jirNelBiq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FW64q6uKtYq6mKupireKuxVqmKupirTHiK4q8682fD5y04/zTL+o4q9GAoTireKuxV2KuxVa5oMVc3VcVYD+aY4WUMp3AmiFPm2Ks2s97OH3jT/iIxVWBoVXvTFV9MVdTFXZEq7CFaIrkgry788W4Wmmt1rK3/EckFYVEednGvSqg5NV3JIrU8xyJr0wsUNGyhCaGjdMVWw2gSRqnqK/firkRQHxVvT2Wr7V64qiItw1BTnsMVQ+tcP0LUj7MyD8ciVez/l8zN5Wtm27/wAMgVZIeRIpTAq4A98CW6YpdQ4q7i3tiruLe2Ku3xV2+Ku+LFXfFirhXvireG1djatUxtWqNX2xVumxHjgVTlidqcTSmKqiihriq6oxVrkMVbBririaYq//1vVGKt4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq44q1irsVdirsVbGKuxVxxVrFXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7Iq7JBVrdskr551SMN511H/jM36hkoqilRI4yT3JyQQUvlZC+2FrU3QkbYqsRHJ3xVz0UYqqq5MC7bYqiiPiiNO2KXeWmJ/MFAR3j/jkCyfQh+0MgycnTAq7FXYq7FXYq49MVef/AJ2/8oUv/Mbbf8TxVm+nf7wW3/GGP/iIxVE4q7FXYq7FXYq7FUBrv/HIuv8AUOKsX/LH/eTUf+M4/UcVZq32hiq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq2MVaxV2KtjFXYq7FXYq7FXYq7FVsv2fpxV515v/wCUw0z/AIzr+o4q9HxV2KuxV2KuxVbJ0HzxVzdVxVgX5sf8cqL/AIzxf8TxVm1j/vJD/wAYk/4iMVVG/vU+WKquKuxVo5Eq7CFWuaCuSCvMvzsXnZaZ/wAZW/4jkgrCmQx20FP5cmqy4gla2GFio/VphEuKohYjzPI78RiqmkKkPviqy1h4hyOu+KqtvHIwSmKofW7Wb9COa/7vT9eRKvZ/y6/5RO1+n+GQKsmHbAq7Al2KXDFW8VccVaxV2KuxV2KuxV2KuxV2KuxV2KuxVosB1xV3JTiruOKtFn8MVbFSKnFX/9f1OSAKk0GKrsbVosAKk0AxVsEEVGKuxV1RirqjFXVGKuxtXY2rsbV2Nq7FXYq0TirqjFXVGKuqMVdUYq4EYq3UY2rRIxV1RirVRStdsVbxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuyKuyQVa53GSV886o9PO+oL+0ZnoPoGSBVEMQ8VCaUJwhiUCYCH2yTBUpSgOKueg6YqpGNX6nFURwRLdVG5xSrBwZIwB0G+Bad5fKHz9EV8Y/45Asnvx+0MiybXYYFXYq7FXYq7FXHpirz/APO3/lCl/wCY22/4nirN9O/3gtv+MMf/ABEYqicVdirsVdirsVdiqB13/jkXX+of14qxX8sSPquojv64/UcVZsftDFV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVsYq1irsVbGKuxV2KuxV2Kurirq4qsk3H04q8784beb9LrtWdafccVejgg4q7FXYq7FXYqtk6Yq5uoxVgX5skfomI9vXh/4nirNrH/eSD/jEn/ERiqof71PliqrXFXVxVo4Crq4hWiKjJBXl353TRxWelmRgo9Vtzt+zkgrCLjV9J+rQKbuINwG3IZK1XNqWlm3A+uw/LmMkxWSajpfoqBew1p/MMVbGo6T6hJvYvs/zjFXRaho9Hrew/8ABjFWob3Ryrj69CDv+2MVbt7/AEtAn+nQ/wDBjBaofWNV099HaKO7idzOlFDAk74Cr2n8vNvK1sh+0Kkjv2yBVkvJQKk0AFScCoD/ABJoFSPr8FQaH4x1GNJd/iTQf+W+D/gxjRS7/EugDrqEH/BjGirf+JvL/wD1cIP+DGPCVaPmby//ANXCD/gxjwlWv8TeXv8Aq4Qf8GMaKtf4p8u/9XGD/gxjRVr/ABV5b/6uVv8A8GMaKu/xV5b/AOrlb/8ABjGirR82eWh11O3H/PQY0Va/xd5Y/wCrnb/8jBjRV3+LvLH/AFc7f/kYMaKtf4w8rf8AV0tv+Ri40Vd/jDyt/wBXS2/5GLjRVo+cfKo66rbf8jFxoq0fOnlMf9La1/5GLjRVa3nfykP+lrbN8pFxoqpt588qDpqMB+TrjRVRb8wPLY6X0P8AwYxoqi9O84aFe3EdvFdxPPN/dRhgWb5DAqd9q4q//9D1LKKoRx5e2KqU8oihaV29ONFq3sBh4VYHf/nX+XunzT297qqBo9mU1w8CqK/n/wDlvxHHUo+PbrjwK3/yv/8ALn/q5R/jjwKsf/nID8vq/BqCMPpx4FW/9DAeQf8AluT8ceBXf9DA+Qu16lfpx4Fd/wBDA+RP+WxPxx4Fd/0MD5E/5bE/HHgVZ/yv/wAkf8tifjjwK7/lf/kj/lsT8ceBXf8AK/PKTbxTLIvc748Cu/5X15Y/mX7zjwK4/nz5ZOwZfvOPArv+V7+XPFfvOPArX/K9/Lniv3nHgVb/AMrz03tbgjsanHgV3/K89N/5Zh95x4FbH546eelsPvOPArv+V4af/wAsw+848Cu/5XfYnYWoP0nHgVkXkzz5a+aLi6ghUI0KBiAa0qaYDGlZWgJHBjUY8Kq46YFdirsVdirsVdirsVdirsVdirsVdirsVdirsVdiqlM68lHevTFXhXmny55oi83TX+n6Q10rFyCDStaYqlkWnfmI8ZZ/LDqORo3Lrk4sSpvZfmGpoPLb/wDBZO2NL0078wGFT5cf/gsbWlh0z8wT/wBM2/8AwWNrSm2k/mD/ANS2/wDwWNrSuNN/MBYk/wCdbev+tizDoLT8w/WofLb0of2siUu8qw+Y4PPMB1bSzYRs6CNya8jvtkSr6KO7qciVXN2yKrskrsVdirsVabpirz/87f8AlCl/5jbb/ieKs403/eG2/wCMMf8AxEYqicVdirsVdirjirWKoPWf+OXc/wCpirEfyu/u9U/5iP4HFWcr9o4q3irsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVbGKtYq7FWxirsVdirsVdirWKuxVr9oYq8588/8pXo3/MSP1Yq9Fj+03zxVfirsVdirsVaPXFVq/aOKsA/Nv8A44Sf8xMP/E8VZzZf7wW//GJP+IjFVT9tPliqpirsVdhCuxVZKjMtFbia9cVedfm9YWV8NIt7tBKjTMCD/qnCFVLP8ofy/lgtpZtIhkYpuSD3xVF/8qc/Lv8A6skH3YbVv/lTn5d/9WSD7sbVYfyb/Lyv/HEg+7G1d/ypr8vP+rHB92Nq2v5N/l2DvocB+jElV/8Ayp38uf8AqxQfcciraflB+XiOrpocCspBVqHYjocVZXYWFpYxCOBAigUoPDFVmpBl0+7dT/umQ0/2Jwq8U/KjyFa+ZfLMuqXb/vmvLmPcV2R9sInSsz/5U3pP84/4HD4itr+TmjA/EVYeFMfEVd/yp3Qv5V+7HxFd/wAqd0L+Vfux8RXf8qe0A9Y1+7HxFcfyY8tnrGn3Y+KrX/Kl/K/eJCflj4qu/wCVL+Vv98p92Piq4fkt5Tr8dtG48KY+Kq7/AJUr5O/5Yo/ux8RWv+VK+Tu9lHT5Y+Irf/KlfIv/ACwRfdj4qu/5Ur5F/wCWCL7sfFVcv5NeQxsdHhkp+3Trj4iqg/Jf8um+1ocH3HHxFXj8lvy2XcaHB9xweIq8fk/+Xa9NCg+448fmrbflN5B4kDRYVHjQ7YONWDX3lrQtF/NnyvFpkSxbSbL7HIq9pRZACS1fDFX/0fU7CopiqXa5SPR7og1pG/X5ZLiV5T+WPl/RNVu7+e/so52FCOaBh+Iw8avSR5M8qUFNKt6f8Yl/pg41b/wb5U/6tVv/AMil/pjxqvTyb5Xp8Ol230xJ/THjVd/g7yz/ANWu1/5FJ/THiVw8n+WQa/ou1/5FJ/THiVd/hPyz/wBWq1/5FJ/TBau/wn5Z/wCrVa/8ik/pjau/wp5a/wCrVa/8ik/pjau/wp5a/wCrVa/8ik/pjatr5V8uDpplsPlEn9MeJV3+FvLv/Vut/wDkWv8ATHiVw8r+XQa/o63/AORa/wBMeJV3+GfL3/Vttv8AkUv9MbVr/DPl7/q223/Ipf6Y2rX+GdB/5YYP+Ra/0w8at/4Z0H/lhg/5Fr/THjVseW9CH/HjB/yLX+mPErf+HdD/AOWGD/kWv9MHEqheaDoUdu7fUYvsn7Ma16fLCJK86/LBrdfzD8yW0C+nHHBGQKU6tiZK9Z4hByG+JkqqOmRV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KqcgYt8NK++KrDAjbuAG8cVaW2RFoNx1whW/Si7qMNrTjFH2GNrTgkX8uNrS70o/5Rja0taFOS7bYQVXelEDsorTwwK8083Hh5s0ugpW4H6jir0xdwp9siVbG4+nIqvySuxV2KuxVpumKvP/AM7f+UKX/mNtv+J4qzjTf94bb/jDH/xEYqicVdirsVdirjirWKoPWf8Ajl3P+pirEfyu/u9U/wCYj+BxVnK/aOKt4q7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FWxirWKuxVsYq7FXYq7FXYq1irsVa/aGKvOfPP/AClejf8AMSP1Yq9Fj+03zxVfirsVdirsVaPXFVq/aOKsA/Nv/jhJ/wAxMP8AxPFWc2X+8Fv/AMYk/wCIjFVT9tPliqpirsVdhCuxVphUYq8+/NBP9I0bgfi9c9f9U4VZlpgn+o29eNOArgVHcB4nBau4DxONq70x4nG1d6Y8Tjau4hd6k42ruXzwq7kPfFXAg9sVQerKx0u83oPRk/4gcKvPP+cfOI8gsKk/6fd9P+MmRIV6ZVffBStqFJ740q/gPHGldwHjjSt4KVaWOPCrgR3w0rvhxpXb/s9cFK7957Y0rX7z2xpXUfwGNK6j+AxpWw1Nj1xpWigbucNK0EVDWpNcFKuBHvhpWyRQ40rybzZ6T/m95XoOJpJ7d8KvVEjYIwJqCOuKv//S9TnFUBrsSPo92GFR6Tn8MFK89/Jlj6mpp+yvHiMaV6kOmNK7GlWnlyrXbww0rfIkbYaVRaT94Pj2H2h740rTs5QMX9Oh3r3GClXmdKpQ7N0ONK0ZJeynDQVwmKg8v2epxpXKZAa15AioxpW0djGR/uwdsFK5mYqAGo/WmGlWpOx+0tB0rjSrzI3NQFqp6t4YKV3P4ar8e/bGlWyCQqGU8fEY0qpGQy7GvjjSr6YKVTnH7p/9U/qxpXk/5eKD+a/mpT0+rxbf7LGlesgACg6DpjSr8KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVoIoYt3PXFWmiRjUjFV1BirRUHFXUFMVa4jFW6YqpycgVA74q3Tf6MbV5z5zjD+ZtJI6/WB+rG1ejJsqqetMVajV1HxGvhgKr9/HArt/HG1dU42rfI42q1nbstcVYB+dp/wCdKWop/ptt/wATySs405l+o22+/ox7f7EYqisVdirWKuxV2KuxVDamoawnU9Cu+KsJ/KlmZdbBP2Lyi/KhxVnq/bOKr6Yq6mKtHFXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FW6Yq6mKupirsVdirsVdirqYq6mKrJNtxirzrz7t5n0MjqboV+7FXo6gAn3xVvFXYq7FXYq0ftYq0BRjirAfzfAXy4jdD9ag3/wBnirNbBg2n29DU+kn/ABEYaVV35IKduuKqlMCt0xVrCrdcVaatD2xV5z+Z3L1tDZRyY3DV/wCBOFWc6ZU6dbll3CDbAqOA2yNK3TGlaxpXY0rsKt0GKuoMVaoCMVQWqiul3g7ehJ/xA4q88/5x5AXyAwHT9IXf/JzCr08Yq1JWgp474hUi84eZo/Lmi3GpyL6qwry4VpirBvy4/Oq084Xtxp6R+jdwh5QhapKJU/wwqm+gfmW+qa1rVgtqwOlqrMKjuaYaY2w+L8/tXvZL+LTtBmuRZTmOSVGFAF698aW2YeSfzWsPNUM/BPqt5ZrzuLUmrAVpvjS2w7/lf/mXUNbv7DQfKdxqUenTGCaSJ1pWla7nGlt6h5Q1/UdY0OO91GyfTLqR2Q20hBYU77YCFth/5g/m9deXNas9AstNe+v71WMbIwB+E06E4rar5Q/MPzHeaFf3es6XLZXFossqrIRVlSpA2PfFbQ1j+ck175GuPMJ097f0YxJQsD1+nGltIdO/PTzbJG12/lm4k05FLNdcl40ArXr4Y0tss/5W9Y3H5fzebbSD1RFG0j26tuvEgdcWTFtB/ND8wNevrW6s9EuF0yVQWIIK7kb9cVe0WTzyRrJKpRmUVjPUGmBUTirsVeT+dUX/AJW55VPekn68CvVeIEVB0IxV/9P1QcVQWtf8ci7/AOMT/qxV5z+TP9/qv+xxV6mOmKuxVay1Na4QqhfSvFaSPGKsFqMVfOmj+Z/PXmbzZr2mWDcfqdyUUlioFFBp0wqnnl/80PNmj+YotC8zQRIsjBI5ASxJY0G5AxVQ8+ebfNt7+YVp5f0147e1LsnqGTgaD54q9A/L/SvMVqrDULoXC8ieQkD/AKsVST88fO17oNpb2lsTG13yUyCopT3xVD/l9p/m+K5jv5L0XdhLbNIayhyGK1AoMVUPIvnbVNQ8+earS5ekemwRyRpXapemKsR8s6l+YHnXWdeexnMdvp920SUkK7AVH6sVZH5L/M7XLLzT/hHzBEFkXj6c25JLkjqR7Yqxu31rzZ5k/MbzBpKaj9UstPuzDGfWCbcQe9PHFXtPkHSdS0/TyuoXfrvyajFw+1dt8VZXGDvU8l7YFXxlSDxFMCrsVWT/AN03yP6sVeT/AJd/+Ta81f8AMPF/xLFXrPjiq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq44q1irsVWsN1xVzn4qYq818+L6XmHRpGcRr9ZFSxA7e+Ks+W/048D9aiqB/Ov9cVXnU9NcCl1Ft/lr/XFXfpLTv8Alqi/4Nf648Ku/SWnf8tUX/Br/XHhVr9J6b/y1R/8Gv8AXHhV36T03/lqj/4Nf648KtHVdLUGt1EP9mv9caVgv5zX1nN5MURTxv8A6bbftqf2/nirOtPLmyteIUr6Ue9f8kYqjsVdirWKuxV2KuxVD6j/ALxTf6uKsG/Kj7Ovf8xv8DirP1+2fliq8Yq7FXHFWsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirYxV2KuxV2KuxV2KuxV2KuxVZLirzrz/8A8pNoX/MUP1Yq9IGKuxV2KuxV2KtftYq1+0cVed/nbLcReUg9uoeQXUFFbp9rCFRCa55sjtIPStYiPSTuf5R7YVdJ5h87lo+NnDuN9z/TFVT9O+df+WSL7z/TFXfp3zp/yyRfef6Yq79NfmC28Nlblf8AKYg/qxpXfpf8yf8Alhtf+D/sxpVkur/mcVpFY2hY+L/2Y0qTatpv5gard2AvbS2RYJOR4PWlRTwxV6Lp1pPHaRxztxdQAQpwKmI2AwK7FWsVdirhireKuxVodMVQeq/8cy8/4wyf8QOKvPP+ce/+UBf/AJj7v/k5hCvTxirTbinjgVgv5x0i/LrVnC+o0cWwO+SV8+21qfLWmW3nK2UxB5Utpggp8LkBumKsw/KvVLHU/NPnnU7eVzaywREHv9oYWKP/AOcd7WGU+ZVljWSCS+kJdxvQg4lUkitodK/ObWk0di0Bii9eNfsUqfDCFSHyHefmKfN/mSXypaW0kQv6TrM/A7gdqeGKvpvRW1KTSYzqUSRXjCjLEaqGp44CryP82vy881TavaeZtCMc95pob93I4H2jXp1wKg/Kf5p3XmrRtc0TVLRbS/s7GYyNCp6IpHUgeGKsW8tvI3/OP+sem5ZBarwZzRuuKvUPyvOnv+WFw13Ro/RdWLf8Yziryzy+sg8ief4bY101bL/Qa/Zr6i1p2xSyL8l7X8yxpdpLFHbtp4Cipl+Knypil9D20jlFWUATADkB0wKr4q7FXlPnX/ybflX5SfrwK9UP92Plir//1PVBGKoLWd9Ju/8AjE/6sVecfk0aXOqj/V/hir1QdMVdiq1tzxxVQvJALeUHYBftHDavn38m9Xs7bz75zS4nRWbUaoD1I4DpjaoX8376z138x9Es9KIme2uYJZpI+nHuO2Nql3n/AEye+/OHT7O3ulgaSRwa1+H50xtXuf5e6R+idNe1a8W+uCzN6qVoAe2/hjaoP8yfL/lXX9LfT9Yuoo74KRA7Ejix+QxtXjWj3fmryH5xh8t2+ppfafdwtMojU7KdqVb2ONqnX5cR211+avnNJP3Ja1hqrd6vjat/kfrOm6XrXmyGW9jtIV1Fi0cnVqKdxjaofzJf2/mb82bOTRl5i0kikmnTcMoqKY2rE9K0fTL/APNzzJDe6gllzvixVywJoq+GNq+lfKVppy2Aihulu+GwKE9Bt3xtWQxgBeEZ402ocbVWQmm4ocCrq4qsl3jYex/Viryb8vzw/N3zSh3rbxf8SxV610NPHFV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KtUxV1MVaIJI9sVWshLhuwxVIPMvkrSfMUts+oIXW2f1IwCVINKdsVQKflh5dWcuUcr2X1Dira/lj5bUn93Jv/AMWHFW/+VZeW/wDfcn/Iw4bV3/KsvLf++5P+RhxtWx+WXljvFJ/yMONq7/lWXlf/AH1J/wAjDjarJPyt8qOvxwyGnT94cSVYb+bXkTQdP8mo0Mbil7bUq5P7eBXq2lQRx6fahBsIY/8AiIxVG1xV1cVaxV2KuxV2Koe/FbOUeK4qwX8qTT9PL4Xv8Dir0AD4z8sVXVxVuuKtYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXb4q7fFXb4q7fFXb4q7fFXb4q7fFXb4q7FXYq6uKt1xVwOKuxV2KuxV2KuxV2KrXUnFXnP5gjj5k0Bv5rsD/AIXFXoytUn2xVdXFXVxV1cVdXFVp61xVw3NcVYF+cS18rqf+XqD/AInirM9ORPqNuvEf3Sf8RGG1VjESw40AHUUxVU9Nf5R92Ku4L/KPuxtWuC+AxtW+C+GNqtaMEbbY2rvTHKp+jG1cEIapxtV9cCt1xVrFXYq7FW64q1XFWq7Yqg9U30y8/wCMMn/EDirzz/nHxiPIbrT/AI/7vf8A56YbV6cSR2xVpmagoOuBWP8AnrRZ9X8u3enQsFa4WhYiow2rENN/LJbnyNdeX9UKzszNJC4FAGoeONqxz8n/AMoNZ8nnWfrkwubfU0CLGqkFQGB3Jw2ikFoX5b/mhoOoammkarFb2N/K8gjMRJHIEDevvjxLTL/y9/Km78vSXuq6tOt/q18nCWVQRsDUbHHiWmGWf5Z/mNoGu6veaFqCW6apc/WKGMtxFKUO+PEtPZvJlrr1vpEcOtzC5vQSWmVeINfbElaYX5s0f8wodamuNIv1SznYkxFC2x28cFrSV+Rfyf1nTo9XvdSu45r7U4JoQyoVoJa0qPpxtab0P8mb608gX3lm6uFle6iEaSKCFWntja0kuiflF+aFrYSaJ/iCAaS7HlD6TV4HYitfDG1pmyflBY2f5fXvlayISW7hMXrmpFSQanv2xtLGfLX5XfmpoSw2ll5igSwjZaxekxJCnpWvhjavaLK3ljjUzNzn4gO42BIGNqicbV2BXlPnei/m55SBP21lP44q9T5DjSvbFX//1fVOKoLV/wDjk3n/ABif9WKvN/yc/wB69W/2P8MVeqDpirsVWMG5VHQA4qoIFltikv7ZIxV5bdfkVpx1a+v4L2Wza+lMsksQHIGlNsVT3yh+U2g+X7pr0SNqN4RtLOoDeI6YqgPNf5OaZ5h1hdXF/NYakpLBoQKgn3OKp15I8gf4ZZiuq3F+zVqJwBuflirXnb8vLbzKVZrh7SQV/eRAE7/PFUq8v/k1pej6imo3V9LqNwi8EedRVVPYUxVNtN/L3TbDzXqWvL8JvkRGAA6Ia4qxy4/I3SJNRu76Gd4frchlkCgbk4qyPyl+XWhaG31m1XnOdmkZQDtiqSX/AOSXl++1281n1Giu7iQyMVUdSB3+jFWW+V/K9t5eg4rK0la7kePyxVPVVHbmp98VXoWNa4quxVa/2G+RxV5N5F/8nD5o/wCMEX/EsVetN9sYquxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVrFXYq7FXYq7FXYq7FXYq7FXHpirz/APO3/lCl/wCY22/4nirN9O/3gtv+MMf/ABEYqicVdirsVdirsVdiqje/7yyfLFWB/lX9vX/+Y3+BxV6CPtH5Yq3irsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdhV2KuxV2KuxV2KuxV2KuxVwwK7FXYq7FWxirsVdirsVdirsVdirsVecfmL/AMpD5d/5jB/xHFXoidW+eKrsVdirsVdirR6Yq5MVYH+cH/KKj/mLt/8AieKs003/AHkt/wDjEn/ERiqJHXFW8VdirRxV2KuxV2KuxV2KuxV2KuxV2KuxV2KtdjiqE1L/AI5l5/xhk/4gcVed/wDOPpP+Bn/5j7r/AJOYq9QY4qtZiB74qtUltnG2KtMtPsDbvirZU/s9MVXDgPniqxYm9TnzNP5cVXSEeFcVaKswWjUoa4q1K8daMK4quZOQFDxGKrqbUriqz0KHkGNcVb9QdDWuKtcQd+RxV1adScVdyXxOKu5r74q8q89TIfzd8nqy78Zd6e+KvVOH7Xbjir//1vVJIxVBavvpV2B1MT7fRirzf8nvhvdWU7N8O33Yq9TBFMVbqMVWSswSq7muKtNGr0FKU3xVpk5ig2p44q5gFYUG/fFWmt1MolHUYquViw2HE174q2Bvv1xVayt1fcV2AxVqaL1eHYDqMVWyRSclKGiKKEYq709vSQcQN6/PFXVLL6XEgj9rFV3wqACOWKruNKFTT2xVeDXFW8VWv9hvkcVeSeRWUfnJ5qSvxpbwll7gcsVet8lZqg1xVfirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirq4q6uKtYq7FXYq7FXYq7FXYq7FXYq49MVef/nYQfJS03pe21f+DxVm+mkGwtiOnpJ/xEYqicVdirqjFXVGKurirsVUbwE2sgHWmKsD/KzZ/MFe17v9xxV6ACKk9sVXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXVxV1cVdXFXVxV2FXYq7FXYq7FXVxV1cVdXFXVxVoYFbxV2KuxVsYq7FXYq6uKurirq4q6uKuqMVebfmTIkWv+XWkPFTe7E/6uKs+TULKrfvl6+OKt/pKw/3+n34q1+lNP/5aE+/FVv6Z0v8A5ao/vw0rv0zpf/LVH9+NKsfXNIXY3cYPzxpWk1/Rv+WyL/gsaVhP5ralYXflpYradJZPrUB4qamgbfBSs70x0aygZTVfSQV+SjFUWpB3HTFW8VdirRxV2KuxV2KuxV2KuxV2KuxV2KuxV2KrSRxJxVB6rIiaVdsxophkAJ8SpxV5D+TPnPy7oflSSx1S/htLoXty5ilbi3Fn2NPfDRVn7fmj5I7aza/8GMeEq4fmj5GpvrNr/wAGMeEq035peRyKLrFt/wAGMeEqpn80/JS7HVrYj2cY8JV3/K2PIy7DU4P+DGPCVWH81vIteX6Th/5GDHhKu/5Wv5J+0NShI8OYx4SrQ/N/ySdvr0X/AAY/pjwlVw/NjyVuRqEJ9uYx4SqmPzb8lFt7yL/gx/THhKrZPzd8n1ot5H/wYx4Srf8Ayt3yeBX63H/wY/pjwlVP/lcnlKtPrKf8GP6Y8JVd/wArh8o0qJ1J9mH9MaVY35x+Wa/C1fkw/pjSqUn5zeX0FfTZ69gw/pjSqTfnVon7NrKfpH9MaVSf86tNCkrZSn6R/TBSsZufM7+ZfzP8p3kFlKLeBZBNLQUSp7nFXt/L4v8AJ44q/wD/1/U7EAVPTFBKC1OJpLK44HdomCj3phRxPFvK3mC/8r61qhubbmHIoxr2+WBeJlw/N1CK/Ux/w2K8Tf8AytxP+WMf8NivE0fzbr9mzH/DYrxNf8rZb/lj/wCJYrxNj82JSaJZBm8PixXiXf8AK1L7/q3L97YrxO/5Wpff9W5fvbFeJS/5Wtq3bSlp82xV3/K1tX/6tS/e2KWx+aesNuNKX72xVv8A5WjrH/VrX72xVa35oa8RSLSFd+ylmGFVv/KzfNn/AFYk/wCDbCl3/KzfNn/ViT/gmxV3/Ky/N3/Vgj/4NsVd/wArL83f9WCP/g2xVtfzH85NuugpT/XbAVb/AOVi+dP+rCn/AAbYFUbr8xPO5QBNCQGo3DtiqB/Lqy8xv+YOveYL3TVgXUYIo61JrxauKvWIxIEPwhWJ6YqiR0GKuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxVxxVrFXYq7FXYq7FXYq7FXYq7FWxirsVUpiAy1Yrv08cVYj+Zej6lqvlw2mnwC4lM0coUmn2DXCqW2+tfmNbWkEKaEjiKgZjIegFMKon/ABR+YP8A1L8f/Iw40rv8UfmD/wBS/H/yMONK1/ir8xO3lyMjx9RsVd/ir8xf+pbj/wCRjYq4eafzDPXy7Gv/AD0bFW/8UfmD/wBS/H/yMONK03mTz3ICk+iJBC2zyhySo8cVS78mJJ5l8yet9r6/3/1TkVelIOKUbFVy0oKdMVbxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuxV2KuwhXYq7FXYq7FXYq7FXYq7FXHEq7ArsVdirsVdirsVdirsVbGKuxVxxVrFXYq7FXYqsc8d6VwhXl35v6ZJf6t5aRZ2iH13tT+XCrIB5BuOTN+k5aE/Zov8ATG1XDyFN31CT7h/TFV3+AwOt/J9wxVePIdv/AL/J96DDxK3/AIDt/wDfx+4Y8StjyHZftnmfEjDxK7/Aenfyj7seJVr/AJf6Y9AyigIPTwyJKsmtraKC3FvGKKopTIqqDZQq9RiqoOmKuxVo4q7FXYq7FXYq7FXYq7FXYq4Yq3irsVWMtVK4qo3EUEtuYLhQyMKFTirEZ/yk8hXMrTXOjW9xKxqZGBr+vJidKp/8qa/Ln/qwWv3N/XD4iu/5U1+XP/Vgtfub+uPiKuH5N/lyP+lBa/c39cfEVf8A8qe/Lmn/ABwLX7m/rj4itf8AKnvy5/6l+1+5v64+Irv+VPflz/1L9r9zf1x8RV6/lF+XQFP0Ba/c39cHiK3/AMqj/Lv/AKsFr9zf1x8RVyflL+Xamv6Atfub+uPGrZ/Kj8vP+rBa/cf64ONWx+VP5eD/AKUFr9zf1x41b/5Vb5A/6sNt9zf1x41d/wAqt8gf9WG2+4/1x41XL+WHkEDbQ7Ye1D/XG1XD8ufJSfY0S3+4/wBcbVcPy/8AKHfSIIx4gH+uG1Xr5E8oL006H7j/AFx4lX/4N8pUp+j4t/Y/1wEqiNO8t6JYyB7WBIyOlBkVTVkYkfyjFX//0PU5p0OKCFB1Kyf5B2pix4UJc6dpqyFntUdpOp4Kf4Yrwq6aXp3EUtIaf8Y1/pivC3+i9P8A+WWH/kWv9MV4Xfouw/5ZYf8AkWv9MV4Xfoyw/wCWWH/kWv8ATFeFo6bYAV+qxfRGv9MV4WvqNh/yzR/8Av8ATFeF31Gw/wCWaP8A4Bf6YrwqosLKn+88X/AL/TCl31Cy/wCWeL/gF/pirvqNkP8Aj3i/4Bf6Yq76lZf74i/4Bf6Yq4WVl2gjH+wX+mKQu+p2n++U/wCBH9MUu+p2n++U/wCBH9MbVv6ra/75T/gRirvqtt/vlP8AgRirRtrcHaJP+BGBXfV4P99J/wACMVd9WgP+6k/4EYq0sSx8eCBanegpiq1o43JJ5bHscVRQ6DFXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq44q1irsVdirsVdirsVdirsVdirYxV2KrZKcfftiqi0nFOcnTphVpkjUq2+/QYVVfh8MVd8PhirqHtSmBXUb2xV3TqK/LFXfD4YVWShDGwK7EYFeeflPzFz5mCii/pD/jU4FekNQihxVyigAxVvFXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7CFdirsVdirsVdirsVdirsVccSrsCuxV2KuxV2KuxV2KuxVsYq7FXHFWsVdirsVdiq2QVWlaYQrzn8y4lGteWian/AE3t/q4VZ8oHM8OQb36YFVQJ+5GKt/H3IxV3IeBwWruQ8Djar0oR/XBat42rjjatAKvfCrQFDU9MVX4q7FWjirsVdirsVdirsVdirsVdirhireKuxVSZTXmpr44q2HDDdd8VbFD7YKVug98aVwAxpXHGlW1Hvjwq3wGCldwGNK7YbYaVvbGldQHGlWlF98aVsIAO+CldyHgcNK7kPA40rXFTvU/LFWxQeONq3WuwGNq3QdxjatMEp0GNqtJCpVRhVtXJQkjfwxV//9H1RTFVpRT1HTFW6DFW8VdirqYq6mKupirsVdireKupiimioOKXcF8MVdwXFW6Yq6mKupirqYq1xGKu4jFW6DFXYq6mKuxV2KuxV2KuxV2KuxV2KuxVrFXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXEA4qteNHXi4qOtMVb4jb26Yq3irsVcFGKu4jG1dQDFXYqtkBKEDrirzj8pJTPceaVRuLJqNCf9icKvQG9dZGYDmoX4R74qrRlmQFhxY9V8MVXUwK4+2FWt/DFW8Vdiq3kPHIq3yHjirqjxxV23jirtvHFXbeOKu28cVa5P/LiruT/y4q2CT1GKt/RirvoxV30Yq76MVW1HjkldUeOFWw6jvgV3NfHFXFwem5wFXcn/AJcCu5P/AC4quxV2KuwhXYVdirsVdirsVccVaq3hirqt4Yq6reGKuq3hiqlOjvHRfhaoxV55+aEwi1ryyGbregD58cCvRoyGAINfHFV+KtcRireNK7BSuxpXY0rsaVplDdcKuKginbFVw6Yq7FXYq1irjirW/hirt/DFXVbwxV1W8MVbFcVdirsVdvirRBxVygKKAUGKt1OKtd8Vdt44q6o8cVaqvjiriV8cVW8h/Niq8MvjirRZfHFXcl8cVdyXxxVuo8cVdUeOKuqPHFXVHjirXw+OKuqPHBSuqe2+NK5Q1d+mGlbPE40q4AAUHTFXYq//0vVGKuxV2KuxV2KuxV2KuxV2KuxVsYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq7FXYq1irsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVdirsVbGKuxVxxVrFWjWm3XFXmf5PALfebQpDA6keRG1DxO2+FXo0YT9lmrXvXCq49dw1fbFWtvBsVbUivRsVbJNOhxVbVv5Tirqt/KcVb9R/99/iv9cGyu9R/99fiv9cdlaMk3aL8V/rjsrXqT/76/Ff647K71J/99fiv9cdld6k/++vxX+uOyu9Sf/fX4r/XHZVP1H7xP96/1xV3qN/vp/vX+uKu9acfZt3YePJP4nFXevc/8sz/APBJ/wA1Yq717n/lmf8A4JP+asVd69z/AMsz/wDBJ/zVirvXuf8Almf/AIJP+asVa+s3X/LI3/BR/wDNWFXfWbv/AJZG/wCCj/5qxVr61d/8sbf8HH/zVirvrV3/AMsbf8HH/wA1YFWvdXVN7N/oeP8A5qxVb9ZuP+WSX/go/wDmrArvrNx/yxy/8FH/AM1Yqu+uXn/LBL/wcX/NeFXfXLz/AJYJf+Di/wCa8Vd9cvP+WCX/AIOL/mvFVj6hqKmiaZM48RJAP1uMKrf0lqn/AFaZ/wDkbb/9VMVd+ktU/wCrTP8A8jbf/qpirv0lqn/Vpn/5G2//AFUxV36S1T/q0z/8jbf/AKqYq79Jap20mev/ABlt/wDqpirv0lq//Vom/wCRtv8A9VMGyu/SWr/9Wib/AJG2/wD1Ux2V36S1f/q0Tf8AI23/AOqmOyu/SWr/APVom/5G2/8A1Ux2VTOpap6m+kz8qGlJben/ACcwKwPzvO82teXTfW72zC9rGHZGqePT4GbFXqAMnMcQOHfxxVUxV2KuxV2KuxV2KuxV2KuxV2KuxVo4q19OKu37Yq38eKu+PFXfHirvjxV3xYq74sVd8WKt/FirvixVrfFW98VaxV2KuxV22Ku2xV3we2Ku2xVo/RirvuxVw+jFW9vbFXbe2Ku29sVdt7Yq7b2xV23tirR5/sU98VcedN8VctPniq/FXYq//9k="
						}]
					}, {
						"SectionTitle": "H - Rotor Shaft Dimensions Check",
						"Questions": [{
							"QuestionHeader": "Standard Question",
							"QuestionTitle": "Drive End (DE)",
							"QuestionType": "Standard",
							"Fields": [{
								"type": "header",
								"title": "Clearance"
							}, {
								"type": "input",
								"title": "Min. clearance",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "Max. clearance",
								"unit": "mm"
							}, {
								"type": "header",
								"title": "Before Reclamation"
							}, {
								"type": "input",
								"title": "A-A1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "B-B1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "C-C1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "D-D1",
								"unit": "mm"
							}, {
								"type": "header",
								"title": "After Reclamation / Spare Rotor"
							}, {
								"type": "input",
								"title": "A-A1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "B-B1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "C-C1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "D-D1",
								"unit": "mm"
							}, {
								"type": "header",
								"title": "Repair Notes"
							}, {
								"type": "checkBox",
								"title": "",
								"CheckBoxes": [{
									"Key": "DE side reused without repair",
									"Title": "DE side reused without repair"
								}, {
									"Key": "DE side reclaimed via metal spray",
									"Title": "DE side reclaimed via metal spray"
								}, {
									"Key": "DE side reclaimed via welding",
									"Title": "DE side reclaimed via welding"
								}, {
									"Key": "Spare rotor used",
									"Title": "Spare rotor used"
								}]
							}],
							"Points": []
						}, {
							"QuestionHeader": "Standard Question",
							"QuestionTitle": "Non Drive End (NDE)",
							"QuestionType": "Standard",
							"Fields": [{
								"type": "header",
								"title": "Clearance"
							}, {
								"type": "input",
								"title": "Min. clearance",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "Max. clearance",
								"unit": "mm"
							}, {
								"type": "header",
								"title": "Before Reclamation"
							}, {
								"type": "input",
								"title": "A-A1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "B-B1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "C-C1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "D-D1",
								"unit": "mm"
							}, {
								"type": "header",
								"title": "After Reclamation / Spare Rotor"
							}, {
								"type": "input",
								"title": "A-A1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "B-B1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "C-C1",
								"unit": "mm"
							}, {
								"type": "input",
								"title": "D-D1",
								"unit": "mm"
							}, {
								"type": "header",
								"title": "Repair Notes"
							}, {
								"type": "checkBox",
								"title": "",
								"CheckBoxes": [{
									"Key": "NDE side reused without repair",
									"Title": "DE side reused without repair"
								}, {
									"Key": "NDE side reclaimed via metal spray",
									"Title": "DE side reclaimed via metal spray"
								}, {
									"Key": "NDE side reclaimed via welding",
									"Title": "DE side reclaimed via welding"
								}, {
									"Key": "Spare rotor used",
									"Title": "Spare rotor used"
								}]
							}],
							"Points": []
						}]
					}]
				}]
			});
			model.setDefaultBindingMode("TwoWay");
			return model;
		}
	};
});