fiori_client_appConfig = {
    "appID": "com.twobm.mobileworkorder.HeidelbergWorkOrderExtension",
    
    "fioriURL": "https://mobile-ewqbr02yqe.hana.ondemand.com:443/",
    
    "fioriURLIsSMP": true,
    
    "multiUser": false,
    
    "certificate": "",
    
    "federated_certificate" : "",
    
    "autoSelectSingleCert": false,
    
    "appName": "WO_DR1",
    
    "passcodePolicy":  {
        "expirationDays":"0",
        "hasDigits":"false",
        "hasLowerCaseLetters":"false",
        "hasSpecialLetters":"false",
        "hasUpperCaseLetters":"false",
        "defaultAllowed":"true",
        "lockTimeout":"300",
        "minLength":"8",
        "minUniqueChars":"0",
        "retryLimit":"10"
    },

    "communicatorId":"REST",

	"resourcePath":"",

	"auth": [{
		"type": "oauth2",
		"config": {
			"oauth2.authorizationEndpoint": "https://oauthasservices-ewqbr02yqe.hana.ondemand.com/oauth2/api/v1/authorize",
			"oauth2.tokenEndpoint": "https://oauthasservices-ewqbr02yqe.hana.ondemand.com/oauth2/api/v1/token",
			"oauth2.clientID": "2ff1537a-e782-4b5a-8fc7-e6b4ebb8359b",
			"oauth2.grantType": "authorization_code"
		}
	}],

    "keysize": "",

    "idpLogonURL": "",

    "disablePasscode": false
};

