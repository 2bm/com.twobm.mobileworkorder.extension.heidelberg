jQuery.sap.require("sap.m.BusyDialog");
jQuery.sap.require("sap.ui.Device");
/* hybrid capacity bootstrap
 *
 * This has to happen after sapui5 bootstrap, and before first application page is loaded.
 */

sap.hybrid = {
	loadCordova: false,
	isOnline: false,
	isInForeGround: true,
	store: null,
	definingRequests: {},
	servicePath: "",
	interruptingSync: false,
	downloadAllImages: false,

	setCordova: function () {
		sap.hybrid.loadCordova = true;
	},

	//Edited to not use route destination
	packUrl: function (url, route) {
		return '/' + fiori_client_appConfig.appID + (route.entryPath ? route.entryPath : "") + url.substring(route.path
			.length);
	},

	// packUrl: function(url, route) {
	// 	return '/' + fiori_client_appConfig.appID + '_' + route.destination + (route.entryPath ? route.entryPath : "") + url.substring(route.path
	// 		.length);
	// },

	appLogon: function (appConfig) {
		var context = {};

		var url = appConfig.fioriURL;
		if (url && (url.indexOf('https://') === 0 || url.indexOf('http://') === 0)) {
			if (url.indexOf('https://') === 0) {
				context.https = true;
				url = url.substring(('https://').length);
			} else {
				context.https = false;
				url = url.substring(('http://').length);
			}

			if (url.indexOf('?') >= 0) {
				url = url.substring(0, url.indexOf('?'));
			}
			if (url.indexOf('/') >= 0) {
				url = url.split('/')[0];
			}
			if (url.indexOf(':') >= 0) {
				context.serverHost = url.split(':')[0];
				context.serverPort = url.split(':')[1];
			}
		}

		// set auth element
		if (appConfig.auth) {
			context.auth = appConfig.auth;
		}

		// If communicatorId is set then use it to be compatible with existing values. Otherwise, use the default "REST". By doing so logon core does not need to send ping request to server root URL, which will cause authentication issue. It occurs when the root URL uses a different auth method from the application's endpoint URL, as application can only handle authentication on its own endpoint URL.
		context.communicatorId = appConfig.communicatorId ? appConfig.communicatorId : "REST";

		//2BM: Added to only show username and password at login
		context.custom = {

			// // This is the Fiori Client background image
			//"backgroundImage": "../../../images/LoginBackground.png",

			// // These fields will be hidden, but any default values specified above will still be sent
			"hiddenFields": ["farmId", "serverHost", "serverPort", "resourcePath", "securityConfig", "https"],

			// // This option hides the logo and copyright information
			"hideLogoCopyright": false,

			// // This option specifies a custom logo image
			"copyrightLogo": "../../../images/WH-logo-login.png",

			// // This option specifies both lines of the copyright message
			"copyrightMsg": ["Copyright 2020 2BM A/S", "All rights reserved."],

			// // This option skips the passcode screen
			// "disablePasscode": false,

			// // This option specifies the css file used to customize the logon screen
			// 	"styleSheet": "../../../custom.css"
		};

		if ('serverHost' in context && 'serverPort' in context && 'https' in context) {
			// start SCPms logon
			sap.hybrid.kapsel.doLogonInit(context, appConfig.appID, sap.hybrid.getOneTimePassword);
		} else {
			//console.log("context data for logon are not complete");
		}
	},

	getOneTimePassword: function () {

		function getOneTimePasswordError(error) {
			console.log("No Passcode provided - continue with no offline database encryption");
			sap.hybrid.openStore(null);
		}

		function getOneTimePasswordSuccess(val) {
			if (val === null) {
				//Create the key value
				val = URL.createObjectURL(new Blob([])).slice(-36).replace(/-/g, "");
				sap.Logon.set(function () {
					sap.hybrid.openStore(val);
				}, function (errorInfo) {
					sap.hybrid.openStore(val);
					console.log("Error - creating encryptkey in datavault");
				}, "encryptkey", val);
				return;
			}
			//otp = val;
			sap.hybrid.openStore(val);
		}
		
		//Login succeeded. Show start screen again in loggedin mode
		sap.hybrid.setStartPageLoggedIn();

		sap.Logon.get(getOneTimePasswordSuccess, getOneTimePasswordError, "encryptkey");
	},

	bootStrap: function () {
		if (sap.hybrid.loadCordova) {
			// bind to Cordova event
			document.addEventListener("deviceready", function () {
				//get info from manifest
				jQuery.getJSON("manifest.json").done(function (appManifest) {
					//2BM: Setting values
					//2BM: getting from manifest.json (app ID) - to identify SCPms app name
					//fiori_client_appConfig.appID = appManifest["sap.app"].id;
					//2BM: getting from manifest.json (relative Odata service path)
					sap.hybrid.servicePath = appManifest["sap.app"].dataSources[appManifest["sap.ui5"].models[""].dataSource].uri;

					//2BM: console logging values
					////console.log("fiori_client_appConfig.appID: " + fiori_client_appConfig.appID);
					////console.log("sap.hybrid.servicePath: " + sap.hybrid.servicePath);

					var definingRequestsPath = "definingRequestsAll.json";
					
					if(!sap.hybrid.downloadAllImages){
						definingRequestsPath = "definingRequestsWithoutOndemand.json";
					}

					jQuery.getJSON(definingRequestsPath).done(function (definingRequests) {
						//2BM: Setting values
						//2BM: getting from definingRequests.json (offline defining request)
						sap.hybrid.definingRequests = definingRequests.definingRequests;
						//2BM: console logging values
						////console.log("sap.hybrid.definingRequests: " + sap.hybrid.definingRequests);

						// check if app configuration is available
						if (fiori_client_appConfig && fiori_client_appConfig.appID && fiori_client_appConfig.fioriURL) {
							sap.hybrid.appLogon(fiori_client_appConfig);
						} else {
							////console.log("Can't find app configuration probably due to a missing appConfig.js from the app binary.");
						}

					}).fail(function (error) {
						//console.error("Failed to load definingRequests.json");
					});

				}).fail(function (error) {
					//console.error("Failed to load manifest.json");
				});

			}, false);
		} else {
			////console.log("cordova is not loaded");
		}
	},

	openStore: function (encryptKey) {
		//console.log("In openStore");
		jQuery.sap.require("sap.ui.thirdparty.datajs"); //Required when using SAPUI5 and the Kapsel Offline Store

		var properties = {
			"name": "store_mainService",
			"host": sap.hybrid.kapsel.appContext.registrationContext.serverHost,
			"port": sap.hybrid.kapsel.appContext.registrationContext.serverPort,
			"https": sap.hybrid.kapsel.appContext.registrationContext.https,
			"serviceRoot": fiori_client_appConfig.appID + sap.hybrid.servicePath,
			"definingRequests": sap.hybrid.definingRequests
		};

		sap.hybrid.store = sap.OData.createOfflineStore(properties);

		var openStoreSuccessCallback = function () {
			//make sure we allow the app to go to sleep again
			if(window.plugins.insomnia){
				window.plugins.insomnia.allowSleepAgain();
			}
			
			// only set incase of log in
			if (window.localStorage.getItem("userLoggedIn") !== "true") {
				// set localStorage userLogged in to true - used in index.html
				window.localStorage.setItem("userLoggedIn", "true");

				// end time for loading
				var endLoadingTime = performance.now();
				var loadingTime = endLoadingTime - this.startLoading;
				// transform to seconds
				loadingTime = loadingTime / 1000.0;
				window.localStorage.setItem("lastLoadingTime", loadingTime);
			}

			sap.OData.applyHttpClient(); //Offline OData calls can now be made against datajs.
			sap.Xhook.disable(); // temporary workaround to ensure the offline app can work in WKWebView
			sap.hybrid.startApp();
		}.bind(this);

		var openStoreErrorCallback = function (error) {
			if(window.plugins.insomnia){
				window.plugins.insomnia.allowSleepAgain(); //Allow the app to sleep again
			}

			this.errorDialog = new sap.m.BusyDialog("errorDialog");
			this.errorDialog.setTitle("Error");
			this.errorDialog.setText(error + " \n \n " + "Resetting client in 30 seconds");
			this.errorDialog.open();

			setTimeout(function () {
				this.errorDialog.close();
				sap.hybrid.logOutAndResetDataBaseDBClosed();
			}.bind(this), 30000);

			// if (error.includes("does not match the ones used when the store was created")||
			// error.includes("is not an entity set in entity container")) {
			// 	//[-10028] Error at URL position 2: \"XXXXXX\" is not an entity set in entity container \"WORKORDER_SRV_Entities\""
			// 	//[-10247] The defining query \"XXXXX\" does not match the ones used when the store was created."
			// 	//A new defining request has been added to the database since last deploy
			// 	//Log out and relogin
			// 	sap.hybrid.logOutAndResetDataBaseDBClosed();
			// }
		}.bind(this);

		//Set to false in order to fix "Show login popup" issue on Android Resume event
		var options = {
			"autoRefresh": false, //enables automatic refresh when the application enters foreground     
			"autoFlush": false //enables automatic flush when the application goes into the background 
		};

		if (encryptKey) {
			options.storeEncryptionKey = encryptKey;
		}
		
		if(window.plugins.insomnia){
			window.plugins.insomnia.keepAwake(); // prevent sleep while downloading DB
		}

		sap.hybrid.store.open(openStoreSuccessCallback, openStoreErrorCallback, options);
	},

	deviceOnline: function () {
		sap.hybrid.isOnline = true;
		////console.log("Device went online");
	},

	deviceOffline: function () {
		sap.hybrid.isOnline = false;
		//console.log("Device went offline");
	},

	pauseEvent: function () {
		sap.hybrid.isInForeGround = false;
	},

	resumeEvent: function () {
		sap.hybrid.isInForeGround = true;
	},
	
	logOutAndResetDataBaseDBClosed: function () {
		sap.hybrid.interruptingSync = true;
		sap.OData.removeHttpClient();
		sap.hybrid.store.clear(function () {
				sap.hybrid.store = null;

				sap.Logon.core.deleteRegistration(
					function (res) {
						sap.hybrid.interruptingSync = false;
						sap.hybrid.setStartPageNotLoggedIn();
						sap.Logon.core.loadStartPage();
					},
					function (errObj) {
						sap.hybrid.interruptingSync = false;
						sap.hybrid.setStartPageNotLoggedIn();
						sap.Logon.core.loadStartPage();
					});
			},
			function (errObj) {
				sap.hybrid.interruptingSync = false;
				sap.hybrid.setStartPageNotLoggedIn();
			}
		);
	},

	logOutAndResetDataBaseDBOpen: function () {
		sap.hybrid.interruptingSync = true;
		sap.OData.removeHttpClient();
		sap.hybrid.store.close(function (arg) {
				//sap.OData.removeHttpClient();
				sap.hybrid.store.clear(function (arg) {
						sap.hybrid.store = null;

						sap.Logon.core.deleteRegistration(
							function (res) {
								sap.hybrid.interruptingSync = false;
								sap.hybrid.setStartPageNotLoggedIn();
								sap.Logon.core.loadStartPage();
							},
							function (errObj) {
								sap.hybrid.interruptingSync = false;
								sap.hybrid.setStartPageNotLoggedIn();
								sap.Logon.core.loadStartPage();
							});
					},
					function (errObj) {
						sap.hybrid.interruptingSync = false;
						sap.hybrid.setStartPageNotLoggedIn();
					}
				);
			},
			function (errObj) {
				sap.hybrid.interruptingSync = false;
				sap.hybrid.setStartPageNotLoggedIn();
			});
	},
	
	setStartPageLoggedIn: function () {
		this.startLoading = performance.now();

		// Hide login button in index.html
		document.getElementById("logInButton").style.display = "none";
		
		// Hide downloadAllImagesSection
		document.getElementById("downloadAllImagesSection").style.display = "none";

		// show loader icon in index.html
		document.getElementById("loaderLogInPage").style.display = "flex";
		//Show descriptive text
		document.getElementById("loadingOfflineDataText").style.display = "flex";

		//Set descriptive text
		if (window.localStorage.getItem("userLoggedIn") === "true") {
			document.getElementById("loadingOfflineDataText").innerText = "Initiating application";
		} else {
			document.getElementById("loadingOfflineDataText").innerText = "Downloading data from server: ";

			// show last reading, only if available
			if (window.localStorage.getItem("lastLoadingTime") !== null) {
				document.getElementById("lastLoadingTime").style.display = "flex";
			}

			// show loading time
			document.getElementById("loadingTimeStopWatch").style.display = "flex";
			
			
			// start loading time counter
			var n = 0;
			var e = document.getElementById("loadingTimeStopWatch");
			setInterval(function () {
				var seconds = ++n;
				
				var endLoadingTime = performance.now();
				var loadingTime = endLoadingTime - this.startLoading;
				// transform to seconds
				loadingTime = loadingTime / 1000.0;
				loadingTime = loadingTime.toFixed(0);
				
				e.innerHTML = loadingTime + " s";
			}.bind(this), 1000);
		}
	},

	setStartPageNotLoggedIn: function () {
		// Show login button in index.html
		document.getElementById("logInButton").style.display = "block";

		// Hide loader icon in index.html
		document.getElementById("loaderLogInPage").style.display = "none";
		document.getElementById("loadingOfflineDataText").style.display = "none";

		// Hide last reading 
		document.getElementById("lastLoadingTime").style.display = "none";

		// Hide stop watch
		document.getElementById("loadingTimeStopWatch").style.display = "none";
		
		// Show offlineModeSection
		document.getElementById("downloadAllImagesSection").style.display = "block";

		window.localStorage.setItem("userLoggedIn", "false");
	},

	loadComponent: function (componentName) {
		sap.ui.getCore().attachInit(function () {
			sap.ui.component({
				name: componentName,
				async: true,
				manifestFirst: true
			}).then(function (oComponent) {
				sap.ui.require([
					"sap/ui/core/ComponentContainer",
					"sap/m/Shell"
				], function (ComponentContainer, Shell) {
					new Shell({
						app: new ComponentContainer("mainComponent", {
							height: "100%",
							component: oComponent,
							async: true,
							lifecycle: "Container"
						})
					}).placeAt("content");
				});
			});
		});
	}
};