fiori_client_appConfig = {
    "appID": "com.twobm.mobileworkorder.HeidelbergWorkOrderExtension",
    
    "fioriURL": "https://mobile-r5qvlp8eil.cn1.hana.ondemand.com:443/",
    
    "fioriURLIsSMP": true,
    
    "multiUser": false,
    
    "certificate": "",
    
    "federated_certificate" : "",
    
    "autoSelectSingleCert": false,
    
    "appName": "WO_QRB",
    
    "passcodePolicy":  {
        "expirationDays":"0",
        "hasDigits":"false",
        "hasLowerCaseLetters":"false",
        "hasSpecialLetters":"false",
        "hasUpperCaseLetters":"false",
        "defaultAllowed":"true",
        "lockTimeout":"300",
        "minLength":"8",
        "minUniqueChars":"0",
        "retryLimit":"10"
    },

    "communicatorId":"REST",

	"resourcePath":"",

	"auth": [{
		"type": "oauth2",
		"config": {
			"oauth2.authorizationEndpoint": "https://oauthasservices-r5qvlp8eil.cn1.platform.sapcloud.cn/oauth2/api/v1/authorize",
			"oauth2.tokenEndpoint": "https://oauthasservices-r5qvlp8eil.cn1.platform.sapcloud.cn/oauth2/api/v1/token",
			"oauth2.clientID": "14d21957-2b11-4713-90d6-86695d561421",
			"oauth2.grantType": "authorization_code"
		}
	}],

    "keysize": "",

    "idpLogonURL": "",

    "disablePasscode": false
};

