fiori_client_appConfig = {
    "appID": "com.twobm.mobileworkorder.HeidelbergWorkOrderExtension",
    
    "fioriURL": "https://mobile-l52s38ya5v.hana.ondemand.com:443/",
    
    "fioriURLIsSMP": true,
    
    "multiUser": false,
    
    "certificate": "",
    
    "federated_certificate" : "",
    
    "autoSelectSingleCert": false,
    
    "appName": "WO_PR0",
    
    "passcodePolicy":  {
        "expirationDays":"0",
        "hasDigits":"false",
        "hasLowerCaseLetters":"false",
        "hasSpecialLetters":"false",
        "hasUpperCaseLetters":"false",
        "defaultAllowed":"true",
        "lockTimeout":"300",
        "minLength":"8",
        "minUniqueChars":"0",
        "retryLimit":"10"
    },

    "communicatorId":"REST",

	"resourcePath":"",

	"auth": [{
		"type": "oauth2",
		"config": {
			"oauth2.authorizationEndpoint": "https://oauthasservices-l52s38ya5v.hana.ondemand.com/oauth2/api/v1/authorize",
			"oauth2.tokenEndpoint": "https://oauthasservices-l52s38ya5v.hana.ondemand.com/oauth2/api/v1/token",
			"oauth2.clientID": "f053ca0a-8254-4930-ba3e-df5ceedbfea1",
			"oauth2.grantType": "authorization_code"
		}
	}],

    "keysize": "",

    "idpLogonURL": "",

    "disablePasscode": false
};

