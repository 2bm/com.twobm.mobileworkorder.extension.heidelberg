fiori_client_appConfig = {
    "appID": "com.twobm.mobileworkorder.HeidelbergWorkOrderExtension",
    
    "fioriURL": "https://mobile-hd71wuqo2q.hana.ondemand.com:443/",
    
    "fioriURLIsSMP": true,
    
    "multiUser": false,
    
    "certificate": "",
    
    "federated_certificate" : "",
    
    "autoSelectSingleCert": false,
    
    "appName": "WO_QR2",
    
    "passcodePolicy":  {
        "expirationDays":"0",
        "hasDigits":"false",
        "hasLowerCaseLetters":"false",
        "hasSpecialLetters":"false",
        "hasUpperCaseLetters":"false",
        "defaultAllowed":"true",
        "lockTimeout":"300",
        "minLength":"8",
        "minUniqueChars":"0",
        "retryLimit":"10"
    },

    "communicatorId":"REST",

	"resourcePath":"",

	"auth": [{
		"type": "oauth2",
		"config": {
			"oauth2.authorizationEndpoint": "https://oauthasservices-hd71wuqo2q.hana.ondemand.com/oauth2/api/v1/authorize",
			"oauth2.tokenEndpoint": "https://oauthasservices-hd71wuqo2q.hana.ondemand.com/oauth2/api/v1/token",
			"oauth2.clientID": "258da392-8da4-4336-9eb0-c2225f75c9df",
			"oauth2.grantType": "authorization_code"
		}
	}],

    "keysize": "",

    "idpLogonURL": "",

    "disablePasscode": false
};

