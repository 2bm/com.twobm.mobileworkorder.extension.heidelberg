fiori_client_appConfig = {
    "appID": "com.twobm.mobileworkorder.HeidelbergWorkOrderExtension",
    
    "fioriURL": "https://mobile-rrq8n7rj74.us2.hana.ondemand.com:443/",
    
    "fioriURLIsSMP": true,
    
    "multiUser": false,
    
    "certificate": "",
    
    "federated_certificate" : "",
    
    "autoSelectSingleCert": false,
    
    "appName": "WO_QRL",
    
    "passcodePolicy":  {
        "expirationDays":"0",
        "hasDigits":"false",
        "hasLowerCaseLetters":"false",
        "hasSpecialLetters":"false",
        "hasUpperCaseLetters":"false",
        "defaultAllowed":"true",
        "lockTimeout":"300",
        "minLength":"8",
        "minUniqueChars":"0",
        "retryLimit":"10"
    },

    "communicatorId":"REST",

	"resourcePath":"",

	"auth": [{
		"type": "oauth2",
		"config": {
			"oauth2.authorizationEndpoint": "https://oauthasservices-rrq8n7rj74.us2.hana.ondemand.com/oauth2/api/v1/authorize",
			"oauth2.tokenEndpoint": "https://oauthasservices-rrq8n7rj74.us2.hana.ondemand.com/oauth2/api/v1/token",
			"oauth2.clientID": "0850bb98-f118-4b2a-892a-733198922c5f",
			"oauth2.grantType": "authorization_code"
		}
	}],

    "keysize": "",

    "idpLogonURL": "",

    "disablePasscode": false
};

